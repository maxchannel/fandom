<?php require 'sessions.php'; ?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<title>Fanplusplus Terms of Service</title>
	<meta name="description" content="Terminos y Condiciones para el uso de Fan Plus Plus." />
	<?php include "favicon.php"; ?>
	<?php include 'static/heads/headcolumn.php'; ?>
</head>
<body>
<?php include "analyticstracking.php" ?>
	<header>
		<?php include '../fanplusplus/static/navs/nav.php'; ?>
	</header>
	<section id="feedMain">
		<div class="bordes">
			<p>Condiciones de Servicio</p>
		</div>
	</section>
	<?php include 'footer.php'; ?>
</body>
</html>