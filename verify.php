<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invitar Amigos a unirse a Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <meta name="description" content="Invitar Amigos a unirse a Fan Plus Plus por medio de correo electrónico."/>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes personalizadas -->
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "static/analyticstracking.php" ?>
    <?php include 'static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Verifica tu email</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        if(isset($_POST['codigo']) && !empty($_POST['codigo']))
                        {
                            include 'linksec.php';            

                            $con = mysqli_connect($host,$user,$pw,$db);
                            $codigo = mysqli_real_escape_string($con, $_POST['codigo']);

                            $existenciaCodigo = mysqli_query($con, "SELECT CODIGO FROM verify WHERE CODIGO='".$codigo."' ");

                            if(mysqli_num_rows($existenciaCodigo) == 1)
                            {
                                if(mysqli_query($con,"DELETE FROM verify WHERE CODIGO='".$codigo."'"))
                                {
                                    echo '<div class="alert alert-success">
                                    <p>Correo CONFIRMADO</p>
                                    <p><a href="http://www.fanplusplus.com/signin.php">Ahora puedes Iniciar Sesión</a></p>
                                    </div>';
                                }else
                                {
                                    echo '<div class="alert alert-danger">
                                    <p>Error al verificar código.</p>
                                    <p>Contacto para soporte: <a href="http://www.twitter.com/fanplusplus_es">@fanplusplus_es</a></p>
                                    </div>';
                                    echo '<form action="verify.php" method="post" class="form-signin" role="form">
                                            <h2 class="form-signin-heading">Verificación de Email</h2>
                                            <label for="enterUser">Código</label>
                                            <input name="codigo" type="text" class="form-control" id="enterUser" placeholder="Usuario" required autofocus>
                                            <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
                                        </form>';
                                } 
                            }else 
                            {
                                echo '<div class="alert alert-danger">
                                    <p>Código INCORRECTO</p>
                                    </div>';
                                echo '<form action="verify.php" method="post" class="form-signin" role="form">
                                        <h2 class="form-signin-heading">Verificación de Email</h2>
                                        <label for="enterUser">Código</label>
                                        <input name="codigo" type="text" class="form-control" id="enterUser" placeholder="Usuario" required autofocus>
                                        <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
                                    </form>';
                            }          

                            mysqli_close($con);
                        }else
                        {
                            echo '<div class="alert alert-warning">
                                <p>Debes introducir tu codigo de confirmación.</p>
                                </div>';
                            echo '<form action="verify.php" method="post" class="form-signin" role="form">
                                        <h2 class="form-signin-heading">Verificación de Email</h2>
                                        <label for="enterUser">Código</label>
                                        <input name="codigo" type="text" class="form-control" id="enterUser" placeholder="Usuario" required autofocus>
                                        <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
                                    </form>';
                        }
                        
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-8">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include 'static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
</body>
</html>