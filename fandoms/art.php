<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fandoms de Arte</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Aquí encontraras todos los fandoms de Arte."/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">

            <!-- Modal para enviar error-->                    
            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Informar sobre error</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para enviar error-->
            <!-- Modal para reportar a fandom-->                   
            <div class="modal fade bs-example-modal-sm-report-f" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Fandom</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Fandom Repetido</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar a fandom-->

            <h1 class="text-center">Fandoms de Arte</h1>
            <?php
            include '../link.php';
            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
            $result = mysqli_query($con,"SELECT * FROM fandoms WHERE CATEGORY='Art' ORDER BY CREATION DESC");  

            function ifFan($a,$b) //Id del Fandom y id de la sesion
            {
                include '../link.php';
                $con = mysqli_connect($host,$user,$pw,$db);
                $a = mysqli_real_escape_string($con, $a);
                $b = mysqli_real_escape_string($con, $b);
                $result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$a."' AND MEMBER_ID='".$b."'");
                $total = mysqli_num_rows($result);            

                if($total > 0)
                {
                    echo '<button id="btnUnfollow" onclick="noFan('.$a.','.$b.',this)" type="button" class="btn btn-danger">Dejar</button>';
                    //echo '<button id="btnUnfollow" onclick="noFan('.$a.','.$b.',this)" >Dejar el Fandom</button>';
                }else
                {
                    echo '<button id="btnFollow" onclick="makeFan('.$a.','.$b.',this)" type="button" class="btn btn-success">Unirme</button>';
                    //echo '<button id="btnFollow" onclick="makeFan('.$a.','.$b.',this)">Unirme al Fandom</button>';
                }        

                mysqli_close($con);
            }  

            while($row = mysqli_fetch_array($result))
            {
                if($row['MODE'] == "fandom") 
                {
                    $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                    $row2 = mysqli_fetch_array($result2);
                    $min = strtolower($row['FANDOM']);

                    echo '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'"class="fandom-inside-image"></a>
                                    <p class="fandom-title"><a href="../'.$min.'" >'.$row['FANDOM'].'</a></p>
                                    <p class="fandom-members">Fans:<a href="../'.$min.'/members" > 28</a></p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                        ifFan($row['ID'], $_SESSION['user_id']);
                                    }else
                                    {
                                        echo '<a href="../signin.php" class="btn btn-primary" >Login</a>';
                                    }
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
                }elseif($row['MODE'] == "fans")
                {
                    $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                    $row2 = mysqli_fetch_array($result2);
                    $min = strtolower($row['URL']);

                    echo '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'"class="fandom-inside-image"></a>
                                    <p class="fandom-title"><a href="../'.$min.'" >'.$row['FANDOM'].'</a></p>
                                    <p class="fandom-members">Fans:<a href="../'.$min.'/members" > 28</a></p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                        ifFan($row['ID'], $_SESSION['user_id']);
                                    }else
                                    {
                                        echo '<a href="../signin.php" class="btn btn-primary" >Login</a>';
                                    }
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
                }
            }
            echo '</table>';
            mysqli_close($con);

            ?>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
    <script src="../static/js/fluid.js"></script>
</body>
</html>