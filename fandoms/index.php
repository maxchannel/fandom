<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fandoms Organizados por Categorías</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Aquí encontraras todos los fandoms organizados por Categorías"/>
    <link rel="stylesheet" type="text/css" href="../static/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <!-- Boton para crear Fandom -->
        <section class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <p>Quieres tener tu propio Fandom?</p>
                <a href="../newfan.php" class="btn btn-success">Crealo Aquí</a>
            </div>
            <div class="col-md-4">
            </div>
        </section>
        <!-- Seccion de Fandoms -->
        <section class="row">
            <!-- Seccion 1-->
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="../fandoms/art.php"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/art.php">Arte</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <?php
                        include '../link.php';
                        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                        $result = mysqli_query($con,"SELECT * FROM fandoms WHERE CATEGORY='Art' ORDER BY CREATION DESC LIMIT 3");                

                        while($row = mysqli_fetch_array($result))
                        {
                            if($row['MODE'] == "fandom") 
                            {
                                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row2 = mysqli_fetch_array($result2);
                                $min = strtolower($row['FANDOM']);       

                                echo '<li class="list-group-item">
                                        <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-image" /></a>
                                        <a href="../'.$min.'" >'.$row['FANDOM'].'</a>
                                    </li>';     

                            }elseif($row['MODE'] == "fans")
                            {
                                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row2 = mysqli_fetch_array($result2);
                                $min = strtolower($row['URL']);        

                                echo '<li class="list-group-item">
                                        <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-image" /></a>
                                        <a href="../'.$min.'" >'.$row['FANDOM'].'</a>
                                    </li>';         

                            }
                        }
                        echo '</table>';
                        mysqli_close($con);            

                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/architecture.php">Arquitectura</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <?php
                        include '../link.php';
                        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                        $result = mysqli_query($con,"SELECT * FROM fandoms WHERE CATEGORY='Arquitecture' ORDER BY CREATION DESC LIMIT 3");                

                        while($row = mysqli_fetch_array($result))
                        {
                            if($row['MODE'] == "fandom") 
                            {
                                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row2 = mysqli_fetch_array($result2);
                                $min = strtolower($row['FANDOM']);       

                                echo '<li class="list-group-item">
                                        <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-image" /></a>
                                        <a href="../'.$min.'" >'.$row['FANDOM'].'</a>
                                        <p class="pull-right">Fans: <a href="#">14</a></p>
                                    </li>';     

                            }elseif($row['MODE'] == "fans")
                            {
                                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row2 = mysqli_fetch_array($result2);
                                $min = strtolower($row['URL']);        

                                echo '<li class="list-group-item">
                                        <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-image" /></a>
                                        <a href="../'.$min.'" >'.$row['FANDOM'].'</a>
                                        <p class="pull-right">Fans: <a href="#">14</a></p>
                                    </li>';         

                            }
                        }
                        echo '</table>';
                        mysqli_close($con);            

                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/food.php">Alimentos</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <?php
                        include '../link.php';
                        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                        $result = mysqli_query($con,"SELECT * FROM fandoms WHERE CATEGORY='Food' ORDER BY CREATION DESC LIMIT 3");                

                        while($row = mysqli_fetch_array($result))
                        {
                            if($row['MODE'] == "fandom") 
                            {
                                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row2 = mysqli_fetch_array($result2);
                                $min = strtolower($row['FANDOM']);       

                                echo '<li class="list-group-item">
                                        <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-image" /></a>
                                        <a href="../'.$min.'" >'.$row['FANDOM'].'</a>
                                        <p class="pull-right">Fans: <a href="#">14</a></p>
                                    </li>';     

                            }elseif($row['MODE'] == "fans")
                            {
                                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row2 = mysqli_fetch_array($result2);
                                $min = strtolower($row['URL']);        

                                echo '<li class="list-group-item">
                                        <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-image" /></a>
                                        <a href="../'.$min.'" >'.$row['FANDOM'].'</a>
                                        <p class="pull-right">Fans: <a href="#">14</a></p>
                                    </li>';         

                            }
                        }
                        echo '</table>';
                        mysqli_close($con);            

                        ?>
                    </ul>
                </div>
            </div>
            <!-- Seccion 2-->
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/cars.php">Autos</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <?php
                        include '../link.php';
                        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                        $result = mysqli_query($con,"SELECT * FROM fandoms WHERE CATEGORY='Cars' ORDER BY CREATION DESC LIMIT 3");                

                        while($row = mysqli_fetch_array($result))
                        {
                            if($row['MODE'] == "fandom") 
                            {
                                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row2 = mysqli_fetch_array($result2);
                                $min = strtolower($row['FANDOM']);       

                                echo '<li class="list-group-item">
                                        <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-image" /></a>
                                        <a href="../'.$min.'" >'.$row['FANDOM'].'</a>
                                        <p class="pull-right">Fans: <a href="#">14</a></p>
                                    </li>';     

                            }elseif($row['MODE'] == "fans")
                            {
                                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row2 = mysqli_fetch_array($result2);
                                $min = strtolower($row['URL']);        

                                echo '<li class="list-group-item">
                                        <a href="../'.$min.'" ><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-image" /></a>
                                        <a href="../'.$min.'" >'.$row['FANDOM'].'</a>
                                        <p class="pull-right">Fans: <a href="#">14</a></p>
                                    </li>';         

                            }
                        }
                        echo '</table>';
                        mysqli_close($con);            

                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/science.php">Ciencia</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/cities.php">Ciudades</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/design.php">Diseño</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Seccion 3-->
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/entertainment.php">Entretenimiento</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/sports.php">Deportes</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/fancy.php">Fancy</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Seccion 4-->
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/fashion.php">Fashion y Outfits</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/photography.php">Fotografía</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/celebrities.php">Famosos</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Seccion 5-->
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/gossip.php">Gossip</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/hipster.php">Hipster</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/internet.php">Internet</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Seccion 6-->
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/industry.php">Industria</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/books.php">Libros</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/music.php">Música</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Seccion 7-->
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/nature.php">Naturaleza</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/movies.php">Películas</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/countries.php">Paises</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/tv.php">Televisión</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Seccion 8-->
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/vlogs.php">Vlogs</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">
                    <!-- Header de panel --> 
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="#"><img src="../fandomimages/17.jpg" class="fandom-image" /></a> <a href="../fandoms/videogames.php">Videojuegos</a>
                        </h3>
                    </div>         
                    <!-- Lista -->   
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                        <li class="list-group-item">
                            <a href="../watchdogs"><img src="../fandomimages/4.jpg" class="fandom-image" /></a>
                            <a href="../watchdogs">Watch Dogs</a>
                            <p class="pull-right">Fans: <a href="#">14</a></p>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
</body>
</html>