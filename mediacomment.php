<?php require 'sessions.php'; ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
	<title>Fan Plus Plus - Agregar Comentario</title>
	<meta name="description" content="Escribe en la caja de texto para insertar un comentario." />
    <?php include "favicon.php"; ?>
	<?php include '/home/jquery23/public_html/static/heads/headcolumn.php'; ?>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
<?php include "analyticstracking.php" ?>
    <header>
        <?php include '/home/jquery23/public_html/static/navs/nav.php'; ?>
    </header>
    <section id="contenedor">
    <?php
    if (isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['pic']) && !empty($_POST['pic']) && isset($_POST['body']) && !empty($_POST['body'])) 
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al concetar.");
        $pic = mysqli_real_escape_string($con, $_POST['pic']);
        $body = mysqli_real_escape_string($con, $_POST['body']);
        $date = date("Y-m-d");

        $sql = "INSERT INTO media_profilecomments(COMMENT,WHO_ID,PIC,FECHA) VALUES('".$body."','".$_SESSION['user_id']."','".$pic."','".$date."')";

        if (mysqli_query($con,$sql)) 
        {
            echo "INSERTADO con exito.";
            echo '<script> window.location="media.php?id='.$_POST['id'].'&pic='.$pic.'"; </script>'; 
        }else
        {
            echo "ERROR al insertar el comentario.";
        }

        mysqli_close($con);
    }elseif(isset($_POST['fandom']) && !empty($_POST['fandom']) && isset($_POST['pic']) && !empty($_POST['pic']) && isset($_POST['body']) && !empty($_POST['body']))
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al concetar.");
        $pic = mysqli_real_escape_string($con, $_POST['pic']);
        $body = mysqli_real_escape_string($con, $_POST['body']);
        $date = date("Y-m-d");
        
        $sql = "INSERT INTO media_fandomcomments(COMMENT,WHO_ID,PIC,FECHA) VALUES('$body','".$_SESSION['user_id']."','$pic','$date')";

        if (mysqli_query($con,$sql)) 
        {
            echo "INSERTADO con exito.";
            echo '<script> window.location="media.php?fandom='.$_POST['fandom'].'&pic='.$pic.'"; </script>'; 
        }else
        {
            echo "ERROR al insertar el comentario.";
        }

        mysqli_close($con);
    }else
    {
        echo 'Debes rellenar todos los campos.';
    }

    ?>
    </section>
    <?php include 'footer.php'; ?>
</body>
</html>