<?php
session_start();
?> 
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <title>Registrar un nuevo Fandom</title>
    <meta name="description" content="Fan Plus Plus es un comunidad donde puedes crear y unirte a Fandoms organizados por categorías."/>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "static/analyticstracking.php" ?>
    <!-- Inicio del Nav-->
    <?php include 'static/navs/nav.php'; ?>
    <!-- Fin del Nav-->

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-3 col-lg-3"></div>
            <div class="col-xs-12 col-sm-10 col-md-6 col-lg-6">
                <?php
                //CODIGO PARA VARIABLES DEL FORMULARIO 2
                //Codigo para validar todas las entradas
                $fansErr=$catErr="";    

                if($_SERVER["REQUEST_METHOD"] == "POST")
                {   
                    function test_input($data)
                    {
                        $data = trim($data);
                        $data = stripslashes($data);
                        $data = htmlspecialchars($data);
                        return $data;
                    } 

                    //Validando nombre del fandom
                    if(empty($_POST["fans"]))
                    {
                       $fansErr = "Nombre es un Campo Obligatorio";
                    }elseif(!preg_match("/^[a-zA-Z ]*$/",$_POST['fans']))//Caracteres permitidos
                    {
                        $fansErr = "Nombre: solo se permiten Letras";
                    }elseif(strlen($_POST['fans']) < 3)//Longitud mínima de 5 Caracteres
                    {
                        $fansErr = "Nombre: escribe 3 caracteres o más";
                    }else
                    {
                        //Revisando que el fandom no existe ya en la db
                        require 'link.php';
                        $con = mysqli_connect($host,$user,$pw,$db);

                        $ur = strtolower($_POST['fans']);
                        $url = str_replace(" ","-",$ur); 

                        $totalUsers = mysqli_num_rows(mysqli_query($con,"SELECT ID FROM profiles WHERE USERNAME='".mysqli_real_escape_string($con,$_POST["fans"])."'"));
                        $totalFandoms = mysqli_num_rows(mysqli_query($con,"SELECT ID FROM fandoms WHERE URL='".mysqli_real_escape_string($con,$url)."'"));    

                        if($totalUsers > 0)
                        {
                            $fansErr = "Este nombre de Fandom ya esta en uso, intenta con otro";
                        }elseif($totalFandoms > 0)
                        {
                            $fansErr = "Este nombre de Fandom ya esta en uso, intenta con otro";
                        }elseif(($totalUsers == 0) && ($totalFandoms == 0))
                        {
                            $fans = test_input($_POST['fans']);
                        }
                        mysqli_close($con);
                    }    

                    //Validando la categoria
                    if(empty($_POST['category']))
                    {
                        $catErr = "Debes seleccionar una categoría";
                    }else
                    {
                        $category = test_input($_POST['category']);
                    }    

                    //Validando que el archivo sea imagen
                    if(empty($_FILES['image']['name']))
                    {
                        $fileErr = "Debes seleccionar una imágen";
                    }else
                    {
                        $extPosibles = array("gif", "jpeg", "jpg", "png");//Extensiones de imagenes posible de subir al servidor
                        $aux = explode(".", $_FILES['image']['name']);
                        $extension = end($aux);//Llendo por la extension
                        //Formatos permitidos
                        if(!(($_FILES['image']['type'] == "image/gif") || ($_FILES['image']['type'] == "image/jpeg") || 
                            ($_FILES['image']['type'] == "image/jpg") || ($_FILES['image']['type'] == "image/pjpeg") || 
                            ($_FILES['image']['type'] == "image/x-png") || ($_FILES['image']['type'] == "image/png"))
                            && !in_array($extension, $extPosibles))
                        {
                            $fileErr = "El archivo debe ser una imágen";
                        }elseif($_FILES["file"]["size"] > 4000000)
                        {
                            $fileErr = "Imágen: Debe pesar menos de 5 Mb";
                        }else
                        {
                            $filename = $_FILES['image']['name'];
                        }
                    }     

                    //Validando la description
                    if(empty($_POST['description']))
                    {
                        $descriptionErr = "La primer foto del Fandom debe tener descripción";
                    }else
                    {
                        $description = test_input($_POST['description']);
                    }    

                }

                //Mostrando errores existentes
                if(isset($fansErr) && ($fansErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$fansErr.'</p>
                        </div>';
                }
                if(isset($catErr) && ($catErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$catErr.'</p>
                        </div>';
                }
                if(isset($fileErr) && ($fileErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$fileErr.'</p>
                        </div>';
                }
                if(isset($descriptionErr) && ($descriptionErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$descriptionErr.'</p>
                        </div>';
                }

                //CREACION DEL FANDOm
                //Creacion del modo fans
                if(isset($fans) && isset($category) && isset($description) && isset($filename))
                {
                    $nombre = "Fans de ".ucfirst($fans);
                    $temp = explode(".", $filename);//Se separa en forma de array por .
                    $ext = end($temp);//Se obtiene la extension
                    $photo_server = "fanaprroval/".md5($name).".".$ext;
                    $fecha = date("d/m/Y");

                    $con = mysqli_connect($host,$user,$pw,$db);
                    $sql = "INSERT INTO fandom_approval(FANDOM,CATEGORY,DESCRIPTION,IMG,USER_ID,MODE,FECHA) VALUES('".$nombre."','".$category."','".$description."','".$photo_server."','".$_SESSION['user_id']."','fans','".$fecha."') ";

                    //INSERTANDO DATOS EN DB
                    if(mysqli_query($con,$sql))
                    {
                        $step = 1;
                    }

                    //COPIANDO FOTO AL SERVIDOR 
                    if($_FILES['image']['type'] == "image/gif" || $_FILES['image']['type'] == "image/jpg" || $_FILES['image']['type'] == "image/png" 
                        || $_FILES['image']['type'] == "image/jpeg" && $_FILES['image']['error'] == 0)
                    {
                        if(move_uploaded_file($_FILES['image']['tmp_name'], $photo_server) )
                        {
                            $step++;
                        }
                    }else
                    {
                        echo '<div class="alert alert-warning"><p>Debes seleccionar por lo menos una imagen a subir.</p></div>';
                    }      

                    //Si se cumplieron todos los pasos se muestra el mensaje de exito
                    if($step == 2)
                    {
                        echo '<div class="alert alert-success fade in" role="alert">
                            <p>El Fandom tiene que ser aprobado te enviaremos un email cuando sea creado, estara en tu sandbox.</p>
                        </div>';
                    }else
                    {
                        echo '<div class="alert alert-success fade in" role="alert">
                            <p>Error al crear Fandom contáctanos.</p>
                        </div>';
                    }

                    //Limpiando formulario
                    $fandom=$category=$description=$filename="";

                }

                //FORMULARIO
                if(isset($_SESSION['username']) && isset($_SESSION['user_id']) && $_SESSION['user_id']==1)
                {
                    echo '<!-- Formulario 2 -->
                        <h1>De la Forma: Fans de...</h1>
                        <div class="btn-group">
                            <a class="btn btn-default" href="newfan.php" role="button">Fandom</a>
                            <a class="btn btn-default btn-select" href="newfn.php" role="button">Fans</a>
                        </div>
                        <form role="form" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="registerName">Fans de:</label>
                                <input name="fans" value="'.$fans.'" type="text" class="form-control" id="registerName" placeholder="ej. Robert Pattison">
                            </div>
                            <div class="form-group">
                                <label>Categoría:</label>
                                <select name="category" class="form-control">
                                    <option value="'.$category.'">'.$category.'</option>
                                    <option value="Art">Arte</option>
                                    <option value="Architecture">Arquitectura</option>
                                    <option value="Food">Alimentos</option>
                                    <option value="Cars">Coches y Motocicletas</option>
                                    <option value="Science">Ciencia</option>
                                    <option value="Cities">Ciudades</option>
                                    <option value="Design">Diseño</option>
                                    <option value="Entertainment">Entretenimiento</option>
                                    <option value="Sports">Deportes</option>
                                    <option value="Fancy">Fancy</option>
                                    <option value="Fashion">Fashion And Outfits</option>
                                    <option value="Photography">Fotografia</option>
                                    <option value="Celebrities">Famosos</option>
                                    <option value="Gossip">Gossip</option>
                                    <option value="Hipster">Hipster</option>
                                    <option value="Internet">Internet</option>
                                    <option value="Industry">Industria</option>
                                    <option value="Books">Libros</option>
                                    <option value="Music">Música</option>
                                    <option value="Nature">Naturaleza</option>
                                    <option value="Countries">Paises</option>
                                    <option value="Movies">Películas</option>
                                    <option value="TV">Television</option>
                                    <option value="Vlogs">Vlogs</option>
                                    <option value="Videogames">Videojuegos</option>
                                </select>        

                            </div>
                            <div class="form-group">
                                <input name="image" type="file" title="Seleccionar Imágen">
                            </div>
                            <div class="form-group">
                                <label>Descripción de la imágen:</label>
                                <textarea name="description" class="form-control" rows="3">'.$description.'</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Crear</button>
                        </form>
                        <!-- Formulario 2 -->';
                }else 
                {
                    echo '<div class="alert alert-info"><p>Debes <a href="signin.php">Iniciar Sesión</a> para crear un fandom.</p></div>';   
                }

                ?>

            </div>
            <div class="col-xs-12 col-sm-1 col-md-3 col-lg-3"></div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <script src="static/js/file-input.js"></script>
    <!-- File input script -->
    <script>
    $('input[type=file]').bootstrapFileInput();
    $('.file-inputs').bootstrapFileInput();
    </script>
</body>
</html>