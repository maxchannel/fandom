<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Notificaciones en Fan Plus Plus</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Tus notificaciones en Fan Plus Plus"/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <!-- Fuentes permitidas-->
    <link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Crafty+Girls' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Devonshire' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Electrolize' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Fanwood+Text' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Kite+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway+Dots' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=The+Girl+Next+Door' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Walter+Turncoat' rel='stylesheet' type='text/css'>
    <!-- Fuentes permitidas-->
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-3">
                <div class="list-group">
                    <a href="/notifications" class="list-group-item active">En Perfil</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
                <?php 

                if(isset($_SESSION['user_id'])) 
                {
                    echo '<h1>Notificaciones</h1>';
                    require '../link.php';
                    $con = mysqli_connect($host,$user,$pw,$db);
                    $result = mysqli_query($con,"SELECT * FROM notifications_profile WHERE USER_ID='".$_SESSION['user_id']."' ORDER BY FECHA DESC");    

                    if(mysqli_num_rows($result) == 0)//Sin notificaciones
                    {
                        echo '<div class="panel panel-default">
                                <div class="panel-body">
                                    <p>Sin notificaciones</p>
                                </div>
                            </div>';
                    }else
                    {
                        //Actualizando todos los estados no visto cuando el usuario entre aquí
                        mysqli_query($con,"UPDATE notifications_profile SET VIEW=1 WHERE VIEW='0' AND USER_ID='".$_SESSION['user_id']."' ");

                        while($row = mysqli_fetch_array($result))
                        {
                            switch ($row['CODE']) 
                            {
                                case '1'://Usuario sigue a Usuario
                                    $result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                                    $row2 = mysqli_fetch_array($result2);
                                    echo '<div class="panel panel-default">
                                            <div class="panel-body">
                                                <p><a href="../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a> te siguió, el día: '.$row['FECHA'].'</p>
                                            </div>
                                        </div>';
                                    break;

                                case '2'://Usuario publica a Usuario
                                    $result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                                    $row2 = mysqli_fetch_array($result2);
                                    echo '<div class="panel panel-default">
                                            <div class="panel-body">
                                                <p><a href="../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a> hizo una <a href="../'.$_SESSION['username'].'/publications/'.$row['POST_ID'].'">publicación</a> en tu perfil , el día: '.$row['FECHA'].'</p>
                                            </div>
                                        </div>';
                                    break;

                                case '3'://Usuario deja nota a Usuario
                                    $result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                                    $row2 = mysqli_fetch_array($result2);
                                    echo '<div class="panel panel-default">
                                            <div class="panel-body">
                                                <p><a href="../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a> te dejo <a href="../'.$_SESSION['username'].'/notes/'.$row['POST_ID'].'">una nota</a> en tu perfil , el día: '.$row['FECHA'].'</p>
                                            </div>
                                        </div>';
                                    break;

                                case '4'://Usuario publica en Fandom
                                    $result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                                    $row2 = mysqli_fetch_array($result2);
                                    $result3 = mysqli_query($con,"SELECT FANDOM,MODE,URL FROM fandoms WHERE ID='".$row['FANDOM_ID']."' ");
                                    $row3 = mysqli_fetch_array($result3);

                                    if($row3['MODE'] == 'fans')
                                    {
                                        $url = $row3['URL'];
                                    }elseif($row3['MODE'] == 'fandom')
                                    {
                                        $url = strtolower($row3['FANDOM']);
                                    }

                                    echo '<div class="panel panel-default">
                                            <div class="panel-body">
                                                <p><a href="../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a> hizo una <a href="../'.$url.'/publications/'.$row['POST_ID'].'">publicación</a> en <a href="../'.$url.'" >'.$row3['FANDOM'].'</a>, el día: '.$row['FECHA'].'</p>
                                            </div>
                                        </div>';
                                    break;

                                case '5'://Usuario deja nota en Fandom
                                    $result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                                    $row2 = mysqli_fetch_array($result2);
                                    $result3 = mysqli_query($con,"SELECT FANDOM,MODE,URL FROM fandoms WHERE ID='".$row['FANDOM_ID']."' ");
                                    $row3 = mysqli_fetch_array($result3);

                                    if($row3['MODE'] == 'fans')
                                    {
                                        $url = $row3['URL'];
                                    }elseif($row3['MODE'] == 'fandom')
                                    {
                                        $url = strtolower($row3['FANDOM']);
                                    }

                                    echo '<div class="panel panel-default">
                                            <div class="panel-body">
                                                <p><a href="../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a> dejo una <a href="../'.$url.'/notes/'.$row['POST_ID'].'">nota</a> en <a href="../'.$url.'" >'.$row3['FANDOM'].'</a>, el día: '.$row['FECHA'].'</p>
                                            </div>
                                        </div>';
                                    break;

                                case '6'://Usuario sube imagen a Fandom
                                    $result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                                    $row2 = mysqli_fetch_array($result2);
                                    $result3 = mysqli_query($con,"SELECT FANDOM,MODE,URL FROM fandoms WHERE ID='".$row['FANDOM_ID']."' ");
                                    $row3 = mysqli_fetch_array($result3);

                                    if($row3['MODE'] == 'fans')
                                    {
                                        $url = $row3['URL'];
                                    }elseif($row3['MODE'] == 'fandom')
                                    {
                                        $url = strtolower($row3['FANDOM']);
                                    }

                                    echo '<div class="panel panel-default">
                                            <div class="panel-body">
                                                <p><a href="../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a> publicó una <a href="../media?fandom='.$row['FANDOM_ID'].'&pic='.$row['POST_ID'].'">imágen</a> en <a href="../'.$url.'" >'.$row3['FANDOM'].'</a>, el día: '.$row['FECHA'].'</p>
                                            </div>
                                        </div>';
                                    break;

                                case '7'://Usuario se unio a Fandom, inserta en drivers/ajax/befan.php
                                    $result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                                    $row2 = mysqli_fetch_array($result2);
                                    $result3 = mysqli_query($con,"SELECT FANDOM,MODE,URL FROM fandoms WHERE ID='".$row['FANDOM_ID']."' ");
                                    $row3 = mysqli_fetch_array($result3);

                                    if($row3['MODE'] == 'fans')
                                    {
                                        $url = $row3['URL'];
                                    }elseif($row3['MODE'] == 'fandom')
                                    {
                                        $url = strtolower($row3['FANDOM']);
                                    }

                                    echo '<div class="panel panel-default">
                                            <div class="panel-body">
                                                <p><a href="../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a> se unió a <a href="../'.$url.'" >'.$row3['FANDOM'].'</a>, el día: '.$row['FECHA'].'</p>
                                            </div>
                                        </div>';
                                    break;
                                
                                default:
                                    echo 'Sin valores aún.';
                                    break;
                            }

                        }
                    }
                }else
                {
                    echo '<div class="alert alert-warning" role="alert">
                            <p>Debes <a href="../signin">Iniciar Sesión</a> o <a href="../signup">Registrarte</a> para ver este contenido.</p>
                        </div>';
                }


                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Perfiles para seguir</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?php
                            require '../drivers/atlantis.php';
                            $index = new Activity;
                            $index->randomProfiles();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
    <script src="../static/js/fluid.js"></script>
</body>
</html>