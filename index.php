<?php
session_start();
?> 
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <meta name="description" content="Fan Plus Plus es un comunidad donde puedes crear y unirte a Fandoms organizados por categor&iacute;as."/>
    <link rel="stylesheet" type="text/css" href="static/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="static/css/estilos.css">
    <link href="static/css/sticky-footer-navbar.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <script src="static/js/moment.js"></script>
    <!-- Fuentes permitidas-->
    <link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Crafty+Girls' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Devonshire' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Electrolize' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Fanwood+Text' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Kite+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway+Dots' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=The+Girl+Next+Door' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Walter+Turncoat' rel='stylesheet' type='text/css'>
    <!-- Fuentes permitidas-->
    <script>
    moment.lang('es');
    </script>
</head>
<body>
    <?php include "static/analyticstracking.php" ?>
    <?php include 'static/navs/nav.php'; ?>

    <?php
    if(!isset($_SESSION['username']) && !isset($_SESSION['user_id'])) 
    {
        //Si el usuario no ha iniciado session le mandamos este index
        echo '<!-- Inicio de contenido -->
            <div class="container-fluid">
                <!-- Row 1 -->
                <section class="row">

                <!-- Modal para enviar error-->                    
                <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Informar sobre error</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para error-->
                <!-- Modal de reporte de publicacion-->                   
                <div class="modal fade bs-example-modal-sm-report-p" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar una Publicación</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Contenido Ofensivo</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal de reporte de publicacion-->
                <!-- Modal de reporte de nota-->                   
                <div class="modal fade bs-example-modal-sm-report-n" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Nota</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Contenido Ofensivo</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal de reporte de nota-->
                <!-- Modal para reportar imagen -->                   
                <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Imágen de Max</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Spam</option>
                                            <option>Usuario Molesto</option>
                                            <option>Apología a la Violencia</option>
                                            <option>Desnudez</option>
                                            <option>Derechos de Autor</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para reportar imagen --> 
                <!--Primer columna del row 1, formulario de inicio de sesion --> 
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <!-- Sign in -->
                    <article class="post">
                        <form role="form" action="signin" method="post">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Usuario</label>
                                <input name="username" type="text" class="form-control" id="exampleInputEmail1" placeholder="Usuario">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Contraseña</label>
                                <input name="pw" type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                            </div>
                            <button type="submit" class="btn btn-primary">Entrar</button>
                        </form>
                    </article>
                </div>
                <!-- Columna 2 de texto-->
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <article class="post">
                        <p class="lead">Fan Plus Plus es una red donde te unes a Fandoms para compartir comentarios, imágenes y videos con otras personas que comparten tus gustos.</p>
                        <p class="lead">¿Existirán Fans de #JenniferLawrence Mexicanas? ahora puedes saberlo. </p>
                    </article>
                </div>
                <!-- Columna 3 de reguistro de usuario -->
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <!-- Sign up -->
                    <article class="post">
                        <form role="form" action="signup" method="post">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nombre Completo</label>
                                <input name="firstname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Nombre para mostrar">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Contraseña</label>
                                <input name="pw2" type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                            </div>
                            <button type="submit" class="btn btn-warning black-text">Registrarme</button>
                        </form>
                    </article>
                </div>
            </section>
            
        </div>
        <!-- Fin de contenido -->';
        include 'static/footer.php';
    }else
    {
        echo '<!-- Inicio de contenido -->
            <div class="container-fluid">
                <section class="row">
                    <!-- Modal para enviar error-->                    
                    <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Informar sobre error</h4>
                                </div>
                                <div class="modal-body">
                                    <form role="form">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal para error--> 
                    <!-- Modal para reportar a fandom-->                   
                    <div class="modal fade bs-example-modal-sm-report-f" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Reportar Fandom</h4>
                                </div>
                                <div class="modal-body">
                                    <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Fandom Repetido</option>
                                        <option>Contenido no Apto</option>
                                    </select>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal para reportar a fandom-->
                    <!-- Modal para reportar a usuario-->                   
                    <div class="modal fade bs-example-modal-sm-report-u" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Reportar Usuario</h4>
                                </div>
                                <div class="modal-body">
                                    <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Usuario Molesto</option>
                                        <option>Contenido no Apto</option>
                                    </select>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal para reportar a usuario-->
                    <!-- Modal de reporte de publicacion-->                   
                    <div class="modal fade bs-example-modal-sm-report-p" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Reportar una Publicación</h4>
                                </div>
                                <div class="modal-body">
                                    <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Contenido Ofensivo</option>
                                        <option>Contenido no Apto</option>
                                    </select>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal de reporte de publicacion-->
                    <!-- Modal de reporte de nota-->                   
                    <div class="modal fade bs-example-modal-sm-report-n" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Reportar Nota</h4>
                                </div>
                                <div class="modal-body">
                                    <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Contenido Ofensivo</option>
                                        <option>Contenido no Apto</option>
                                    </select>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal de reporte de nota-->
                    <!-- Modal para ver fotos de resultados-->                    
                    <div class="modal fade bs-example-modal-sm-profileimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Imágen de Max</h4>
                                </div>
                                <div class="modal-body">
                                    <img src="../images/13.jpg" class="img-responsive">
                                </div>
                                <div class="modal-footer">
                                    <p>Hola mundo</p>
                                    <button type="button" class="btn btn-primary">Sandbox</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal para ver fotos de resultados-->        

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Fandoms Populares</div>
                            <div class="panel-body">
                                <div class="row">';
                                //Fandoms populares traidos con la clase Activity
                                require 'drivers/atlantis.php';
                                $index = new Activity;
                                $index->popularFandoms();
echo '                          </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h2>Feed de Actividad
                        </h2>
                        <div class="row">
                            <!-- Publicacion 1-->
                            <div class="col-md-12 col-lg-12">
                                <div class="panel panel-default post-panel">
                                    <div class="panel-heading">
                                        <a href="max"><img src="profilesavatar/max.jpg" class="post-inside-image" /></a>
                                        <a href="max">Max</a>
                                        <p class="pull-right"><a href="#">
                                        <script>document.writeln(moment("11/07/2014 20:14", "DDMMYYYY h:mm").fromNow() + "<br>");</script>
                                        </a></p>
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, animi, provident esse voluptates distinctio modi cum. Veritatis, minima, impedit, alias, pariatur at eos voluptas officiis adipisci neque sit beatae blanditiis!</p>
                                    </div>
                                    <div class="panel-footer">
                                        <button type="button" class="btn btn-primary">Sandbox</button>
                                        <!-- Boton dropdown-->
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Publicacion 2-->
                            <div class="col-md-12 col-lg-12">
                                <div class="panel panel-default post-panel">
                                    <div class="panel-heading">
                                        <a href="max"><img src="profilesavatar/max.jpg" class="post-inside-image" /></a>
                                        <a href="max">Max</a>
                                        <p class="pull-right"><a href="#">16.05.2014</a></p>
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, animi, provident esse voluptates distinctio modi cum. Veritatis, minima, impedit, alias, pariatur at eos voluptas officiis adipisci neque sit beatae blanditiis!</p>
                                    </div>
                                    <div class="panel-footer">
                                        <button type="button" class="btn btn-primary">Sandbox</button>
                                        <!-- Boton dropdown-->
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Nota 1 -->
                            <div class="col-md-12 col-lg-12">
                                <div class="panel panel-default post-panel">
                                    <div class="panel-heading">
                                        <a href="max"><img src="profilesavatar/max.jpg" class="post-inside-image" /></a>
                                        <a href="max">Max</a>
                                        <p class="pull-right"><a href="#">16.05.2014</a></p>
                                    </div>
                                    <div class="panel-body">
                                        <p class="font1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, harum, impedit, et, omnis quo adipisci illo a explicabo non minus natus libero nesciunt animi atque totam! Ducimus, laudantium vel temporibus!</p>
                                    </div>
                                    <div class="panel-footer">
                                        <button type="button" class="btn btn-primary">Sandbox</button>
                                        <!-- Boton dropdown-->
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Nota 2 -->
                            <div class="col-md-12 col-lg-12">
                                <div class="panel panel-default post-panel">
                                    <div class="panel-heading">
                                        <a href="max"><img src="profilesavatar/max.jpg" class="post-inside-image" /></a>
                                        <a href="max">Max</a>
                                        <p class="pull-right"><a href="#">16.05.2014</a></p>
                                    </div>
                                    <div class="panel-body">
                                        <p class="font1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, harum, impedit, et, omnis quo adipisci illo a explicabo non minus natus libero nesciunt animi atque totam! Ducimus, laudantium vel temporibus!</p>
                                    </div>
                                    <div class="panel-footer">
                                        <button type="button" class="btn btn-primary">Sandbox</button>
                                        <!-- Boton dropdown-->
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Imágen 1 -->
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="panel panel-default">
                                    <a href="" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg"><img src="fandomimages/a2252ccb407cc83111e7970a740e17d4.jpg" class="img-responsive"></a>
                                    <div class="panel-footer">
                                        Añadida el: 20.04.2014
                                    </div>
                                </div>
                            </div>
                            <!-- Imágen 2 -->
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="panel panel-default">
                                    <a href="" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg"><img src="fandomimages/a2252ccb407cc83111e7970a740e17d4.jpg" class="img-responsive"></a>
                                    <div class="panel-footer">
                                        Añadida el: 20.04.2014
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="panel panel-default">
                                    <a href="" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg"><img src="fandomimages/a2252ccb407cc83111e7970a740e17d4.jpg" class="img-responsive"></a>
                                    <div class="panel-footer">
                                        Añadida el: 20.04.2014
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Personas para seguir</div>
                            <div class="panel-body">
                                <div class="row">';
                                    $index->randomProfiles();
echo '                          </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>&copy; 2014 Fanplusplus &middot; <a href="help.html" >Ayuda</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!-- Fin de contenido -->';
    }

    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <script src="static/js/fluid.js"></script>
</body>
</html>