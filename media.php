<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <meta name="description" content="Fundador de Fan Plus Plus"/>
    <?php
    //Funcion que sanitiza por seguridad
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
        
    //Descidiendo que mostrar en el title
    if(isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['pic']) && !empty($_GET['pic']) && is_numeric($_GET['pic']))
    {
        $id = test_input($_GET['id']);//ID del Profile
        $pic = test_input($_GET['pic']);//ID de la foto
        
        require_once "drivers/columbia.php";
        $media = new Usuario($id);
        
        include 'link.php'; 
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
        $result0 = mysqli_query($con,"SELECT * FROM profile_photos WHERE ID='".mysqli_real_escape_string($con,$pic)."' AND USER_ID='".mysqli_real_escape_string($con,$id)."' ") or die("Problemas al conectar.");
        if(mysqli_num_rows($result0) == 1)
        {
            $row0 = mysqli_fetch_array($result0);
        }
        
        echo "<title>";
        echo $row0['PHOTO_NAME'];
        echo " - ";
        $media->getLucky("FIRSTNAME");
        echo "</title>";
        echo "\n";
        mysqli_close($con);

    }elseif(isset($_GET['fandom']) && !empty($_GET['fandom']) && is_numeric($_GET['fandom']) && isset($_GET['pic']) && !empty($_GET['pic']) && is_numeric($_GET['pic']))
    {
        $fandom = test_input($_GET['fandom']); //ID del Fandom
        $pic = test_input($_GET['pic']);//ID de la foto
        
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
        $result0 = mysqli_query($con,"SELECT * FROM fandom_images WHERE ID='".mysqli_real_escape_string($con,$pic)."' AND FANDOM_ID='".mysqli_real_escape_string($con,$fandom)."'") or die("Problemas con el query.");
        if(mysqli_num_rows($result0) > 0)
        {
            $row0 = mysqli_fetch_array($result0);
        }
            
        require_once "drivers/houston.php";
        $media = new Fandom($fandom);
        echo "<title>";
        echo $row0['PHOTO_NAME'];
        echo " - ";
        $media->getLucky("NAME");
        echo  "</title>";
        echo "\n";
        mysqli_close($con);
    }else
    {
        echo '<title>Contenido no disponible - Fan Plus Plus</title>';
    }
    
    ?>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <!-- JavaScript SDK -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- JavaScript SDK -->

    <!-- Inicio del Nav-->
    <?php include "static/analyticstracking.php" ?>
    <?php
    if(isset($_SESSION['username']))
    {
        echo '<!-- Inicio del Nav-->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://www.fanplusplus.com">F++</a>
            </div>    
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="http://www.fanplusplus.com/fandoms">Fandoms</a></li>
                </ul>
                <form action="http://www.fanplusplus.com/search.php" method="get" class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input name="q" type="text" class="form-control" placeholder="Encuentra cosas">
                    </div>
                    <button type="submit" class="btn btn-default">Buscar</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a href="http://www.fanplusplus.com/sandbox">Sandbox <span class="badge">3</span></a>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">';
                        require_once "drivers/columbia.php";
                        $navIns = new Usuario($_SESSION['user_id']);
                        $navIns->getLucky("FIRSTNAME");
                        echo '<b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="http://www.fanplusplus.com/';
                            $navIns->getLucky("USERNAME");
                            echo '">Perfil</a></li>
                            <li><a href="http://www.fanplusplus.com/invite.php">Invitar Amigos</a></li>
                            <li class="divider"></li>
                            <li><a href="http://www.fanplusplus.com/settings"><span class="glyphicon glyphicon-cog"></span> Configuración</a></li>
                        </ul>
                    </li>
                    <li><a href="http://www.fanplusplus.com/logout.php">Cerrar Sesión</a></li>
                </ul>
            </div>
        </nav>
        <!-- Fin del Nav-->';
    }else
    {
        echo '<!-- Inicio del Nav-->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://www.fanplusplus.com">F++</a>
            </div>    
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="http://www.fanplusplus.com/fandoms">Fandoms</a></li>
                </ul>
                <form action="http://www.fanplusplus.com/search.php" method="get" class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input name="q" type="text" class="form-control" placeholder="Encuentra cosas">
                    </div>
                    <button type="submit" class="btn btn-default">Buscar</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://www.fanplusplus.com/signin.php">Iniciar Sesión</a></li>
                </ul>
            </div>
        </nav>
        <!-- Fin del Nav-->';
    }
    ?>
    <!-- Fin del Nav-->

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <!-- Ads y perfil-->
        <section class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="row">
                            <!-- Modal para reportar a usuario-->                   
                            <div class="modal fade bs-example-modal-sm-report-u" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Reportar Usuario</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form role="form">
                                                <div class="form-group">
                                                    <select class="form-control">
                                                        <option>Spam</option>
                                                        <option>Usuario Molesto</option>
                                                        <option>Contenido no Apto</option>
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="button" class="btn btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal para reportar a usuario--> 
                            <!-- Modal para enviar error-->                    
                            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Informar sobre error</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form role="form">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="3"></textarea>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="button" class="btn btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal para error-->  
                            <!-- Modal para reportar imagen de media -->                   
                            <div class="modal fade bs-example-modal-sm-report-media" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Reportar Imágen de Max</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form role="form">
                                                <div class="form-group">
                                                    <select class="form-control">
                                                        <option>Spam</option>
                                                        <option>Usuario Molesto</option>
                                                        <option>Apología a la Violencia</option>
                                                        <option>Desnudez</option>
                                                        <option>Derechos de Autor</option>
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="button" class="btn btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal para reportar imagen de media -->  

                            <!-- Perfil de quien subio la foto-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <?php
                                //Mostrando avatar e info del perfil para seguir
                                if(isset($id) && !empty($id) && is_numeric($id) && isset($pic) && !empty($pic) && is_numeric($pic))
                                {
                                    echo '<div class="panel panel-default fandom-panel">
                                            <div class="panel-body">
                                                <a href="';
                                    $media->getLucky("USERNAME");
                                    echo '"><img src="';
                                    $media->getLucky("AVATAR");
                                    echo '" class="profile-inside-image" /></a>
                                                <p class="fandom-title"><a href="';
                                    $media->getLucky("USERNAME");
                                    echo '">';
                                    $media->getLucky("FIRSTNAME");
                                    echo '</a></p>
                                            </div>
                                            <div class="panel-footer">';
                                                if (isset($_SESSION['username']) && ($media->username != $_SESSION['username']) ) 
                                                {
                                                    $media->ifFollow($media->id, $_SESSION['user_id']);
                                                }elseif($media->username == $_SESSION['username'])
                                                {
                                                    echo '<a href="../../'.$_SESSION['username'].'" class="btn btn-primary">Mi Perfil</a>';
                                                }else
                                                {
                                                    echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                                                }
                                    echo '      <!-- Boton dropdown-->
                                                <div class="btn-group pull-right">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>
                                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                    </ul>
                                                </div>      
                                            </div>
                                        </div>';
                                }elseif(isset($fandom) && !empty($fandom) && is_numeric($fandom) && isset($pic) && !empty($pic) && is_numeric($pic))
                                {
                                    echo '<div class="panel panel-default fandom-panel">
                                            <div class="panel-body">
                                                <a href="'.$media->getLucky("RETURNURL").'"><img src="'.$media->getLucky("RETURNAVATAR").'" class="profile-inside-image" /></a>
                                                <p class="fandom-title"><a href="'.$media->getLucky("RETURNURL").'">'.$media->getLucky("RETURNNAME").'</a></p>
                                            </div>
                                            <div class="panel-footer">';
                                                if (isset($_SESSION['username']))
                                                {
                                                    $media->ifFan($media->id, $_SESSION['user_id']);
                                                }else
                                                {
                                                    echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                                                }
                                    echo '      <!-- Boton dropdown-->
                                                <div class="btn-group pull-right">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>
                                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                    </ul>
                                                </div>      
                                            </div>
                                        </div>';
                                }

                                ?>
                            </div>
                            <!-- Ads -->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default hidden-xs hidden-sm">
                                    <div class="panel-body">
                                        Ads
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                            <!-- 728x90, creado 21/05/11 -->
                                            <ins class="adsbygoogle"
                                                 style="display:inline-block;width:728px;height:90px"
                                                 data-ad-client="ca-pub-8002585975415354"
                                                 data-ad-slot="2717762212"></ins>
                                            <script>
                                            (adsbygoogle = window.adsbygoogle || []).push({});
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <?php
                                        //Botonera de siguiente y anterior
                                        if(isset($fandom) && !empty($fandom) && is_numeric($fandom) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");

                                            //Foto anterior
                                            $result2 = mysqli_query($con,"SELECT * FROM fandom_images WHERE ID<".mysqli_real_escape_string($con,$pic)." AND FANDOM_ID='".mysqli_real_escape_string($con,$fandom)."' ORDER BY ID DESC LIMIT 1");
                                            if(mysqli_num_rows($result2) == 1) 
                                            {
                                                $row2 = mysqli_fetch_array($result2);
                                                echo '<a href="media?fandom='.$row2['FANDOM_ID'].'&pic='.$row2['ID'].'" class="btn btn-default pull-left">Anterior</a>';
                                            }
                                            
                                            //Foto siguiente
                                            $result3 = mysqli_query($con,"SELECT * FROM fandom_images WHERE ID>".mysqli_real_escape_string($con,$pic)." AND FANDOM_ID='".mysqli_real_escape_string($con,$fandom)."' ORDER BY ID ASC LIMIT 1");
                                            if(mysqli_num_rows($result3) == 1) 
                                            {
                                                $row3 = mysqli_fetch_array($result3);
                                                echo '<a href="media?fandom='.$row3['FANDOM_ID'].'&pic='.$row3['ID'].'" class="btn btn-default pull-right">Siguiente</a>';

                                            } 
                                        }elseif(isset($id) && !empty($id) && is_numeric($id) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");

                                            //Foto anterior a la actual
                                            $result2 = mysqli_query($con,"SELECT * FROM profile_photos WHERE ID<".mysqli_real_escape_string($con,$pic)." AND USER_ID='".mysqli_real_escape_string($con,$id)."' ORDER BY ID DESC LIMIT 1");
                                            $row2 = mysqli_fetch_array($result2);
                                            if(mysqli_num_rows($result2) == 1) 
                                            {
                                                echo '<a href="media?id='.$row2['USER_ID'].'&pic='.$row2['ID'].'" class="btn btn-default pull-left">Anterior</a>';
                                            }
                                            //Foto siguiente
                                            $result3 = mysqli_query($con,"SELECT * FROM profile_photos WHERE ID>".mysqli_real_escape_string($con,$pic)." AND USER_ID='".mysqli_real_escape_string($con,$id)."' ORDER BY ID ASC LIMIT 1");
                                            $row3 = mysqli_fetch_array($result3);
                                            if(mysqli_num_rows($result3) == 1) 
                                            {
                                                echo '<a href="media?id='.$row3['USER_ID'].'&pic='.$row3['ID'].'" class="btn btn-default pull-right">Siguiente</a>';
                                            }
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <?php
                                        //Trayendo imagen para el modo pelicula
                                        if(isset($id) && !empty($id) && is_numeric($id) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            include 'link.php'; 
                                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                                    
                                            $result = mysqli_query($con,"SELECT * FROM profile_photos WHERE ID='".mysqli_real_escape_string($con,$pic)."' AND USER_ID='".mysqli_real_escape_string($con,$id)."' ") or die("Problemas al conectar.");
                                            $row = mysqli_fetch_array($result);
                                            //Mostrando imagen
                                            echo '<img src="'.$row['PHOTO_SERVER'].'" class="img-responsive center">';
                                            //Modal para modo pelicula
                                            echo '<!-- Modal para ver foto en pelicula-->                    
                                                    <div class="modal fade bs-example-modal-sm-mediaimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title">Imágen de ';
                                                                    $media->getLucky("FIRSTNAME");
                                            echo '                  </h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <img src="'.$row['PHOTO_SERVER'].'" class="img-responsive center">
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <p>'.$row['PHOTO_NAME'].'</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal para ver foto en pelicula-->';

                                        //Mostrando las fotos para fandoms
                                        }elseif(isset($fandom) && !empty($fandom) && is_numeric($fandom) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");                         

                                            $result = mysqli_query($con,"SELECT * FROM fandom_images WHERE ID='".mysqli_real_escape_string($con,$pic)."' AND FANDOM_ID='".mysqli_real_escape_string($con,$fandom)."'") or die("Problemas con el query.");
                                            if(mysqli_num_rows($result) == 1)
                                            {
                                                $row = mysqli_fetch_array($result);
                                                //Mostrando imagen
                                                echo '<img src="'.$row['PHOTO_SERVER'].'" class="img-responsive">';
                                                //Modal para modo pelicula
                                                echo '<!-- Modal para ver foto en pelicula-->                    
                                                        <div class="modal fade bs-example-modal-sm-mediaimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title">Imágen de ';
                                                                        $media->getLucky("NAME");
                                                echo '                  </h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <img src="'.$row['PHOTO_SERVER'].'" class="img-responsive">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <p>'.$row['PHOTO_NAME'].'</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modal para ver foto en pelicula-->';
                                            }
                                        }else 
                                        {
                                            echo '<p>Contenido no disponible.</p>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                //Mostrando el via fandom en caso de que la pic sea de fandoms
                                if(isset($fandom) && !empty($fandom) && is_numeric($fandom) && isset($pic) && !empty($pic) && is_numeric($pic))
                                {
                                    //Seleccionando quien subio la foro
                                    $resultV = mysqli_query($con, "SELECT USER_ID FROM fandom_images WHERE ID='".$pic."' ");
                                    $rowV = mysqli_fetch_array($resultV);
                                    //Llendo por el perfil
                                    $resultV2 = mysqli_query($con, "SELECT FIRSTNAME,AVATAR,USERNAME FROM profiles WHERE ID='".$rowV['USER_ID']."' ");
                                    $rowV2 = mysqli_fetch_array($resultV2);

                                    echo '<div class="row">
                                            <div class="col-md-6">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <a href="'.$rowV2['USERNAME'].'"><img src="'.$rowV2['AVATAR'].'" class="profile-inside-image" /></a>
                                                        <p><a href="'.$rowV2['USERNAME'].'">'.$rowV2['FIRSTNAME'].'</a></p>
                                                    </div>
                                                    <div class="panel-footer">Subido por</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <a href="'.$media->getLucky("RETURNURL").'"><img src="'.$media->getLucky("RETURNAVATAR").'" class="profile-inside-image" /></a>
                                                        <p><a href="'.$media->getLucky("RETURNURL").'">'.$media->getLucky("RETURNNAME").'</a></p>
                                                    </div>
                                                    <div class="panel-footer">Via el Fandom</div>
                                                </div>
                                            </div>
                                        </div>';
                                }

                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <!-- Boton dropdown-->
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-media">Reportar Esta Imágen</a></li>
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                            </ul>
                                        </div>  
                                        <?php
                                        echo '<p>Descripción: '.$row['PHOTO_NAME'].'</p>';
                                        ?>
                                        <p><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-mediaimg">Modo Película</a></p>
                                        <!-- Fb button -->
                                        <?php
                                        //Boton de fb
                                        if(isset($fandom) && !empty($fandom) && is_numeric($fandom) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            echo '<div class="fb-like" data-href="http://www.fanplusplus.com/media.php?fandom='.$fandom.'&pic='.$pic.'" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>';
                                        }elseif(isset($id) && !empty($id) && is_numeric($id) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            echo '<div class="fb-like" data-href="http://www.fanplusplus.com/media.php?id='.$id.'&pic='.$pic.'" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>';
                                        }

                                        ?>
                                        <!-- TW button -->
                                        <div class="btn-social">
                                            <a href="https://twitter.com/share" class="twitter-share-button" data-related="jasoncosta" data-lang="en" data-size="large" data-count="none">Tweet</a>
                                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                        </div>
                                        <?php
                                        require 'drivers/media.php';
                                        $move = new Cross;

                                        //Boton de sandbox, decidiendo que modo mostrar
                                        if(isset($id) && !empty($id) && is_numeric($id) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            if (isset($_SESSION['user_id'])) 
                                            {
                                                //Botones para agregar al fandom
                                                $move->ifIssetOnSandbox("profile",$pic,$_SESSION['user_id'],$id,0);
                                            }
                                        }elseif(isset($fandom) && !empty($fandom) && is_numeric($fandom) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            if(isset($_SESSION['user_id']))
                                            {
                                                $move->ifIssetOnSandbox("fandom",$pic,$_SESSION['user_id'],$fandom,1);
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <!-- <div class="panel panel-default">
                                    <div class="panel-body">
                                        <p>Comentario:</p>
                                        <form role="form">
                                            <div class="form-group">
                                                <textarea class="form-control" rows="3"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Enviar</button>
                                        </form>
                                    </div>
                                </div> -->
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <?php
                                        //Adaptando la caja de comentarios de fb segun sea el caso
                                        if(isset($id) && !empty($id) && is_numeric($id) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            echo '<div class="fb-comments" data-href="http://fanplusplus.com/media.php?id='.$id.'&amp;pic='.$pic.'" data-width="100%" data-numposts="5" data-colorscheme="light"></div>';
                                        }elseif(isset($fandom) && !empty($fandom) && is_numeric($fandom) && isset($pic) && !empty($pic) && is_numeric($pic))
                                        {
                                            echo '<div class="fb-comments" data-href="http://fanplusplus.com/media.php?id='.$fandom.'&amp;pic='.$pic.'" data-width="100%" data-numposts="5" data-colorscheme="light"></div>';
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-1"></div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include 'static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <script src="static/js/fluid.js"></script>
</body>
</html>