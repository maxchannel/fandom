<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <title>Fan Plus Plus</title>
    <meta name="description" content="Fan Plus Plus es un comunidad donde puedes crear y unirte a Fandoms organizados por categorías."/>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="static/css/signin.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "static/analyticstracking.php" ?>
    <?php include 'static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container">
        <?php
        $userErr=$pwErr="";

        if ($_SERVER["REQUEST_METHOD"] == "POST")
        {
            function test_input($data)
            {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }

            //Validando el segundo campo: username
            if(empty($_POST['username']))
            {
                $userErr = "Username es un Campo Obligatorio";
            }elseif(!preg_match("/^[a-zA-Z0-9_]*$/", $_POST["username"]))
            {
                $userErr = "Usuario Incorrecto";
            }elseif(strlen($_POST["username"]) > 30)
            {
                $userErr = "Usuario Incorrecto";
            }else
            {
                $username = test_input($_POST["username"]);
            }

            //Validando el password
            if(empty($_POST['pw']))
            {
                $pwErr = "Contraseña es un Campo Obligatorio";
            }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['pw']))
            {
                $pwErr = "Contraseña Incorrecta";
            }elseif(strlen($_POST['pw']) < 6)
            {
                $pwErr = "Contraseña Incorrecta";
            }else
            {
                $pw = test_input($_POST['pw']);
            } 

        } 

        //Mostrando errores
        if(isset($userErr) && ($userErr!="")) 
        {
            echo '<div class="alert alert-danger fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <p>'.$userErr.'</p>
                </div>';
        }
        if(isset($pwErr) && ($pwErr!="")) 
        {
            echo '<div class="alert alert-danger fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <p>'.$pwErr.'</p>
                </div>';
        }

        //INICIANDO SESION CUANDO NO HALLA ERRORES
        if(isset($username) && isset($pw) && !isset($_SESSION['username'])) 
        {
            include 'link.php';
            $con1 = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar...");
            $result = mysqli_query($con1,"SELECT ID,USERNAME,PW,FIRSTNAME FROM profiles WHERE USERNAME='".$username."'");

            if(mysqli_num_rows($result) > 0)//Si existe el username
            {
                $row = mysqli_fetch_array($result);

                //Verificando pw en db diferente
                $con2 = mysqli_connect('localhost','django83_3dmx','smuse_(6)','django83_jal3001');
                $result2 = mysqli_query($con2,"SELECT PW FROM pws_rellic WHERE USER_ID='".$row['ID']."' ");
                $row2 = mysqli_fetch_array($result2);

                if(md5($_POST['pw']) == $row2['PW'])//Si la contraseña ingresada coincide con la db
                {
                    //Iniciando la sesion si los datos son correctos
                    if(($_SESSION['username'] = $row['USERNAME']) && ($_SESSION['user_id'] = $row['ID']) && ($_SESSION['fn'] = $row['FIRSTNAME']))
                    {
                        $ip = $_SERVER["REMOTE_ADDR"];
                        $date = date("Y-m-d");
                        if (mysqli_query($con1,"INSERT INTO control_ip(USERNAME_ID,IP,FECHA) VALUES('".$row['ID']."','".$ip."','".$date."')")) 
                        {
                            echo '<div class="alert alert-success"><p>Iniciando Sesión</p></div>';
                        }
                        echo '<script> window.location="/"; </script>'; 
                    }else
                    {
                        echo '<div class="alert alert-danger">
                            <p>ERROR al Iniciar Sesión, contactanos:</p>
                            <p><a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
                            </div>';
                    } 
                }else
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                         <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                         <p>Datos Incorrectos</p>
                     </div>';
                }

            }else
            {
                echo '<div class="alert alert-danger fade in" role="alert">
                         <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                         <p>No Existe este Usuario</p>
                     </div>';
            }
        }

        if(!isset($_SESSION['username']) && !isset($_SESSION['user_id']))
        {
            echo '<form action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" method="post" class="form-signin" role="form">
                <h2 class="form-signin-heading">Entrar a F++</h2>
                <label for="enterUser">Usuario</label>
                <input name="username" value="'.$username.'" type="text" class="form-control" id="enterUser" placeholder="Usuario" />
                <label for="enterPass">Contraseña</label>
                <input name="pw" type="password" class="form-control" id="enterPass" placeholder="Contraseña" />
                <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
            </form>';
        }else 
        {
            echo '<div class="alert alert-info" role="alert"><p>Ya estas logueado en F++</p></div>';
        }

        ?>
    </div> <!-- /container -->
    <!-- Fin de contenido -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
</body>
</html>