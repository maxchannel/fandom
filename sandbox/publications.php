<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Publicaciones - Sandbox</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Sandbox: todas la publicaciones que te gustaron y guardaste aparecen en este apartado de tu sandbox."/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <h2 class="text-center">Publicaciones de tu Sandbox</h2>
        <section class="row">

            <!-- Modal para enviar error-->                    
            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Informar sobre error</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para enviar error-->
            <!-- Modal para eliminar publicacion de sandbox-->                    
            <div class="modal fade bs-example-modal-sm-sand-dpub" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar Publicación del Sandbox</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Estas seguro de eliminar esta publicación de tu Sandbox?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para eliminar publicacion de sandbox-->
            <!-- Modal de reporte de publicacion-->                   
            <div class="modal fade bs-example-modal-sm-report-p" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar una Publicación</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Contenido Ofensivo</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal de reporte de publicacion-->

            <!-- Division 1-->
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2"></div>
            <!-- Contenedor de 4 sandbox-->
            <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6">
                <!-- Fila dentro de col-->
                <div class="row">
                    <!-- Publicaciones-->
                    <?php
                    if (isset($_SESSION['user_id'])) 
                    {
                        include '../link.php';
                        $user_id = $_SESSION['user_id'];            

                        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                        $result = mysqli_query($con,"SELECT * FROM sandbox_publications WHERE WHO_ADD='".$user_id."' ORDER BY FECHA");
                        while($row = mysqli_fetch_array($result))
                        {
                            if($row['MODE'] == "fandom")
                            {
                                $result2 = mysqli_query($con,"SELECT * FROM fan_publications WHERE ID='".$row['PUBLICATION_ID']."'");
                                $row2 = mysqli_fetch_array($result2);
                                $result3 = mysqli_query($con,"SELECT PHOTO_SERVER,FANDOM_ID FROM fandom_images WHERE FANDOM_ID='".$row['PUBLICATEDIN_ID']."' ORDER BY FECHA DESC LIMIT 1");
                                $row3 = mysqli_fetch_array($result3);
                                $result4 = mysqli_query($con,"SELECT FANDOM FROM fandoms WHERE ID='".$row['PUBLICATEDIN_ID']."'");
                                $row4 = mysqli_fetch_array($result4);
                                $result5 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['WHO_CREATE_PUBLICATION']."'");
                                $row5 = mysqli_fetch_array($result5);            

                                echo '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="panel panel-default post-panel">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <a href="../'.$row5['USERNAME'].'" ><img src="../'.$row5['AVATAR'].'" class="post-inside-image" /></a>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <a href="../'.$row5['USERNAME'].'" >'.$row5['FIRSTNAME'].'</a>
                                                        <p>Subido por</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="" class="pull-right" >'.$row['FECHA'].'</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <p>'.$row2['CONTENT'].'</p>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <a href="max" ><img src="../fandomimages/817236bbd8515e47eae9c5f72b115c5a.jpg" class="post-inside-image" /></a>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <a href="panchebrios" >Panchebrios</a>
                                                        <p>Via Fandom</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="pull-right">';
                                                        //Boton de eliminar solo si existe en la db y hay una sesion activa
                                                        if (isset($_SESSION['username'])) 
                                                        {
                                                            $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                                                            $totalS = mysqli_num_rows($resultS);                                                               

                                                            if($totalS > 0)
                                                            {
                                                                echo '<a href="" onclick="deletePublication('.$row2['ID'].','.$_SESSION['user_id'].',1,this)"><span class="glyphicon glyphicon-trash unsandbox-font"></span></a>';
                                                            }

                                                        }
                                echo '                  </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>';
                                        
                            }elseif($row['MODE'] == "profile") 
                            {
                                $result2 = mysqli_query($con,"SELECT * FROM profile_publications WHERE ID='".$row['PUBLICATION_ID']."'");
                                $row2 = mysqli_fetch_array($result2);
                                $result3 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['WHO_CREATE_PUBLICATION']."' ");
                                $row3 = mysqli_fetch_array($result3);
                                $result4 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['PUBLICATEDIN_ID']."'");
                                $row4 = mysqli_fetch_array($result4);  

                                echo '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="panel panel-default post-panel">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <a href="../'.$row3['USERNAME'].'" ><img src="../'.$row3['AVATAR'].'" class="post-inside-image" /></a>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <a href="../'.$row3['USERNAME'].'" >'.$row3['FIRSTNAME'].'</a>
                                                        <p>Subido por</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="" class="pull-right" >23/05/2014</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <p>'.$row2['CONTENT'].'</p>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="pull-right">';
                                                        if (isset($_SESSION['username'])) 
                                                        {
                                                            $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                                            $totalS = mysqli_num_rows($resultS);                                                               

                                                            if($totalS > 0)
                                                            {
                                                                echo '<a href="" onclick="deletePublication('.$row2['ID'].','.$_SESSION['user_id'].',0,this)" ><span class="glyphicon glyphicon-trash unsandbox-font"></span></a>';
                                                            }
                                                        }
                                echo '                  </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>';
                            }
                        }//Fin de while

                    }else
                    {
                        echo "Logueate para ver tus mensajes";
                    }        

                    ?>

                </div>
            </div>

            <!-- Contenedor de publicidad-->
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 hidden-xs">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="../sandbox"><span class="glyphicon glyphicon-arrow-left"></span> Volver a Sandbox</a>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Publicidad</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <p>&copy; 2014 Fanplusplus &middot; <a href="help.html" >Ayuda</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
    <script src="../static/js/fluid.js"></script>
</body>
</html>