<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sandbox - todas tus cosas estan aquí</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Sandbox es el lugar donde guardas todas las cosas que te gustaron y quieres conservar en tus recorridos por fan plus plus."/>
    <link rel="stylesheet" type="text/css" href="../static/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes permitidas-->
    <link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Crafty+Girls' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Devonshire' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Electrolize' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Fanwood+Text' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Kite+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway+Dots' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=The+Girl+Next+Door' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Walter+Turncoat' rel='stylesheet' type='text/css'>
    <!-- Fuentes permitidas-->
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">

            <!-- Modal para enviar error-->                    
            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Informar sobre error</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para enviar error-->
            <!-- Modal para eliminar mensaje de sandbox-->                    
            <div class="modal fade bs-example-modal-sm-sand-dmsn" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar Mensaje del Sandbox</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Estas seguro de eliminar este mensaje recibido en tu Sandbox?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para eliminar mensaje de sandbox--> 
            <!-- Modal para eliminar nota de sandbox-->                    
            <div class="modal fade bs-example-modal-sm-sand-dnote" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar Nota del Sandbox</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Estas seguro de eliminar esta nota de tu Sandbox?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para eliminar nota de sandbox-->
            <!-- Modal para eliminar fotos-->                    
            <div class="modal fade bs-example-modal-sm-sand-dfoto" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar Imágen de Sandbox</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Estas seguro de eliminar esta Imágen de tu Sandbox?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para eliminar fotos-->
            <!-- Modal para eliminar publicacion de sandbox-->                    
            <div class="modal fade bs-example-modal-sm-sand-dpub" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar Publicación del Sandbox</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Estas seguro de eliminar esta publicación de tu Sandbox?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para eliminar publicacion de sandbox-->
            <!-- Modal de reporte de mensaje recibido-->                   
            <div class="modal fade bs-example-modal-sm-report-msn" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Mensaje Recibido</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Contenido Ofensivo</option>
                                <option>Contenido no Apto</option>
                                <option>No Conozco a este Usuario</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal de reporte de mensaje recibido-->  
            <!-- Modal de reporte de nota-->                   
            <div class="modal fade bs-example-modal-sm-report-n" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Nota</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Contenido Ofensivo</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal de reporte de nota-->
            <!-- Modal de reporte de publicacion-->                   
            <div class="modal fade bs-example-modal-sm-report-p" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar una Publicación</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Contenido Ofensivo</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal de reporte de publicacion-->
            <!-- Modal para reportar imagen -->                   
            <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Imágen de Max</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Usuario Molesto</option>
                                        <option>Apología a la Violencia</option>
                                        <option>Desnudez</option>
                                        <option>Derechos de Autor</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar imagen -->  

            <?php
            //Solo se muestra el contenido si estas logueado
            if(isset($_SESSION['username']) && isset($_SESSION['user_id']))
            {
                echo '<h2 class="text-center">Sandbox todas tus cosas estan aquí</h2>
                    <!-- Division 1-->
                    <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2"></div>
                    <!-- Contenedor de 4 sandbox-->
                    <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6">
                        <!-- Fila dentro de col-->
                        <div class="row">
                            <!-- Mensajes-->';
                            include '../link.php';
                            $user_id = $_SESSION['user_id'];

                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                            $resultM = mysqli_query($con,"SELECT * FROM sandbox_messages WHERE PARA_ID='$user_id' ORDER BY FECHA DESC LIMIT 3");
                            echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><a href="messages">Mensajes</a></h3>
                                        </div>
                                        <div class="panel-body" >
                                        <!-- Mensaje 1 -->';
                                        while($rowM = mysqli_fetch_array($resultM))
                                        {
                                            $resultM2 = mysqli_query($con,"SELECT USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".$rowM['DE_ID']."'");
                                            $rowM2 = mysqli_fetch_array($resultM2);                        

                                            echo '<div class="panel panel-default post-panel">
                                            <div class="panel-heading">
                                                <a href="../'.strtolower($rowM2['USERNAME']).'" ><img src="../'.$rowM2['AVATAR'].'" class="post-inside-image" /></a>
                                                <a href="../'.strtolower($rowM2['USERNAME']).'" >'.$rowM2['FIRSTNAME'].'</a>
                                                <p class="pull-right">'.$rowM['FECHA'].'</p>
                                            </div>
                                            <div class="panel-body">
                                                <p>'.$rowM['MESSAGE'].'</p>
                                            </div>
                                            <div class="panel-footer">
                                                <button type="button" class="btn btn-danger" onclick="deleteMessage('.$rowM['ID'].',this)" >Eliminar</button>
                                                <!-- Boton dropdown-->
                                                <div class="btn-group pull-right">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dmsn">Eliminar Mensaje</a></li>
                                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-msn">Reportar</a></li>
                                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>';
                                        }
                            echo '</div>
                                </div>
                            </div>';
                            //Fin de Mensajes
                            //Inicio de notas
                            echo '<!-- Notas-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><a href="notes">Notas</a></h3>
                                    </div>
                                    <div class="panel-body">
                                        <!-- Nota 1 -->';
                                        $resultN = mysqli_query($con,"SELECT * FROM sandbox_notes WHERE WHO_ADD='".$user_id."' ORDER BY FECHA LIMIT 3");
                                        while($rowN = mysqli_fetch_array($resultN))
                                        {
                                            if($rowN['MODE'] == "fandom")
                                            {
                                                $resultN2 = mysqli_query($con,"SELECT * FROM fandom_notes WHERE ID='".$rowN['NOTE_ID']."'");
                                                $rowN2 = mysqli_fetch_array($resultN2);
                                                $resultN3 = mysqli_query($con,"SELECT PHOTO_SERVER,FANDOM_ID FROM fandom_images WHERE FANDOM_ID='".$rowN['PUBLICATEDIN_ID']."' ORDER BY FECHA DESC LIMIT 1");
                                                $rowN3 = mysqli_fetch_array($resultN3);
                                                $resultN4 = mysqli_query($con,"SELECT FANDOM FROM fandoms WHERE ID='".$rowN['PUBLICATEDIN_ID']."'");
                                                $rowN4 = mysqli_fetch_array($resultN4);
                                                $resultN7 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowN['WHO_CREATE_NOTE']."'");
                                                $rowN7 = mysqli_fetch_array($resultN7);      

                                                //Imprimiendo nota
                                                echo '<div class="panel panel-default post-panel">
                                                    <div class="panel-heading">
                                                        <a href="../'.$rowN7['USERNAME'].'" ><img src="../'.$rowN7['AVATAR'].'" class="post-inside-image" /></a>
                                                        <a href="../'.$rowN7['USERNAME'].'" >'.$rowN7['FIRSTNAME'].'</a>
                                                        <p class="pull-right"><a href="#">'.$rowN['FECHA'].'</a></p>
                                                    </div>
                                                    <div class="panel-body">
                                                        <p class="font9">'.$rowN2['NOTE'].'</p>
                                                    </div>
                                                    <div class="panel-footer">';
                                                        if(isset($_SESSION['username']))
                                                        {
                                                            $resultS = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$rowN2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                                                            $totalS = mysqli_num_rows($resultS);                                                                

                                                            if($totalS > 0)
                                                            {
                                                                echo '<button type="button" class="btn btn-danger" onclick="noteDeleteSandbox('.$rowN2['ID'].','.$_SESSION['user_id'].',1,this)" >Unsandbox</button>';
                                                            }else
                                                            {
                                                                echo '<button type="button" class="btn btn-primary" onclick="noteSandbox('.$rowN2['ID'].','.$_SESSION['user_id'].', '.$rowN2['PROFILE_ID'].', '.$rowN2['FANDOM_ID'].',1, this)" >Sandbox</button>';
                                                            }
                                                        }else
                                                        {
                                                            echo '<a href="../signin" class="btn btn-primary">Login</a>';
                                                        }
                                            echo '      <!-- Boton dropdown-->
                                                        <div class="btn-group pull-right">
                                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dnote">Eliminar Nota</a></li>
                                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-n">Reportar</a></li>
                                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>';                  

                                                //echo '<td><a href="../'.lcfirst($rowN4['FANDOM']).'"><img src="../'.$rowN3['PHOTO_SERVER'].'" height="70" width="70"></a></td>';
  
                                            }elseif($rowN['MODE'] == "profile") 
                                            {
                                                $resultN2 = mysqli_query($con,"SELECT * FROM profile_notes WHERE ID='".$rowN['NOTE_ID']."'");
                                                $rowN2 = mysqli_fetch_array($resultN2);
                                                $resultN3 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowN['WHO_CREATE_NOTE']."' ");
                                                $rowN3 = mysqli_fetch_array($resultN3);
                                                $resultN4 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowN['PUBLICATEDIN_ID']."'");
                                                $rowN4 = mysqli_fetch_array($resultN4);   

                                                //Imprimiendo nota
                                                echo '<div class="panel panel-default post-panel">
                                                    <div class="panel-heading">
                                                        <a href="../'.$rowN3['USERNAME'].'" ><img src="../'.$rowN3['AVATAR'].'" class="post-inside-image" /></a>
                                                        <a href="../'.$rowN3['USERNAME'].'" >'.$rowN3['FIRSTNAME'].'</a>
                                                        <p class="pull-right">'.$rowN['FECHA'].'</p>
                                                    </div>
                                                    <div class="panel-body">
                                                        <p class="font9">'.$rowN2['NOTE'].'</p>
                                                    </div>
                                                    <div class="panel-footer">';
                                                    if(isset($_SESSION['username']))
                                                    {
                                                        $resultS = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$rowN2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                                        $totalS = mysqli_num_rows($resultS);                                                    

                                                        if($totalS > 0)
                                                        {
                                                            echo '<button type="button" class="btn btn-danger" onclick="noteDeleteSandbox('.$rowN2['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                                            //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                                        }else
                                                        {
                                                            echo '<button type="button" class="btn btn-primary" onclick="noteSandbox('.$rowN2['ID'].','.$_SESSION['user_id'].', '.$rowN2['FROM_ID'].', '.$rowN2['TO_ID'].', 0, this)" >Sandbox</button>';
                                                            //echo '<div id="noteSandbox"><button id="btnSandNotes" >Agregar al Sandbox</button></div>';
                                                        }
                                                    }else
                                                    {
                                                        echo '<a href="../signin" class="btn btn-primary">Login</a>';
                                                    }
                                    echo '          <!-- Boton dropdown-->
                                                        <div class="btn-group pull-right">
                                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dnote">Eliminar Nota</a></li>
                                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-n">Reportar</a></li>
                                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>';                        
                                            }
                                        }

echo '                              </div>
                                </div>
                            </div>';
                            //Fin de notas

                            //Publicidad
echo '                      <!-- Publicidad -->
                            <aside class="col-xs-12 visible-xs">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Publicidad</h3>
                                    </div>
                                    <div class="panel-body">
                                        Panel content
                                    </div>
                                </div>
                            </aside>';

                            //Inicio de imagenes
echo '                      <!-- Imágenes-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><a href="images">Imágenes</a></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <!-- Comienzo de imagenes con pie-->
                                            <!-- Imagen 1 -->';

                                            $resultI = mysqli_query($con,"SELECT * FROM sandbox_images WHERE WHO_ADD='".$user_id."' ORDER BY FECHA LIMIT 4");
                                            while($rowI = mysqli_fetch_array($resultI))
                                            {
                                                if($rowI['MODE'] == "fandom")
                                                {
                                                    $resultI2 = mysqli_query($con,"SELECT * FROM fandom_images WHERE ID='".$rowI['IMAGE_ID']."'");
                                                    $rowI2 = mysqli_fetch_array($resultI2);
                                                    $resultI3 = mysqli_query($con,"SELECT * FROM fandom_images WHERE FANDOM_ID='".$rowI['UPLOADIN_ID']."' ORDER BY FECHA DESC LIMIT 1");
                                                    $rowI3 = mysqli_fetch_array($resultI3);
                                                    $resultI4 = mysqli_query($con,"SELECT FANDOM FROM fandoms WHERE ID='".$rowI['UPLOADIN_ID']."'");
                                                    $rowI4 = mysqli_fetch_array($resultI4);
                                                    $resultI5 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowI2['USER_ID']."'");
                                                    $rowI5 = mysqli_fetch_array($resultI5);          

                                                    $resultM = mysqli_query($con,"SELECT ID FROM sandbox_images WHERE IMAGE_ID='".$rowI['IMAGE_ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom' ");
                                                    $total = mysqli_num_rows($resultM);           

                                                    echo '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <div class="row">
                                                                        <div class="col-md-7">
                                                                            <a href="../'.$rowI5['USERNAME'].'" ><img src="../'.$rowI5['AVATAR'].'" class="post-inside-image" /></a>
                                                                            <a href="../'.$rowI5['USERNAME'].'" >'.$rowI5['FIRSTNAME'].'</a>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <p><a href="../media?id='.$rowI2['FANDOM_ID'].'&pic='.$rowI2['ID'].'" class="pull-right">'.$rowI['FECHA'].'</a></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="../media?fandom='.$rowI2['FANDOM_ID'].'&pic='.$rowI2['ID'].'" ><img src="../'.$rowI2['PHOTO_SERVER'].'" class="img-responsive"></a>
                                                                <div class="panel-footer">';
                                                                    if(isset($_SESSION['user_id']))
                                                                    {
                                                                        if($total > 0)
                                                                        {
                                                                            echo '<button type="button" onclick="deleteSandbox('.$rowI['IMAGE_ID'].','.$_SESSION['user_id'].',1,this)" class="btn btn-danger" >Unsandbox</button>';
                                                                        }else
                                                                        {
                                                                            echo '<button type="button" onclick="addSandboxP('.$rowI['IMAGE_ID'].','.$_SESSION['user_id'].','.$rowI2['id'].','.$rowI2['id'].',1,this)" class="btn btn-primary" >Sandbox</button>';
                                                                        }   
                                                                    }
                                                    echo '      </div>
                                                            </div>
                                                        </div>';                                         

                                                }elseif($rowI['MODE'] == "profile") 
                                                {
                                                    $resultI2 = mysqli_query($con,"SELECT ID,USER_ID,PHOTO_SERVER FROM profile_photos WHERE ID='".$rowI['IMAGE_ID']."'");
                                                    $rowI2 = mysqli_fetch_array($resultI2);
                                                    $resultI3 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowI['WHO_UPLOAD_IMAGE']."' ");
                                                    $rowI3 = mysqli_fetch_array($resultI3);
                                                    $resultI4 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowI['UPLOADIN_ID']."'");
                                                    $rowI4 = mysqli_fetch_array($resultI4);        

                                                    $resultM = mysqli_query($con,"SELECT ID FROM sandbox_images WHERE IMAGE_ID='".$rowI['IMAGE_ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile' ");
                                                    $total = mysqli_num_rows($resultM);               

                                                    echo '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <div class="row">
                                                                        <div class="col-md-7">
                                                                            <a href="../'.$rowI3['USERNAME'].'" ><img src="../'.$rowI4['AVATAR'].'" class="post-inside-image" /></a>
                                                                            <a href="../'.$rowI3['USERNAME'].'" >'.$rowI3['FIRSTNAME'].'</a>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <p><a href="../media?id='.$rowI2['USER_ID'].'&pic='.$rowI2['ID'].'" class="pull-right">'.$rowI['FECHA'].'</a></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="../media?id='.$rowI2['USER_ID'].'&pic='.$rowI2['ID'].'" ><img src="../'.$rowI2['PHOTO_SERVER'].'" class="img-responsive"></a>
                                                                <div class="panel-footer">';
                                                                    if(isset($_SESSION['user_id']))
                                                                    {
                                                                        if($total > 0)
                                                                        {
                                                                            echo '<button type="button" onclick="deleteSandbox('.$rowI['IMAGE_ID'].','.$_SESSION['user_id'].',0,this)" class="btn btn-danger" >Unsandbox</button>';
                                                                        }else
                                                                        {
                                                                            echo '<button type="button" onclick="addSandboxP('.$rowI['IMAGE_ID'].','.$_SESSION['user_id'].','.$rowI2['id'].','.$rowI2['id'].',0,this)" class="btn btn-primary" >Sandbox</button>';
                                                                        }   
                                                                    }
                                                    echo '      </div>
                                                            </div>
                                                        </div>';                                       

                                                }
                                            }

echo '                                  </div>
                                    </div>
                                </div>
                            </div>';
                            //Fin de imagenes

echo '                      <!-- Publicaciones -->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><a href="publications">Publicaciones</a></h3>
                                    </div>
                                    <div class="panel-body">
                                        <!-- Publicacion 1 -->';
                                        $resultP = mysqli_query($con,"SELECT * FROM sandbox_publications WHERE WHO_ADD='".$user_id."' ORDER BY FECHA LIMIT 3");

                                        while($rowP = mysqli_fetch_array($resultP))
                                        {
                                            if($rowP['MODE'] == "fandom")
                                            {
                                                $resultP2 = mysqli_query($con,"SELECT * FROM fan_publications WHERE ID='".$rowP['PUBLICATION_ID']."'");
                                                $rowP2 = mysqli_fetch_array($resultP2);
                                                $resultP3 = mysqli_query($con,"SELECT PHOTO_SERVER,FANDOM_ID FROM fandom_images WHERE FANDOM_ID='".$rowP['PUBLICATEDIN_ID']."' ORDER BY FECHA DESC LIMIT 1");
                                                $rowP3 = mysqli_fetch_array($resultP3);
                                                $resultP4 = mysqli_query($con,"SELECT FANDOM FROM fandoms WHERE ID='".$rowP['PUBLICATEDIN_ID']."'");
                                                $rowP4 = mysqli_fetch_array($resultP4);
                                                $resultP5 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowP['WHO_CREATE_PUBLICATION']."'");
                                                $rowP5 = mysqli_fetch_array($resultP5);

                                                echo '<div class="panel panel-default post-panel">
                                                        <div class="panel-heading">
                                                            <a href="../'.$rowP5['USERNAME'].'" ><img src="../'.$rowP5['AVATAR'].'" class="post-inside-image" /></a>
                                                            <a href="../'.$rowP5['USERNAME'].'" >'.$rowP5['FIRSTNAME'].'</a>
                                                            <p class="pull-right">'.$rowP['FECHA'].'</p>
                                                        </div>
                                                        <div class="panel-body">
                                                            <p>'.$rowP2['CONTENT'].'</p>
                                                        </div>
                                                        <div class="panel-footer">';
                                                            if (isset($_SESSION['username'])) 
                                                            {
                                                                $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$rowP2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                                                                $totalS = mysqli_num_rows($resultS);                                                               

                                                                if($totalS > 0)
                                                                {
                                                                    echo '<button type="button" class="btn btn-danger" onclick="deletePublication('.$rowP2['ID'].','.$_SESSION['user_id'].',1,this)" >Unsandbox</button>';
                                                                    //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                                                }else
                                                                {
                                                                    echo '<button type="button" class="btn btn-primary" onclick="addPublication('.$rowP2['ID'].', '.$_SESSION['user_id'].', '.$rowP2['DE_ID'].', '.$rowP2['PARA_ID'].', 1, this)" >Sandbox</button>';
                                                                }
                                                            }else
                                                            {
                                                                echo '<a href="../signin" class="btn btn-primary">Login</a>';
                                                            }
                                        echo '              <!-- Boton dropdown-->
                                                            <div class="btn-group pull-right">
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dpub">Eliminar Publicación</a></li>
                                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>';                        

                                            }elseif($rowP['MODE'] == "profile") 
                                            {
                                                $resultP2 = mysqli_query($con,"SELECT * FROM profile_publications WHERE ID='".$rowP['PUBLICATION_ID']."'");
                                                $rowP2 = mysqli_fetch_array($resultP2);
                                                $resultP3 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowP['WHO_CREATE_PUBLICATION']."' ");
                                                $rowP3 = mysqli_fetch_array($resultP3);
                                                $resultP4 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowP['PUBLICATEDIN_ID']."'");
                                                $row4 = mysqli_fetch_array($resultP4);                        

                                                echo '<div class="panel panel-default post-panel">
                                                        <div class="panel-heading">
                                                            <a href="../'.$rowP3['USERNAME'].'"><img src="../'.$rowP3['AVATAR'].'" class="post-inside-image" /></a>
                                                            <a href="../'.$rowP3['USERNAME'].'">'.$rowP3['FIRSTNAME'].'</a>
                                                            <p class="pull-right">'.$rowP['FECHA'].'</p>
                                                        </div>
                                                        <div class="panel-body">
                                                            <p>'.$rowP2['CONTENT'].'</p>
                                                        </div>
                                                        <div class="panel-footer">';
                                                        if (isset($_SESSION['username'])) 
                                                        {
                                                            $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$rowP2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                                            $totalS = mysqli_num_rows($resultS);                                                           

                                                            if($totalS > 0)
                                                            {
                                                                echo '<button type="button" class="btn btn-danger" onclick="deletePublication('.$rowP2['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                                                //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                                            }else
                                                            {
                                                                echo '<button type="button" class="btn btn-primary" onclick="addPublication('.$rowP2['ID'].', '.$_SESSION['user_id'].', '.$rowP2['DE_ID'].', '.$rowP2['PARA_ID'].', 0, this)" >Sandbox</button>';
                                                            }
                                                        }else
                                                        {
                                                            echo '<a href="../signin" class="btn btn-primary">Login</a>';
                                                        }
                                    echo '              <!-- Boton dropdown-->
                                                            <div class="btn-group pull-right">
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dpub">Eliminar Publicación</a></li>
                                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>';

                                            }
                                        }

echo '                              </div>
                                </div>
                            </div>
                            <!-- Fin de publicacions-->
                        </div>
                    </div>
                    <!-- Contenedor de publicidad-->
                    <aside class="col-xs-12 col-sm-4 col-md-3 col-lg-3 hidden-xs">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Publicidad</h3>
                            </div>
                            <div class="panel-body">
                                Panel content
                            </div>
                        </div>
                    </aside>';
            }else
            {
                echo '<div class="alert alert-warning"><p>Debes <a href="../signin">Iniciar Sesión</a> para ver tu Sandbox</p></div>';
            }

            ?>
            
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
    <script src="../static/js/fluid.js"></script>
</body>
</html>