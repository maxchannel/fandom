<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Imágenes - Sandbox</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Sandbox: todas las imágenes que guardas en fan plus plus se guardan aquí, incluyendo la de los fandoms."/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Modal para enviar error-->                    
    <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Informar sobre error</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal para enviar error-->
    <!-- Modal para reportar imagen -->                   
    <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Reportar Imágen de Max</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Usuario Molesto</option>
                                <option>Apología a la Violencia</option>
                                <option>Desnudez</option>
                                <option>Derechos de Autor</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal para reportar imagen -->

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <h2 class="text-center">Imágenes de tu Sandbox</h2>
        <section class="row">
            <!-- Division 1-->
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2"></div>
            <!-- Contenedor de 4 sandbox-->
            <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6">
                <!-- Imagenes -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- Comienzan Imagenes-->
                    <div class="row">
                        <?php
                        if(isset($_SESSION['user_id']))
                        {
                            include '../link.php';
                            $user_id = $_SESSION['user_id'];
                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar."); 
                            $step = 1;   

                            $resultI = mysqli_query($con,"SELECT * FROM sandbox_images WHERE WHO_ADD='".$user_id."' ORDER BY FECHA");
                            while($rowI = mysqli_fetch_array($resultI))
                            {
                                if($rowI['MODE'] == "fandom")
                                {
                                    $resultI2 = mysqli_query($con,"SELECT * FROM fandom_images WHERE ID='".$rowI['IMAGE_ID']."'");
                                    $rowI2 = mysqli_fetch_array($resultI2);
                                    $resultI3 = mysqli_query($con,"SELECT * FROM fandom_images WHERE FANDOM_ID='".$rowI['UPLOADIN_ID']."' ORDER BY FECHA DESC LIMIT 1");
                                    $rowI3 = mysqli_fetch_array($resultI3);
                                    $resultI4 = mysqli_query($con,"SELECT FANDOM FROM fandoms WHERE ID='".$rowI['UPLOADIN_ID']."'");
                                    $rowI4 = mysqli_fetch_array($resultI4);
                                    $resultI5 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowI2['USER_ID']."'");
                                    $rowI5 = mysqli_fetch_array($resultI5);  

                                    $resultM = mysqli_query($con,"SELECT ID FROM sandbox_images WHERE IMAGE_ID='".$rowI['IMAGE_ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom' ");
                                    $total = mysqli_num_rows($resultM);   

                                    echo '<!-- Imagen  -->
                                        <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <a href="../'.$rowI5['USERNAME'].'" ><img src="../'.$rowI5['AVATAR'].'" class="post-inside-image" /></a>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <a href="../'.$rowI5['USERNAME'].'" >'.$rowI5['FIRSTNAME'].'</a>
                                                                    <p>Subido por</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <a href="">23/05/2014</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="" data-toggle="modal" data-target=".img'.$step.'" ><img src="../'.$rowI2['PHOTO_SERVER'].'" class="img-responsive"></a>
                                                <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <a href="" ><img src="../fandomimages/a2252ccb407cc83111e7970a740e17d4.jpg" class="post-inside-image" /></a>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <a href="max" >Panchebrios</a>
                                                                    <p>Via Fandom</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">';
                                                            if(isset($_SESSION['user_id']))
                                                            {
                                                                if($total > 0)
                                                                {
                                                                    echo '<a href="" onclick="deleteSandbox('.$rowI['IMAGE_ID'].','.$_SESSION['user_id'].',1,this)" ><span class="glyphicon glyphicon-trash unsandbox-font"></span></a>';
                                                                }
                                                            }
                                    echo '              </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Modal para ver foto de inicio-->                    
                                        <div class="modal fade img'.$step.'" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <a href="max" ><img src="../fandomimages/817236bbd8515e47eae9c5f72b115c5a.jpg" class="post-inside-image" /></a>
                                                        <a href="max" >Max</a>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="../'.$rowI2['PHOTO_SERVER'].'"  class="img-responsive">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Imagen -->';

                                }elseif($rowI['MODE'] == "profile") 
                                {
                                    $resultI2 = mysqli_query($con,"SELECT ID,USER_ID,PHOTO_SERVER FROM profile_photos WHERE ID='".$rowI['IMAGE_ID']."'");
                                    $rowI2 = mysqli_fetch_array($resultI2);
                                    $resultI3 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowI['WHO_UPLOAD_IMAGE']."' ");
                                    $rowI3 = mysqli_fetch_array($resultI3);
                                    $resultI4 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$rowI['UPLOADIN_ID']."'");
                                    $rowI4 = mysqli_fetch_array($resultI4);

                                    $resultM = mysqli_query($con,"SELECT ID FROM sandbox_images WHERE IMAGE_ID='".$rowI['IMAGE_ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile' ");
                                    $total = mysqli_num_rows($resultM);   

                                    echo '<!-- Imagen -->
                                        <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <a href="../'.$rowI3['USERNAME'].'" ><img src="../'.$rowI4['AVATAR'].'" class="post-inside-image" /></a>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <a href="../'.$rowI3['USERNAME'].'" >'.$rowI3['FIRSTNAME'].'</a>
                                                                    <p>Subido por</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <a href="../media?id='.$rowI2['USER_ID'].'&pic='.$rowI2['ID'].'" >'.$rowI['FECHA'].'</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="" data-toggle="modal" data-target=".img'.$step.'" ><img src="../'.$rowI2['PHOTO_SERVER'].'" class="img-responsive"></a>
                                                <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                </div>
                                                                <div class="col-md-9">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">';
                                                            if(isset($_SESSION['user_id']))
                                                            {
                                                                if($total > 0)
                                                                {
                                                                    echo '<a href="" onclick="deleteSandbox('.$rowI['IMAGE_ID'].','.$_SESSION['user_id'].',0,this)" ><span class="glyphicon glyphicon-trash unsandbox-font"></span></a>';
                                                                } 
                                                            }
                                    echo '              </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Modal para ver foto de inicio-->                    
                                        <div class="modal fade img'.$step.'" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <a href="max" ><img src="../fandomimages/817236bbd8515e47eae9c5f72b115c5a.jpg" class="post-inside-image" /></a>
                                                        <a href="max" >Max</a>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="../'.$rowI2['PHOTO_SERVER'].'"  class="img-responsive">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal para ver foto de inicio-->
                                        <!-- Imagen -->';                                 

                                }
                            $step++;
                            }//Fin de while
                        }else
                        {
                            echo "Logueate para ver tus imagenes.";
                        }

                        ?>
                    </div>
                </div>
            </div>

            <!-- Contenedor de publicidad-->
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 hidden-xs">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="../sandbox"><span class="glyphicon glyphicon-arrow-left"></span> Volver a Sandbox</a>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Publicidad</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <p>&copy; 2014 Fanplusplus &middot; <a href="help.html" >Ayuda</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
    <script src="../static/js/fluid.js"></script>
</body>
</html>