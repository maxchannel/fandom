<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mensajes - Sandbox</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Sandbox: todos los mensajes que te envian se guardaran aquí."/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <h2 class="text-center">Mensajes de tu Sandbox</h2>
        <section class="row">

            <!-- Modal para enviar error-->                    
            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Informar sobre error</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para enviar error-->
            <!-- Modal para eliminar mensaje de sandbox-->                    
            <div class="modal fade bs-example-modal-sm-sand-dmsn" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar Mensaje del Sandbox</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Estas seguro de eliminar este mensaje de tu Sandbox?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para eliminar mensaje de sandbox--> 
            <!-- Modal de reporte de mensaje recibido-->                   
            <div class="modal fade bs-example-modal-sm-report-msn" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Mensaje Recibido</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Contenido Ofensivo</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal de reporte de mensaje recibido-->  

            <!-- Division 1-->
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2"></div>
            <!-- Contenedor de los mensajes-->
            <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6">
                <!-- Fila dentro del contenedor-->
                <div class="row">
                    <!-- Mensajes-->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <?php
                        if(isset($_SESSION['username']) && isset($_SESSION['user_id']))
                        {
                            include '../link.php';
                            $user_id = $_SESSION['user_id'];

                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                            $resultM = mysqli_query($con,"SELECT * FROM sandbox_messages WHERE PARA_ID='".$user_id."' ORDER BY FECHA DESC LIMIT 4");
                            while($rowM = mysqli_fetch_array($resultM))
                            {
                                $resultM2 = mysqli_query($con,"SELECT USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".$rowM['DE_ID']."'");
                                $rowM2 = mysqli_fetch_array($resultM2);      

                                echo '<!-- Mensaje 1 -->
                                    <div class="panel panel-default post-panel">
                                        <div class="panel-heading">
                                            <a href="../'.strtolower($rowM2['USERNAME']).'"><img src="../'.$rowM2['AVATAR'].'" class="post-inside-image" /></a>
                                            <a href="../'.strtolower($rowM2['USERNAME']).'">'.$rowM2['FIRSTNAME'].'</a>
                                            <p class="pull-right">16.05.2014</p>
                                        </div>
                                        <div class="panel-body">
                                            <p>'.$rowM['MESSAGE'].'</p>
                                        </div>
                                        <div class="panel-footer">
                                            <button type="button" class="btn btn-danger" onclick="deleteMessage('.$rowM['ID'].',this)" >Eliminar</button>
                                            <!-- Boton dropdown-->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-msn">Reportar</a></li>
                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>';                  
                            }
                            //Fin de Mensajes
                        }else
                        {
                            echo '<div class="alert alert-warning" role="alert">
                                    <p>Para ver tus mensajes <a href="../signin" class="alert-link">Inicia Sesión</a></p>
                                </div>';
                        }


                        ?>
                    </article>
                </div>
            </div>
            <!-- Contenedor de publicidad-->
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 hidden-xs">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="../sandbox"><span class="glyphicon glyphicon-arrow-left"></span> Volver a Sandbox</a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Publicidad</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
    <script src="../static/js/fluid.js"></script>
</body>
</html>