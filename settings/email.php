<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Configuración de Email- Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Panel para configurar el email del usuario en Fan plus plus."/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-4">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-4">
                <?php
                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }
                
                $newmailErr = $pw2Err = "";        

                if($_SERVER['REQUEST_METHOD'] == "POST")
                {
                    //Validando: email
                    if(empty($_POST['newmail']))
                    {
                        $newmailErr = "Email: Campo Obligatorio";
                    }elseif(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$_POST['newmail']))
                    {
                        $newmailErr = "Email no Válido";
                    }else
                    {
                        //Revisando si ya existe en la base de datos ese email
                        include '../link.php';
                        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar con la DB.<br>");
                        $totalEmail = mysqli_num_rows(mysqli_query($con,"SELECT ID FROM profiles WHERE EMAIL='".mysqli_real_escape_string($con,$_POST['newmail'])."'"));
                        if($totalEmail == 1)
                        {
                            $newmailErr = "Email ya existente";
                        }elseif($totalEmail == 0)
                        {
                            $newmail = test_input($_POST['newmail']);
                        }
                        mysqli_close($con);
                    }        

                    //Validando el password
                    if(empty($_POST['pw2']))
                    {
                        $pw2Err = "Campo Obligatorio";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['pw2']))
                    {
                        $pw2Err = "Contraseña Incorrecta";
                    }elseif(strlen($_POST['pw2']) < 6)
                    {
                        $pw2Err = "Contraseña Incorrecta";
                    }else
                    {
                        $pw2 = test_input($_POST['pw2']);
                    }         

                }        

                //Mostrando los errores solo si existen y son diferentes a ""
                if(isset($newmailErr) && ($newmailErr != ""))
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$newmailErr.'</p>
                        </div>';
                }  
                if(isset($pw2Err) && ($pw2Err != ""))
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$pw2Err.'</p>
                        </div>';
                }  

                if(isset($_SESSION['username']) && isset($_SESSION['user_id']))
                {
                    //Trayendo la información de la base de datos
                    include '../link.php';
                    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                    $user_id = mysqli_real_escape_string($con,$_SESSION['user_id']);
                    $username = mysqli_real_escape_string($con,$_SESSION['username']);
                    $result = mysqli_query($con,"SELECT ID,PW,USERNAME,EMAIL FROM profiles WHERE ID=".mysqli_real_escape_string($con,$user_id)."");
                    $row = mysqli_fetch_array($result);        

                    //Formulario para cambiar el avatar
                    echo '<div class="btn-group">
                                <a class="btn btn-default" href="../settings/" role="button">Básico</a>
                                <a class="btn btn-default" href="../settings/avatar" role="button">Avatar</a>
                                <a class="btn btn-default btn-select" href="../settings/email" role="button">Email</a>
                                <a class="btn btn-default" href="../settings/pass" role="button">Contraseña</a>
                            </div>
                            <h1>Actualizar Email</h1>
                            <form role="form" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" method="post">
                                <div class="form-group">
                                    <p class="help-block">Tu email actualmente es: '.$row['EMAIL'].'</p>
                                    <p class="help-block">Tendras que confirmar tu nuevo email.</p>
                                    <label for="exampleInputEmail1">Nuevo Email</label>
                                    <input name="newmail" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Válido" value="'.$newmail.'" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Contraseña</label>
                                    <input name="pw2" type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                                </div>
                                <button type="submit" class="btn btn-primary">Actualizar</button>
                            </form>';

                    //Si ya no hay mensajes de error y las variables se crean entonces prosigue
                    if(isset($newmail) && isset($pw2))
                    {
                        //Verificando pw en db diferente
                        $con2 = mysqli_connect('localhost','django83_3dmx','smuse_(6)','django83_jal3001');
                        $result2 = mysqli_query($con2,"SELECT PW FROM pws_rellic WHERE USER_ID='".$row['ID']."' ");
                        $row2 = mysqli_fetch_array($result2);

                        if(md5($pw2) == $row2['PW'])//Si las contraseñas coinciden
                        {
                            //Funcion para validar email
                            function spamcheck($field)
                            {
                                $field = filter_var($field,FILTER_SANITIZE_EMAIL);
                                if(filter_var($field,FILTER_VALIDATE_EMAIL))
                                {
                                    return TRUE;
                                }else
                                {
                                    return FALSE;
                                }
                            }        

                            //Insertando el codigo confirmacion en la db
                            //Enviando por email el codigo unico a cada usuario
                            if(isset($newmail))
                            {
                                $codigo_confirmacion = uniqid(); //Generando codigo
                                $sql = "UPDATE profiles SET EMAIL='".$newmail."' WHERE ID='".$user_id."' AND USERNAME='".$username."' AND PW='".md5($pw2)."'";
                                $sql2 = "INSERT INTO verify(EMAIL,CODIGO) VALUES('".$newmail."','".$codigo_confirmacion."')";
                                $mailcheck = spamcheck($newmail);//Valindando email        

                                if($mailcheck==FALSE)
                                {
                                    echo "Email no Valido<br>";
                                }else
                                {
                                    $subject = "Codigo de Confirmacion en F++";
                                    $message = "Tu código de confirmación es: $codigo_confirmacion";        

                                    if(mysqli_query($con,$sql2) && mail($newmail, $subject, $message, "From: register@fanplusplus.com") && mysqli_query($con,$sql))
                                    {
                                        echo '<div class="alert alert-success" >
                                        <p>Actualización Exitosa</p>
                                        <p>Hemos enviado un codigo de confirmación a: '.$newmail.'</p>
                                        </div>';
                                    }else
                                    {
                                        echo '<div class="alert alert-danger" ><p>Error al enviar email a email.</p></div>';
                                    }
                                }
                            }        

                        }else//Si la contraseña es incorrecta
                        {
                            echo '<div class="alert alert-danger" ><p>Contraseña Incorrecta</p></div>';
                        }
                    }
                    echo '</article>';
                    mysqli_close($con);
                }else
                {
                    echo '<div class="alert alert-info"><p>Debes <a href="../signin">Iniciar Sesión</a> para editar tu información.</p></div>';
                }        

                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-4">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
</body>
</html>