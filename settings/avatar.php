<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Configuración de Avatar- Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Panel para actualizar el avatar del usuario en Fan plus plus."/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <!-- Modal para ver foto de inicio-->                    
            <div class="modal fade bs-example-modal-sm-profileimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Avatar</h4>
                        </div>
                        <div class="modal-body">
                            <?php
                            include '../link.php';
                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                            $user_id = mysqli_real_escape_string($con,$_SESSION['user_id']);
                            $username = mysqli_real_escape_string($con,$_SESSION['username']);
                            $result = mysqli_query($con,"SELECT AVATAR,PW,USERNAME FROM profiles WHERE ID=".mysqli_real_escape_string($con,$user_id)."");
                            $row = mysqli_fetch_array($result);  

                            echo '<img src="../'.$row['AVATAR'].'" class="img-responsive">';

                            mysqli_close($con);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para ver foto de inicio-->

            <div class="col-xs-12 col-sm-1 col-md-4">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-4">
                <?php
                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }
                
                $fileErr = $pw2Err = "";        

                if($_SERVER['REQUEST_METHOD'] == "POST")
                {
                    if(empty($_FILES['file']['name']))
                    {
                        $fileErr = "Selecciona una imágen";
                    }else
                    {
                        $extPosibles = array("gif", "jpeg", "jpg", "png");//Extensiones de imagenes posible de subir al servidor
                        $aux = explode(".", $_FILES["file"]["name"]);
                        $extension = end($aux);//Llendo por la extension
                        if(!(($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || 
                            ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || 
                            ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png"))//Formatos permitidos
                            && !in_array($extension, $extPosibles))
                        {
                            $fileErr = "El archivo debe ser una imágen";
                        }elseif($_FILES["file"]["size"] > 4000000)
                        {
                            $fileErr = "Imágen: Debe pesar menos de 5 Mb";
                        }else
                        {
                            $filename = $_FILES['file']['name'];
                        }
                    }            

                    //Validando el pw2
                    if(empty($_POST['pw2']))
                    {
                        $pw2Err = "Contraseña Incorrecta";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['pw2']))
                    {
                        $pw2Err = "Contraseña Incorrecta";
                    }elseif(strlen($_POST['pw2']) < 6)
                    {
                        $pw2Err = "Contraseña Incorrecta";
                    }else
                    {
                        $pw2 = test_input($_POST['pw2']);
                    }
                } 

                //Mostrando los errores solo si existen y son diferentes a ""
                if(isset($fileErr) && ($fileErr != ""))
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$fileErr.'</p>
                        </div>';
                }  
                if(isset($pw2Err) && ($pw2Err != ""))
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$pw2Err.'</p>
                        </div>';
                }  
      

                ?>
                <?php
                if(isset($_SESSION['username']) && isset($_SESSION['user_id']))
                {
                    //Trayendo la información de la base de datos
                    include '../link.php';
                    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                    $user_id = mysqli_real_escape_string($con,$_SESSION['user_id']);
                    $username = mysqli_real_escape_string($con,$_SESSION['username']);
                    $result = mysqli_query($con,"SELECT ID,AVATAR,PW,USERNAME FROM profiles WHERE ID=".mysqli_real_escape_string($con,$user_id)."");
                    $row = mysqli_fetch_array($result);        

                    //Formulario para cambiar el avatar
                    echo '<div class="btn-group">
                            <a class="btn btn-default" href="../settings/" role="button">Básico</a>
                            <a class="btn btn-default btn-select" href="../settings/avatar" role="button">Avatar</a>
                            <a class="btn btn-default" href="../settings/email" role="button">Email</a>
                            <a class="btn btn-default" href="../settings/pass" role="button">Contraseña</a>
                        </div>
                        <h1>Configura tu Avatar</h1>
                        <form role="form" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <a href="" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg"><img src="../'.$row['AVATAR'].'" height="100" width="100"></a>
                            </div>
                            <div class="form-group">
                                <input name="file" type="file" title="Seleccionar Imágen">
                                <p class="help-block">Archivo de imágen menor a o igual a 5mb</p>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Contraseña</label>
                                <input name="pw2" type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Actualizar</button>
                            </div>
                        </form>';

                    //Si ya no hay mensajes de error y las variables se crean entonces prosigue
                    if(isset($pw2) && isset($filename))
                    {
                        //Verificando pw en db diferente
                        $con2 = mysqli_connect('localhost','django83_3dmx','smuse_(6)','django83_jal3001');
                        $result2 = mysqli_query($con2,"SELECT PW FROM pws_rellic WHERE USER_ID='".$row['ID']."' ");
                        $row2 = mysqli_fetch_array($result2);

                        if(md5($pw2) == $row2['PW'])//Si las contraseñas coinciden
                        {
                            $aux = explode(".", $_FILES["file"]["name"]);
                            $extension = end($aux);//Llendo por la extension
                            $ruta = "/profilesavatar/".strtolower($row['USERNAME']).".".$extension;        

                            //Actualizando en la db
                            if(mysqli_query($con,"UPDATE profiles SET AVATAR='".$ruta."' WHERE ID='".$user_id."' AND USERNAME='".$username."' AND PW='".md5($pw2)."'"))
                            {
                                $success_men = 1;
                            }else
                            {
                                echo "Problemas actualizando la base de datos con la nueva imagen<br>";
                            }        

                            //Borrando imagen del servidor si es default sino no
                            if(!($row['AVATAR'] == "/profilesavatar/default.jpg"))
                            {
                                $a_borrar = "/home/django83/public_html".$row['AVATAR'];
                                if(unlink($a_borrar))
                                {
                                    $success_men++;
                                }else
                                {
                                    echo "Imagen NO Borrada del servidor.<br>";
                                }
                            }else
                            {
                                echo "La imagen no se borro por que es la de Default.<br>";
                            }        

                            //Copiando imagen al servidor
                            $ruta2 = "/home/django83/public_html/profilesavatar/".strtolower($row['USERNAME']).".".$extension;
                            if(move_uploaded_file($_FILES['file']['tmp_name'], $ruta2))
                            {
                                if($success_men == 2)
                                {
                                    echo '<div class="alert alert-success" ><p>Actualización Exitosa</p></div>';
                                    echo '<script>location.reload();</script>';
                                }
                            }else
                            {
                                echo "Problemas al copiar la imagen al servidor</br>";
                            }
                        }else//Si la contraseña es incorrecta
                        {
                            echo '<div class="alert alert-danger" ><p>Contraseña Incorrecta</p></div>';
                        }
                    }

                    mysqli_close($con);
                }else
                {
                    echo '<div class="alert alert-info"><p>Debes <a href="../signin">Iniciar Sesión</a> para editar tu información.</p></div>';
                }        

                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-4">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
    <script src="../static/js/file-input.js"></script>
    <!-- File input script -->
    <script>
    $('input[type=file]').bootstrapFileInput();
    $('.file-inputs').bootstrapFileInput();
    </script>
</body>
</html>