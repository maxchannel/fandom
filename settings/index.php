<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Configuración Básica - Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Panel para cambiar la configuración básica de el usuario en Fan plus plus."/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-4">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-4">
                <?php
                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }
                
                $firstnameErr = $pw2Err = $urlErr = $bioErr = "";        

                if($_SERVER['REQUEST_METHOD'] == "POST")
                {
                    //Firstname
                    if (empty($_POST["firstname"]))
                    {
                       $firstnameErr = "Nombre: Campo Obligatorio";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST["firstname"]))//Caracteres permitidos
                    {
                        $firstnameErr = "Nombre: solo se permiten Letras, Números y Espacios en Blanco";
                    }elseif(strlen($_POST["firstname"]) > 30)//Longitud mínima de 5 Caracteres
                    {
                        $firstnameErr = "Nombre: Escribe 30 caracteres o menos";
                    }else
                    {
                        $firstname = test_input($_POST["firstname"]);
                    }        

                    //Validando el pw2
                    if(empty($_POST['pw2']))
                    {
                        $pw2Err = "Debes escribir tu contraseña para poder editar.";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['pw2']))
                    {
                        $pw2Err = "Contraseña Incorrecta";
                    }elseif(strlen($_POST['pw2']) < 6)
                    {
                        $pw2Err = "Contraseña Incorrecta";
                    }else
                    {
                        $pw2 = test_input($_POST['pw2']);
                    }        

                    //Variables en select
                    $country = test_input($_POST['country']);
                    $sex = test_input($_POST['sex']);
                    $language = test_input($_POST['language']);        

                    //URL
                    if(!empty($_POST['url']))
                    {
                        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$_POST['url']))
                        {
                            $urlErr = "URL no Válido"; 
                        }else
                        {
                            function toLink($text)
                            {
                                $text = html_entity_decode($text);
                                $text = " ".$text;
                                $text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                                '<a href="\1" target="_blank">\1</a>', $text);
                                $text = eregi_replace('(((f|ht){1}tps://)[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                                '<a href="\1" target="_blank">\1</a>', $text);
                                $text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                                '\1<a href="http://\2" target="_blank">\2</a>', $text);
                                $text = eregi_replace('([_.0-9a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,3})',
                                '<a href="mailto:\1" target="_blank">\1</a>', $text);
                                return $text;
                            }
                            $url = toLink($_POST['url']);
                        }
                    }        

                    //Biografía
                    if(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['biography']))
                    {
                        $bioErr = "La biografía solo se permiten Letras, Números y Espacios en Blanco";
                    }elseif(strlen($_POST['biography']) > 250)
                    {
                        $bioErr = "La biografía solo puede tener 250 caracteres o menos.";
                    }else
                    {
                        $biography = test_input($_POST['biography']);
                    } 

                    //Mostrando los errores solo si existen y son diferentes a ""
                    if(isset($firstnameErr) && ($firstnameErr != ""))
                    {
                        echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$firstnameErr.'</p>
                        </div>';
                    }  
                    if(isset($pw2Err) && ($pw2Err != ""))
                    {
                        echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$pw2Err.'</p>
                        </div>';
                    }  
                    if(isset($urlErr) && ($urlErr != ""))
                    {
                        echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$urlErr.'</p>
                        </div>';
                    }  
                    if(isset($bioErr) && ($bioErr != ""))
                    {
                        echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$bioErr.'</p>
                        </div>';
                    }         

                }        

                ?>
                <?php
                if(isset($_SESSION['username']) && isset($_SESSION['user_id']))
                {        
                    include '../link.php';
                    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                    $user_id = mysqli_real_escape_string($con,$_SESSION['user_id']);
                    $username = mysqli_real_escape_string($con,$_SESSION['username']);
                    $result = mysqli_query($con,"SELECT * FROM profiles WHERE ID=".mysqli_real_escape_string($con,$user_id)."");
                    $row = mysqli_fetch_array($result);

                    //Cambiar lo basico
                    echo '<div class="btn-group">
                            <a class="btn btn-default btn-select" href="../settings/" role="button">Básico</a>
                            <a class="btn btn-default" href="../settings/avatar" role="button">Avatar</a>
                            <a class="btn btn-default" href="../settings/email" role="button">Email</a>
                            <a class="btn btn-default" href="../settings/pass" role="button">Contraseña</a>
                        </div>
                    <h1>Configura lo Básico</h1>
                    <form role="form" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" method="post" >
                        <div class="form-group">
                            <label for="InputText1">Nombre</label>
                            <input name="firstname" type="text" class="form-control" id="InputText1" placeholder="Introduce tu nombre para mostrar" value="'.$row['FIRSTNAME'].'" />
                        </div>
                        <div class="form-group">
                            <label for="InputSelect1">País</label>
                            <select name="country" class="form-control" id="InputSelect1">
                                <option value="'.$row['COUNTRY'].'">'.$row['COUNTRY'].'</option>
                                <option value="Afghanistan"  >Afghanistan</option>
                                <option value="Algeria"  >Algeria</option>
                                <option value="Argentina"  >Argentina</option>
                                <option value="Australia"  >Australia</option>
                                <option value="Austria"  >Austria</option>
                                <option value="Bahamas"  >Bahamas</option>
                                <option value="Bangladesh"  >Bangladesh</option>
                                <option value="Barbados"  >Barbados</option>
                                <option value="Barbados"  >Barbados</option>
                                <option value="Belice"  >Belice</option>
                                <option value="Bermudas"  >Bermudas</option>
                                <option value="Bolivia"  >Bolivia</option>
                                <option value="Brazil"  >Brazil</option>
                                <option value="Brunei Darussalam"  >Brunei Darussalam</option>
                                <option value="Bulgaria"  >Bulgaria</option>
                                <option value="Canadá"  >Canadá</option>
                                <option value="República checa"  >República checa</option>
                                <option value="Chile"  >Chile</option>
                                <option value="China"  >China</option>
                                <option value="Colombia"  >Colombia</option>
                                <option value="Costa Rica"  >Costa Rica</option>
                                <option value="Croacia"  >Croacia</option>
                                <option value="Dinamarca"  >Dinamarca</option>
                                <option value="República Dominicana"  >República Dominicana</option>
                                <option value="Ecuador"  >Ecuador</option>
                                <option value="Egipto"  >Egipto</option>
                                <option value="Estonia"  >Estonia</option>
                                <option value="Estados Unidos"  >Estados Unidos</option>
                                <option value="Spain"  >España</option>
                                <option value="Inglaterra"  >Inglaterra</option>
                                <option value="Finlandia"  >Finlandia</option>
                                <option value="Francia"  >Francia</option>
                                <option value="Alemania"  >Alemania</option>
                                <option value="Guatemala"  >Guatemala</option>
                                <option value="Grecia"  >Grecia</option>
                                <option value="Honduras"  >Honduras</option>
                                <option value="Hong Kong"  >Hong Kong</option>
                                <option value="Hungría"  >Hungría</option>
                                <option value="Islandia"  >Islandia</option>
                                <option value="India"  >India</option>
                                <option value="Indonesia"  >Indonesia</option>
                                <option value="Irlanda"  >Irlanda</option>
                                <option value="Israel"  >Israel</option>
                                <option value="Italia"  >Italia</option>
                                <option value="Jamaica"  >Jamaica</option>
                                <option value="Japón"  >Japón</option>
                                <option value="Jordania"  >Jordania</option>
                                <option value="Kenya"  >Kenya</option>
                                <option value="Kuwait"  >Kuwait</option>
                                <option value="Irak"  >Irak</option>
                                <option value="Letonia"  >Letonia</option>
                                <option value="Líban"  >Líbano</option>
                                <option value="Liechtenstein"  >Liechtenstein</option>
                                <option value="Lituania"  >Lituania</option>
                                <option value="Luxemburgo"  >Luxemburgo</option>
                                <option value="Malasia"  >Malasia</option>
                                <option value="Maldivas"  >Maldivas</option>
                                <option value="Malta"  >Malta</option>
                                <option value="México"  >México</option>
                                <option value="Monaco"  >Monaco</option>
                                <option value="Marruecos"  >Marruecos</option>
                                <option value="Nepal"  >Nepal</option>
                                <option value="Holanda"  >Holanda</option>
                                <option value="Nueva Zelanda"  >Nueva Zelanda</option>
                                <option value="Noruega"  >Noruega</option>
                                <option value="Pakistán"  >Pakistán</option>
                                <option value="Panamá"  >Panamá</option>
                                <option value="Paraguay"  >Paraguay</option>
                                <option value="Perú"  >Perú</option>
                                <option value="Philippines"  >Philippines</option>
                                <option value="Polonia"  >Polonia</option>
                                <option value="Portugal"  >Portugal</option>
                                <option value="Puerto Rico"  >Puerto Rico</option>
                                <option value="Qatar"  >Qatar</option>
                                <option value="Rumania"  >Rumania</option>
                                <option value="Rusia"  >Rusia</option>
                                <option value="Arabia Saudita"  >Arabia Saudita</option>
                                <option value="Escocia"  >Escocia</option>
                                <option value="Singapur"  >Singapur</option>
                                <option value="Eslovenia"  >Eslovenia</option>
                                <option value="Sudáfrica"  >Sudáfrica</option>
                                <option value="Korea del Sur"  >Korea del Sur</option>
                                <option value="Sri Lanka"  >Sri Lanka</option>
                                <option value="Suecia"  >Suecia</option>
                                <option value="Suiza"  >Suiza</option>
                                <option value="Taiwán"  >Taiwán</option>
                                <option value="Tailandia"  >Tailandia</option>
                                <option value="Turquía"  >Turquía</option>
                                <option value="Uganda"  >Uganda</option>
                                <option value="Ucrania"  >Ucrania</option>
                                <option value="Emiratos árabes Unidos"  >Emiratos árabes Unidos</option>
                                <option value="Reino Unido"  >Reino Unido</option>
                                <option value="Venezuela"  >Venezuela</option>
                                <option value="Vietnam"  >Vietnam</option>
                                <option value="Yugoslavia"  >Yugoslavia</option>
                                <option value="Gales"  >Gales</option>
                                <option value="República Dominicana"  >República Dominicana</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="InputSelect2">Género</label>
                            <select name="sex" class="form-control" id="InputSelect2">
                                <option>'.$row['SEX'].'</option>
                                <option value="Male">Masculino</option>
                                <option value="Female">Femenino</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="InputSelect3">Idioma</label>
                            <select name="language" class="form-control" id="InputSelect3">
                                <option>'.$row['LANGUAGE'].'</option>
                                <option value="English">Inglés</option>
                                <option value="Spanish" >Español</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento</label>
                            <div class="form-control">'.$row['BIRTHDAY'].'</div>
                        </div>
                        <div class="form-group">
                            <label for="InputText2">Sitio Web ('.$row['URL'].' )</label>
                            <input name="url" type="text" class="form-control" id="InputText2" placeholder="Formato: www.twitter.com/true_max" />
                        </div>
                        <div class="form-group">
                            <label for="InputTextarea1">Biografía</label>
                            <textarea name="biography" class="form-control" id="InputTextarea1" placeholder="Introduce tu nombre para mostrar">'.$row['BIOGRAPHY'].'</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña</label>
                            <input name="pw2" type="password" class="form-control" id="exampleInputPassword1" >
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                        </div>
                    </form>';

                    //Cambiando el avatar    
                    if(isset($firstname) && isset($pw2) && isset($country) && isset($sex) && isset($language))
                    {
                        //Verificando pw en db diferente
                        $con2 = mysqli_connect('localhost','django83_3dmx','smuse_(6)','django83_jal3001');
                        $result2 = mysqli_query($con2,"SELECT PW FROM pws_rellic WHERE USER_ID='".$row['ID']."' ");
                        $row2 = mysqli_fetch_array($result2);

                        //Todo el proceso de UPDATE en la base de datos
                        if(md5($pw2) == $row2['PW'])
                        {
                            //Determinando si se envio la url para actualizar la db
                            if(isset($url))
                            {
                                $sql = "UPDATE profiles SET FIRSTNAME='".$firstname."',COUNTRY='".$country."',SEX='".$sex."',LANGUAGE='".$language."',URL='".$url."',BIOGRAPHY='".$biography."' WHERE ID='".$user_id."' AND USERNAME='".$username."'";
                            }else
                            {
                                $sql = "UPDATE profiles SET FIRSTNAME='".$firstname."',COUNTRY='".$country."',SEX='".$sex."',LANGUAGE='".$language."',BIOGRAPHY='".$biography."' WHERE ID='".$user_id."' AND USERNAME='".$username."'";
                            }
                            
                            if(mysqli_query($con,$sql))
                            {
                                echo '<div class="alert alert-success" ><p>Actualización Exitosa</p></div>';
                                echo '<script>location.reload();</script>';
                            }
                        }else
                        {
                            echo '<div class="alert alert-danger" ><p>Contraseña Incorrecta</p></div>';
                        }
                    } 


                }else
                {
                    echo '<div class="alert alert-warning" role="alert">
                            <p>Debes <a href="../../signin.php">Iniciar Sesión</a> o <a href="../../signup.php">Registrarte</a> para ver este contenido.</p>
                        </div>';
                }        

                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-4">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
</body>
</html>