<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Configuración de la Contraseña - Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="../static/favicon.ico" />
    <meta name="description" content="Panel para actualizar la contraseña del usuario en Fan plus plus."/>
    <link href="../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "../static/analyticstracking.php" ?>
    <?php include '../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-4">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-4">
                <?php
                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }
                
                $oldErr = $new1Err = $new2Err = "";        

                if($_SERVER['REQUEST_METHOD'] == "POST")
                {
                    if(empty($_POST['old']))
                    {
                        $oldErr = "Contraseña Actual: Campo Obligatorio";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['old']))
                    {
                        $oldErr = "Contraseña Actual : Contraseña Incorrecta";
                    }elseif(strlen($_POST['old']) < 6)
                    {
                        $oldErr = "Contraseña Actual: Contraseña Incorrecta";
                    }else
                    {
                        $old = test_input($_POST['old']);
                    }        

                    if(empty($_POST['new1']))
                    {
                        $new1Err = "Contraseña Nueva: Campo Obligatorio";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['new1']))
                    {
                        $new1Err = "Contraseña Nueva: Solo puede contener letras, numeros y espacios";
                    }elseif(strlen($_POST['new1']) < 6)
                    {
                        $new1Err = "Contraseña Nueva: Debe tener una longitud minima de 6";
                    }else
                    {
                        $new1 = test_input($_POST['new1']);
                    }        

                    if(empty($_POST['new2']))
                    {
                        $new2Err = "Confirma de la Contraseña nueva: Campo Obligatorio";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['new2']))
                    {
                        $new2Err = "Confirma de la Contraseña nueva: Solo puede contener letras, numeros y espacios";
                    }elseif(strlen($_POST['new2']) < 6)
                    {
                        $new2Err = "Confirma de la Contraseña nueva: Debe tener una longitud minima de 6";
                    }else
                    {
                        $new2 = test_input($_POST['new2']);
                    }        

                }    

                //Mostrando los errores solo si existen y son diferentes a ""
                if(isset($oldErr) && ($oldErr != ""))
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$oldErr.'</p>
                        </div>';
                }  
                if(isset($new1Err) && ($new1Err != ""))
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$new1Err.'</p>
                        </div>';
                }  
                if(isset($new2Err) && ($new2Err != ""))
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$new2Err.'</p>
                        </div>';
                }      

                if(isset($_SESSION['username']) && isset($_SESSION['user_id']))
                {
                    //Trayendo la información de la base de datos
                    include '../link.php';
                    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                    $user_id = mysqli_real_escape_string($con,$_SESSION['user_id']);
                    $username = mysqli_real_escape_string($con,$_SESSION['username']);
                    $result = mysqli_query($con,"SELECT ID,PW,USERNAME FROM profiles WHERE ID=".mysqli_real_escape_string($con,$user_id)."");
                    $row = mysqli_fetch_array($result);        

                    //Formulario para cambiar el avatar
                    echo '<div class="btn-group">
                            <a class="btn btn-default" href="../settings/" role="button">Básico</a>
                            <a class="btn btn-default" href="../settings/avatar" role="button">Avatar</a>
                            <a class="btn btn-default" href="../settings/email" role="button">Email</a>
                            <a class="btn btn-default btn-select" href="../settings/pass" role="button">Contraseña</a>
                        </div>
                        <h1>Configura tu Contraseña</h1>
                        <form role="form" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" method="post">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Contraseña Actual</label>
                                <input name="old" type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword2">Contraseña Nueva</label>
                                <input name="new1" type="password" class="form-control" id="exampleInputPassword2" placeholder="Contraseña">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword3">Confirma la Contraseña</label>
                                <input name="new2" type="password" class="form-control" id="exampleInputPassword3" placeholder="Contraseña">
                            </div>
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                        </form>';

                    //Si ya no hay mensajes de error y las variables se crean entonces prosigue
                    if(isset($old) && isset($new1) && isset($new2) && ($new1==$new2) && isset($_SESSION['user_id']))
                    {
                        //Verificando pw en db diferente
                        $con2 = mysqli_connect('localhost','django83_3dmx','smuse_(6)','django83_jal3001');
                        $result2 = mysqli_query($con2,"SELECT PW FROM pws_rellic WHERE USER_ID='".$row['ID']."' ");
                        $row2 = mysqli_fetch_array($result2);

                        if(md5($old) == $row2['PW'])//Si las contraseñas coinciden
                        {
                            $newpw = md5($new1);
                            $user_id = mysqli_real_escape_string($con,$_SESSION['user_id']);
                            $old = md5($old);        

                            if(mysqli_query($con2, "UPDATE pws_rellic SET PW='".$newpw."' WHERE PW='".$old."' AND USER_ID='".$user_id."'"))
                            {
                                echo '<div class="alert alert-success" ><p>La constraseña se actualizo</p></div>';
                            }else
                            {
                                echo '<div class="alert alert-danger" ><p>Error al actualizar.</p></div>';
                            }
                        }else//Si la contraseña es incorrecta
                        {
                            echo '<div class="alert alert-danger" ><p>Contraseña Incorrecta</p></div>';
                        }
                    }elseif($new1 != $new2)
                    {
                        echo '<div class="alert alert-danger" ><p>Las Contraseñas no Coinciden</p></div>';
                    }
                    echo '</article>';        

                }else
                {
                    echo '<div class="alert alert-info"><p>Debes <a href="../signin">Iniciar Sesión</a> para editar tu información.</p></div>';
                }        

                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-4">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
</body>
</html>