<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <?php 
    if(isset($_GET['q']) && !empty($_GET['q']))
    {
        function test_input($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
        $q = test_input($_GET['q']);
    }
    
    echo '<title>Buscar '.$q.' en Fan Plus Plus</title>';
    echo "\n";
    echo '<meta name="description" content="Busqueda de: '.$q.'" />';
    echo "\n"; 
    ?>
    <meta name="description" content="Fan Plus Plus es un comunidad donde puedes crear y unirte a Fandoms organizados por categorías."/>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes permitidas-->
    <link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Crafty+Girls' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Devonshire' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Electrolize' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Fanwood+Text' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Kite+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway+Dots' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=The+Girl+Next+Door' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Walter+Turncoat' rel='stylesheet' type='text/css'>
    <!-- Fuentes permitidas-->
</head>
<body>
    <?php include "static/analyticstracking.php" ?>
    <?php include 'static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <!-- Busquedas recientes -->
                <div class="panel panel-default">
                    <div class="panel-heading">Busquedas Recientes</div>               
                    <ul class="list-group">
                        <li class="list-group-item"><a href="search.php?q=hola+mundo">Hola mundo</a></li>
                        <li class="list-group-item"><a href="search.php?q=justified">Justified</a></li>
                        <li class="list-group-item"><a href="search.php?q=neymar">Neymar</a></li>
                        <li class="list-group-item"><a href="search.php?q=watchdogs">Watch Dogs</a></li>
                        <li class="list-group-item"><a href="search.php?q=max">Max</a></li>
                        <li class="list-group-item"><a href="search.php?q=panchebrios">Panchebrios</a></li>
                        <li class="list-group-item"><a href="search.php?q=gta+5">GTA 5</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
                <!-- Ads -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        Ads
                    </div>
                </div>

                <!-- Modal para reportar a usuario-->                   
                <div class="modal fade bs-example-modal-sm-report-u" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Usuario</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Usuario Molesto</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para reportar a usuario-->
                <!-- Modal para reportar imagen -->                   
                <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Imágen de Max</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Spam</option>
                                            <option>Usuario Molesto</option>
                                            <option>Apología a la Violencia</option>
                                            <option>Desnudez</option>
                                            <option>Derechos de Autor</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para reportar imagen -->  
                <!-- Modal para reportar a fandom-->                   
                <div class="modal fade bs-example-modal-sm-report-f" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Fandom</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Fandom Repetido</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para reportar a fandom-->
                <!-- Modal de reporte de publicacion-->                   
                <div class="modal fade bs-example-modal-sm-report-p" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar una Publicación</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Contenido Ofensivo</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal de reporte de publicacion-->
                <!-- Modal de reporte de nota-->                   
                <div class="modal fade bs-example-modal-sm-report-n" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Nota</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Contenido Ofensivo</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal de reporte de nota-->
                <!-- Modal para enviar error-->                    
                <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Informar sobre error</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para error-->

                <?php
                if(isset($q) && !empty($q)) 
                {    
                    include 'drivers/rio.php';
                    $search = new Busqueda($q);
                    $search->Buscar();
                }

                ?> 
                
            </div>
            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-2"></div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include 'static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <script src="static/js/fluid.js"></script>
</body>
</html>