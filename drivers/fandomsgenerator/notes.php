<?php
//Clase para usar en los perfiles en publications.php
class fandomNotes
{
    private $id;
    private $username;

    public function fandomNotes($id,$username)
    {
        $this->id = $id;
        $this->username = $username;
    }

    public function generateNotes()
    {
        session_start();
        echo '<!DOCTYPE html>';
        echo "\n";
        echo '<html lang="es">';
        echo "\n";
        echo '<head>';
        echo "\n";
        echo '    <meta charset="utf-8">';
        echo "\n";
        echo '   <title>Notas en ';
        require '../../drivers/houston.php';
        $generic = new Fandom($this->id);
        $generic->getLucky("NAME");
        echo ' - F++</title>';
        echo "\n";
        echo '    <meta http-equiv="X-UA-Compatible" content="IE=edge">';
        echo "\n";
        echo '    <meta name="viewport" content="width=device-width, initial-scale=1">';
        echo "\n";
        echo '    <meta name="description" content="Fandom donde todos los fans de watchdogs comparten notas, publicaciones, videos, links y más."/>';
        echo "\n";
        echo '    <link href="../../static/css/bootstrap.css" rel="stylesheet">';
        echo "\n";
        echo '    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">';
        echo "\n";
        echo '    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">';
        echo "\n";
        echo '</head>';
        echo "\n";
        echo '<body>';
        echo "\n";
        include "../../static/analyticstracking.php";
        echo "\n";
        include "../../static/navs/nav.php";
        //Inicio de contenido
        echo "\n";
        echo '<div class="container-fluid">
        <!-- Ads y perfil-->
        <section class="row">
            <!-- Modal para enviar error-->                    
            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Informar sobre error</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para error-->
            <!-- Modal para ver foto de inicio-->                    
            <div class="modal fade bs-example-modal-sm-profileimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">';
                            $generic->getLucky("NAME");
echo                        '</h4>
                        </div>
                        <div class="modal-body">
                            <img src="../../';
                            $generic->getLucky("AVATAR");
echo                        '" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para ver foto de inicio-->
            <!-- Modal para reportar imagen -->                   
            <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Imágen de Max</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Usuario Molesto</option>
                                        <option>Apología a la Violencia</option>
                                        <option>Desnudez</option>
                                        <option>Derechos de Autor</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar imagen -->  
            <!-- Modal para sugerir que imagen se quite del fandom-->                    
            <div class="modal fade bs-example-modal-sm-suger-i" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar una Imágen</h4>
                        </div>
                        <div class="modal-body">
                            <p>Cuando eres miembro de un fandom puedes votar para que un contenido se elimine, ¿Seguro deseas votar para que esta Imágen se elimine?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para sugerir que imagen se quite del fandom--> 
            <!-- Modal para reportar a fandom-->                   
            <div class="modal fade bs-example-modal-sm-report-f" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Fandom</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Fandom Repetido</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar a fandom-->

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="';
                        $generic->getLucky("NAME");
echo                    '" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg" ><img src="../../';
                        $generic->getLucky("AVATAR");
echo                    '" class="img-responsive"></a>
                        <h1 class="fandom-h1">';
                        $generic->getLucky("NAME");
echo                    '</h1>';
echo                    '<p>';
                        $generic->getLucky("DESCRIPTION");
echo                    '</p>';
echo                    '<div class="row">';
echo                    '    <div class="col-md-9">';
                        if(isset($_SESSION['username']))
                        {
                            echo '<div id="secBTN">';;
                            $generic->ifFan($this->id, $_SESSION['user_id']);
                            echo '</div>';
                        }else
                        {
                            echo '<a href="../../signin.php" class="btn btn-primary" >Login</a>';
                        }
echo                    '    </div>';
echo                    '    <div class="col-md-3">';
echo '                           <!-- Boton dropdown-->
                                 <div class="btn-group pull-right">
                                     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                         <span class="caret"></span>
                                     </button>
                                     <ul class="dropdown-menu" role="menu">
                                         <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                         <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                     </ul>
                                 </div>';
echo                    '    </div>';
echo                    '</div>';
echo '                
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p><a href="';
                        $generic->getLucky("WEBSITE");
echo                    '" target="_blank" >Sitio Web</a></p>
                        <p><a href="https://www.facebook.com/';
                        $generic->getLucky("FB");
echo                    '" target="_blank" >';
                        $generic->getLucky("FB");
echo                    '</a></p>
                        <p><a href="https://www.twitter.com/';
                        $generic->getLucky("TWTR");
echo                    '" target="_blank" >';
                        $generic->getLucky("TWTR");
echo                    '</a></p>
                        <p>Categoría: <a href="../../fandoms/'.strtolower($generic->getLucky("CATEGORY")).'.php">'.$generic->getLucky("CATEGORY").'</a></p>
                    </div>
                </div>
            </div>
            <!-- Botonera para moverde dentro del fandom -->
            <div class="col-xs-12 visible-xs">
                <div class="list-group">
                    <a href="/'.$generic->getLucky("RETURNURL").'" class="list-group-item">'.$generic->getLucky("RETURNNAME").'</a>
                    <a href="../images" class="list-group-item">Imágenes ('.$generic->Numbers($this->id,"IMAGES").')</a>
                    <a href="../publications" class="list-group-item">Publicaciones ('.$generic->Numbers($this->id,"PUBLICATIONS").')</a>
                    <a href="../notes" class="list-group-item active">Notas ('.$generic->Numbers($this->id,"NOTES").')</a>
                    <a href="../members" class="list-group-item">Miembros ('.$generic->Numbers($this->id,"FANS").')</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <h2>Notas en '.$generic->getLucky("RETURNNAME").'</h2>
                <!-- Formulario users-->';
                //VALIDACION DEL FOMULARIO E IMPRESION DE ERRORES
                $noteErr=$fontErr="";

                //Validacion
                if ($_SERVER["REQUEST_METHOD"] == "POST")
                {
                    function test_input($data)
                    {
                        $data = trim($data);
                        $data = stripslashes($data);
                        $data = htmlspecialchars($data);
                        return $data;
                    }    

                    //Validando nota
                    if (empty($_POST['note']))
                    {
                       $noteErr = "Las publicación no puede estar vacía.";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['note']))//Caracteres permitidos
                    {
                        $noteErr = "Publicación: solo se permiten Letras, Números y Espacios en Blanco";
                    }elseif(strlen($_POST['note']) > 160)//Longitud mínima de 5 Caracteres
                    {
                        $noteErr = "Publicación: escribe 160 caracteres o menos";
                    }else
                    {
                        $note = test_input($_POST['note']);
                    }   

                    if(empty($_POST["font"]))
                    {
                       $fontErr = "Selecciona la fuente de tu nota";
                    }else
                    {
                        $font = test_input($_POST["font"]);
                    }   

                }

                //Mostrando errores
                if(isset($noteErr) && ($noteErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$noteErr.'</p>
                        </div>';
                }
                if(isset($fontErr) && ($fontErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$fontErr.'</p>
                        </div>';
                }

                //Creando post
                if(isset($note) && isset($font) && isset($_SESSION['username']))
                {
                    require '../../link.php';
                    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                    $register = date("Y-m-d");
                    $note = mysqli_real_escape_string($con, $note);
                    $font = mysqli_real_escape_string($con, $font);
                    $fandom_id = mysqli_real_escape_string($con, $this->id);
                    $profile_id = mysqli_real_escape_string($con, $_SESSION['user_id']);   
                       
                    $sql = "INSERT INTO fandom_notes(NOTE,PROFILE_ID,FANDOM_ID,REGISTER,FONT) VALUES('".$note."','".$profile_id."','".$fandom_id."','".$register."','".$font."')";                        

                    if(mysqli_query($con,$sql))
                    {
                        $sql2 = "SELECT ID FROM fandom_notes WHERE NOTE='".$note."' AND PROFILE_ID='".$profile_id."' AND FANDOM_ID='".$fandom_id."' AND FONT='".$font."'";
                        $result = mysqli_query($con,$sql2);
                        $row = mysqli_fetch_array($result);
                        
                        $result2 = mysqli_query($con,"SELECT ID,FANDOM,MODE,URL FROM fandoms WHERE ID='".$fandom_id."'");
                        $row2 = mysqli_fetch_array($result2);

                        //Insertando valor para las NOTIFICACIONES
                        $date = date("d/m/Y h:i");
                        $selectSQL = "SELECT MEMBER_ID FROM fandom_members WHERE FANDOM_ID='".$fandom_id."' ";
                        $selectF = mysqli_query($con,$selectSQL);
                        while($rowSel = mysqli_fetch_array($selectF))
                        {
                            $sql = "INSERT INTO notifications_profile(CODE,VIEW,USER_ID,WHO_ID,POST_ID,FANDOM_ID,FECHA) VALUES('5','0','".$rowSel['MEMBER_ID']."','".$profile_id."','".$row['ID']."','".$fandom_id."','".$date."') ";
                            mysqli_query($con,$sql); 
                        }

                        //Insertando para actividad del fandom
                        $activitySQL = "INSERT INTO activity_fandom(CODE,FANDOM_ID,USER_ID,POST_ID,FECHA) VALUES('2','".$this->id."','".$profile_id."','".$row['ID']."','".$date."')";
                        mysqli_query($con,$activitySQL);
                        
                        if($row2['MODE'] == "fandom")
                        {
                            if($fi = fopen("/home/django83/public_html/".strtolower($row2['FANDOM'])."/notes/".$row['ID'].".php","w"))
                            {
                                fwrite($fi, "<?php"."\n");
                                fwrite($fi, "require '/home/django83/public_html/drivers/fandomsgenerator/notesIndie.php';"."\n");
                                fwrite($fi, '$nota = new fandomNotesIndie('.$row2['ID'].','.$row['ID'].');'."\n");
                                fwrite($fi, '$nota->generateNotes();'."\n");
                                fwrite($fi, "?>"."\n");
                                fclose($fi);
                                echo '<div class="alert alert-success fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <p>Nota guardada</p>
                                    </div>';
                                $note=$font="";
                                //echo '<script> window.location="/'.strtolower($row2['FANDOM']).'/notes"; </script>';
                            }
                        }elseif($row2['MODE'] == "fans")
                        {
                            if($fi = fopen("/home/django83/public_html/".$row2['URL']."/notes/".$row['ID'].".php","w"))
                            {
                                fwrite($fi, "<?php"."\n");
                                fwrite($fi, "require '/home/django83/public_html/drivers/fandomsgenerator/notesIndie.php';"."\n");
                                fwrite($fi, '$nota = new fandomNotesIndie('.$row2['ID'].','.$row['ID'].');'."\n");
                                fwrite($fi, '$nota->generateNotes();'."\n");
                                fwrite($fi, "?>"."\n");
                                fclose($fi);
                                echo '<div class="alert alert-success fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <p>Nota guardada</p>
                                    </div>';
                                $note=$font="";
                                //echo '<script> window.location="/'.$row2['URL'].'/notes"; </script>';
                            }
                        }
                        
                    }else
                    {
                        echo "Error al insertar en la DB";
                    }                        

                    mysqli_close($con);
                }

                //Solo si exta logueado y es miembro del Fandom
                if(isset($_SESSION['user_id']) && $generic->ifMember($this->id ,$_SESSION['user_id']))
                {
                    echo '<div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default no-border">
                            <label><a href="">Fuentes disponibles</a></label>
                            <form role="form" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" method="post">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3" name="note" maxlength="160" placeholder="160 Caracteres" >'.$note.'</textarea>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="font">
                                        <option value="">Selecciona la Tipografía de tu nota</option>
                                        <option value="font1">Arial</option>
                                        <option value="font2">Arimo</option>
                                        <option value="font3">Crafty Girls</option>
                                        <option value="font4">Electrolize</option>
                                        <option value="font5">Fanwood Text</option>
                                        <option value="font6">Julius Sans One</option>
                                        <option value="font7">Josefin Sans</option>
                                        <option value="font8">Kite One</option>
                                        <option value="font9">Over the Rainbow</option>
                                        <option value="font10">Poiret One</option>
                                        <option value="font11">Raleway Dots</option>
                                        <option value="font12">Shadows Into Light</option>
                                        <option value="font13">The Girl Next Door</option>
                                        <option value="font14">Walter Turncoat</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </form>
                        </div>
                        <ol class="breadcrumb">
                          <li class="active">Top</li>
                          <li><a href="#">Todo</a></li>
                        </ol>
                    </div>';
                }else
                {
                    echo '<div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default no-border">
                            <p class="help-block"><a href="../../signin">Inicia sesión</a> y Únete a este Fandom para poder dejar una Nota.</p>
                        </div>
                        <ol class="breadcrumb">
                          <li class="active">Top</li>
                          <li><a href="#">Todo</a></li>
                        </ol>
                    </div>';
                }
echo '          <!-- Notas -->';
                $generic->getNotes(10);
echo '      </div>
            <!-- Botonera para moverde dentro del fandom -->
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 hidden-xs">
                        <div class="list-group">
                            <a href="/'.$generic->getLucky("RETURNURL").'" class="list-group-item">'.$generic->getLucky("RETURNNAME").'</a>
                            <a href="../images" class="list-group-item">Imágenes ('.$generic->Numbers($this->id,"IMAGES").')</a>
                            <a href="../publications" class="list-group-item">Publicaciones ('.$generic->Numbers($this->id,"PUBLICATIONS").')</a>
                            <a href="../notes" class="list-group-item active">Notas ('.$generic->Numbers($this->id,"NOTES").')</a>
                            <a href="../members" class="list-group-item">Miembros ('.$generic->Numbers($this->id,"FANS").')</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>&copy; 2014 Fanplusplus &middot; <a href="help.html" >Ayuda</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->';

        echo "\n";
        echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
        echo "\n";
        echo '<script src="../../static/js/bootstrap.min.js"></script>';        
        echo "\n";
        echo '<script src="../../static/js/file-input.js"></script>';  
        echo "\n";
        echo '<script src="../../static/js/fluid.js"></script>';       
        echo "\n";
        echo '<!-- File input script -->';        
        echo "\n";
        echo '<script>';        
        echo "\n";
        echo "$('input[type=file]').bootstrapFileInput();";        
        echo "\n";
        echo "$('.file-inputs').bootstrapFileInput();";     
        echo "\n";
        echo '</script>';    
        echo "\n";
        echo '</body>';
        echo "\n";
        echo '</html>';
    }

}

?>