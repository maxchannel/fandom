<?php
//NO USADO DE MOMENTO
class FandomPhotosIndie
{
    private $id;
    private $username;

    public function FandomPhotosIndie($id,$username)
    {
        $this->id = $id;
        $this->username = $username;
    }

    public function generatePhotosIndie()
    {
        session_start();
        echo '<!DOCTYPE html>';
        echo "\n";
        echo '<html lang="es">';
        echo "\n";
        echo '<head>';
        echo "\n";
        echo '    <meta charset="utf-8">';
        echo "\n";
        echo '   <title>Imágen en ';
        echo "\n";
        require '../../drivers/houston.php';
        $generic = new Fandom($this->id);
        $generic->getLucky("NAME");
        echo ' - F++</title>';
        echo "\n";
        echo '    <meta http-equiv="X-UA-Compatible" content="IE=edge">';
        echo "\n";
        echo '    <meta name="viewport" content="width=device-width, initial-scale=1">';
        echo "\n";
        echo '    <meta name="description" content="Fandom donde todos los fans de watchdogs comparten notas, publicaciones, videos, links y más."/>';
        echo "\n";
        echo '    <link href="../../static/css/bootstrap.css" rel="stylesheet">';
        echo "\n";
        echo '    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">';
        echo "\n";
        echo '</head>';
        echo "\n";
        echo '<body>';
        echo "\n";
        include "../../static/analyticstracking.php";
        echo "\n";
        include "../../static/navs/nav.php";
        echo "\n";
        echo '<!-- JavaScript SDK -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, ';
        echo "'script', 'facebook-jssdk'));
        </script>
        <!-- JavaScript SDK -->";
        //Inicio de contenido
        echo "\n";
        echo '<div class="container-fluid">
        <!-- Ads y perfil-->
        <section class="row">
            <!-- Modal para enviar error-->                    
            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Informar sobre error</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para error-->
            <!-- Modal para ver foto de inicio-->                    
            <div class="modal fade bs-example-modal-sm-profileimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">';
                            $generic->getLucky("NAME");
echo                        '</h4>
                        </div>
                        <div class="modal-body">
                            <img src="../../';
                            $generic->getLucky("AVATAR");
echo                        '" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para ver foto de inicio-->
            <!-- Modal para reportar imagen -->                   
            <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Imágen de Max</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Usuario Molesto</option>
                                        <option>Apología a la Violencia</option>
                                        <option>Desnudez</option>
                                        <option>Derechos de Autor</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar imagen -->  
            <!-- Modal para sugerir que imagen se quite del fandom-->                    
            <div class="modal fade bs-example-modal-sm-suger-i" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar una Imágen</h4>
                        </div>
                        <div class="modal-body">
                            <p>Cuando eres miembro de un fandom puedes votar para que un contenido se elimine, ¿Seguro deseas votar para que esta Imágen se elimine?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para sugerir que imagen se quite del fandom--> 
            <!-- Modal para reportar a fandom-->                   
            <div class="modal fade bs-example-modal-sm-report-f" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Fandom</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Fandom Repetido</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar a fandom-->

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="';
                        $generic->getLucky("NAME");
echo                    '" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg" ><img src="../../';
                        $generic->getLucky("AVATAR");
echo                    '" class="img-responsive"></a>
                        <h1 class="fandom-h1">';
                        $generic->getLucky("NAME");
echo                        '</h1>
                        <p>';
                        $generic->getLucky("DESCRIPTION");
echo                    '</p>
                        <button type="button" class="btn btn-success">Unirme</button>
                        <!-- Boton dropdown-->
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p><a href="';
                        $generic->getLucky("WEBSITE");
echo                    '" target="_blank" >Sitio Web</a></p>
                        <p><a href="https://www.facebook.com/';
                        $generic->getLucky("FB");
echo                    '" target="_blank" >';
                        $generic->getLucky("FB");
echo                    '</a></p>
                        <p><a href="https://www.twitter.com/';
                        $generic->getLucky("TWTR");
echo                    '" target="_blank" >';
                        $generic->getLucky("TWTR");
echo                    '</a></p>
                        <p>Categoría: <a href="../fandoms/';
                        strtolower($generic->getLucky("CATEGORY"));
echo                    '">';
                        $generic->getLucky("CATEGORY");
echo                    '</a></p>
                    </div>
                </div>
            </div>
            <!-- Botonera para moverde dentro del fandom -->
            <div class="col-xs-12 visible-xs">
                <ul class="list-group">
                <li class="list-group-item"><a href="/';
                $generic->getLucky("URL");
echo '" >';
                $generic->getLucky("NAME");
echo'</a></li>
                    <li class="list-group-item btn-select"><a href="../images">Imágenes ('.$generic->Numbers($this->id,"IMAGES").')</a></li>
                    <li class="list-group-item"><a href="../publications">Publicaciones ('.$generic->Numbers($this->id,"PUBLICATIONS").')</a></li>
                    <li class="list-group-item"><a href="../notes">Notas ('.$generic->Numbers($this->id,"NOTES").')</a></li>
                    <li class="list-group-item"><a href="../members">Miembros ('.$generic->Numbers($this->id,"FANS").')</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <h2>Imágen en ';
                $generic->getLucky("NAME");
echo '          </h2>
                <!-- Imagenes -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Imagen 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p><a href="1.html">20.04.2014</a></p>
                                </div>
                                <a href="../../media.php"><img src="../../profilesimages/5.jpg" class="img-responsive"></a>
                                <div class="panel-footer">
                                    <a href="../../max"><img src="../../profilesimages/1.jpg" class="post-inside-image" /></a>
                                    <a href="../../max">Max</a>
                                    <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-suger-i">Quitar de Fandom</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-img">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="fb-comments" data-href="http://www.fanplusplus.com/panchebrios/images/1.php" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Botonera para moverde dentro del fandom -->
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 hidden-xs">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="/';
                            $generic->getLucky("URL");
echo '" >';
                            $generic->getLucky("NAME");
echo                        '</a></li>
                            <li class="list-group-item btn-select"><a href="../images">Imágenes ('.$generic->Numbers($this->id,"IMAGES").')</a></li>
                            <li class="list-group-item"><a href="../publications">Publicaciones ('.$generic->Numbers($this->id,"PUBLICATIONS").')</a></li>
                            <li class="list-group-item"><a href="../notes">Notas ('.$generic->Numbers($this->id,"NOTES").')</a></li>
                            <li class="list-group-item"><a href="../members">Miembros ('.$generic->Numbers($this->id,"FANS").')</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>&copy; 2014 Fanplusplus &middot; <a href="help.html" >Ayuda</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->';

        echo '<div id="footer">
            <div class="container">
                <p class="footer-text">&copy; 2014 Fanplusplus &middot; <a href="#">Terminos y Condiciones</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
            </div>
        </div>';
        echo "\n";
        echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
        echo "\n";
        echo '<script src="../../static/js/bootstrap.min.js"></script>';        
        echo "\n";
        echo '<script src="../../static/js/file-input.js"></script>';        
        echo "\n";
        echo '<!-- File input script -->';        
        echo "\n";
        echo '<script>';        
        echo "\n";
        echo "$('input[type=file]').bootstrapFileInput();";        
        echo "\n";
        echo "$('.file-inputs').bootstrapFileInput();";     
        echo "\n";
        echo '</script>';    
        echo "\n";
        echo '</body>';
        echo "\n";
        echo '</html>';
    }
}

?>