<?php
//Clase para usar en los perfiles en publications.php
class fandomNotesIndie
{
    private $id;
    private $noteID;

    public function fandomNotesIndie($id,$noteID)//ID del Fandom y ID de la nota para ir por ella
    {
        $this->id = $id;
        $this->noteID = $noteID;
    }

    public function generateNotes()
    {
        session_start();
        echo '<!DOCTYPE html>';
        echo "\n";
        echo '<html lang="es">';
        echo "\n";
        echo '<head>';
        echo "\n";
        echo '    <meta charset="utf-8">';
        echo "\n";
        echo '   <title>Nota Publicada en ';
        require '../../drivers/houston.php';
        $generic = new Fandom($this->id);
        $generic->getLucky("NAME");
        echo ' - F++</title>';
        echo "\n";
        echo '    <meta http-equiv="X-UA-Compatible" content="IE=edge">';
        echo "\n";
        echo '    <meta name="viewport" content="width=device-width, initial-scale=1">';
        echo "\n";
        echo '    <meta name="description" content="Fandom donde todos los fans de watchdogs comparten notas, publicaciones, videos, links y más."/>';
        echo "\n";
        echo '    <link href="../../static/css/bootstrap.css" rel="stylesheet">';
        echo "\n";
        echo '    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">';
        echo "\n";
        echo '    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">';
        echo "\n";
        echo '</head>';
        echo "\n";
        echo '<body>';
        echo "\n";
        include "../../static/analyticstracking.php";
        echo "\n";
        include "../../static/navs/nav.php";
        //Inicio de contenido
        echo "\n";
        echo '<div class="container-fluid">
        <!-- Ads y perfil-->
        <section class="row">
            <!-- Modal para enviar error-->                    
            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Informar sobre error</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para error-->
            <!-- Modal para ver foto de inicio-->                    
            <div class="modal fade bs-example-modal-sm-profileimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">';
                            $generic->getLucky("NAME");
echo                        '</h4>
                        </div>
                        <div class="modal-body">
                            <img src="../../';
                            $generic->getLucky("AVATAR");
echo                        '" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para ver foto de inicio-->
            <!-- Modal para reportar imagen -->                   
            <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Imágen de Max</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Usuario Molesto</option>
                                        <option>Apología a la Violencia</option>
                                        <option>Desnudez</option>
                                        <option>Derechos de Autor</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar imagen -->  
            <!-- Modal para sugerir que imagen se quite del fandom-->                    
            <div class="modal fade bs-example-modal-sm-suger-i" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar una Imágen</h4>
                        </div>
                        <div class="modal-body">
                            <p>Cuando eres miembro de un fandom puedes votar para que un contenido se elimine, ¿Seguro deseas votar para que esta Imágen se elimine?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para sugerir que imagen se quite del fandom--> 
            <!-- Modal para reportar a fandom-->                   
            <div class="modal fade bs-example-modal-sm-report-f" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Fandom</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Fandom Repetido</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar a fandom-->

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="';
                        $generic->getLucky("NAME");
echo                    '" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg" ><img src="../../';
                        $generic->getLucky("AVATAR");
echo                    '" class="img-responsive"></a>
                        <h1 class="fandom-h1">';
                        $generic->getLucky("NAME");
echo                        '</h1>
                        <p>';
                        $generic->getLucky("DESCRIPTION");
echo                    '</p>';
echo                    '<div class="row">';
echo                    '    <div class="col-md-9">';
                        if(isset($_SESSION['username']))
                        {
                            echo '<div id="secBTN">';;
                            $generic->ifFan($this->id, $_SESSION['user_id']);
                            echo '</div>';
                        }else
                        {
                            echo '<a href="../../signin.php" class="btn btn-primary" >Login</a>';
                        }
echo                    '    </div>';
echo                    '    <div class="col-md-3">';
echo '                           <!-- Boton dropdown-->
                                 <div class="btn-group pull-right">
                                     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                         <span class="caret"></span>
                                     </button>
                                     <ul class="dropdown-menu" role="menu">
                                         <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                         <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                     </ul>
                                 </div>';
echo                    '    </div>';
echo                    '</div>';
echo '              </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p><a href="';
                        $generic->getLucky("WEBSITE");
echo                    '" target="_blank" >Sitio Web</a></p>
                        <p><a href="https://www.facebook.com/';
                        $generic->getLucky("FB");
echo                    '" target="_blank" >';
                        $generic->getLucky("FB");
echo                    '</a></p>
                        <p><a href="https://www.twitter.com/';
                        $generic->getLucky("TWTR");
echo                    '" target="_blank" >';
                        $generic->getLucky("TWTR");
echo                    '</a></p>
                        <p>Categoría: <a href="../../fandoms/'.strtolower($generic->getLucky("CATEGORY")).'.php">'.$generic->getLucky("CATEGORY").'</a></p>
                    </div>
                </div>
            </div>
            <!-- Botonera para moverde dentro del fandom -->
            <div class="col-xs-12 visible-xs">
                <div class="list-group">
                    <a href="/'.$generic->getLucky("RETURNURL").'" class="list-group-item">'.$generic->getLucky("RETURNNAME").'</a>
                    <a href="../images" class="list-group-item">Imágenes ('.$generic->Numbers($this->id,"IMAGES").')</a>
                    <a href="../publications" class="list-group-item">Publicaciones ('.$generic->Numbers($this->id,"PUBLICATIONS").')</a>
                    <a href="../notes" class="list-group-item active">Notas ('.$generic->Numbers($this->id,"NOTES").')</a>
                    <a href="../members" class="list-group-item">Miembros ('.$generic->Numbers($this->id,"FANS").')</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <h2>Nota en ';
                $generic->getLucky("NAME");
echo '          </h2>';
//Aqui no se pueden publicar notas
/*echo '          <!-- Formulario users-->
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default no-border">
                        <label><a href="">Fuentes disponibles</a></label>
                        <form role="form">
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputEmail1" maxlength="160" placeholder="160 Caracteres">
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">Selecciona la Tipografía de tu nota</option>
                                    <option value="Arial">Arial</option>
                                    <option value="Arimo">Arimo</option>
                                    <option value="Crafty Girls">Crafty Girls</option>
                                    <option value="Electrolize">Electrolize</option>
                                    <option value="Fanwood Text">Fanwood Text</option>
                                    <option value="Julius Sans One">Julius Sans One</option>
                                    <option value="Josefin Sans">Josefin Sans</option>
                                    <option value="Kite One">Kite One</option>
                                    <option value="Over the Rainbow">Over the Rainbow</option>
                                    <option value="optionoiret One">Poiret One</option>
                                    <option value="Raleway Dots">Raleway Dots</option>
                                    <option value="Shadows Into Light">Shadows Into Light</option>
                                    <option value="The Girl Next Door">The Girl Next Door</option>
                                    <option value="Walter Turncoat">Walter Turncoat</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </form>
                    </div>
                </div>';*/
echo '          <!-- Nota 1-->';
                $generic->getNotesIndie($this->noteID);
echo '      </div>
            <!-- Botonera para moverde dentro del fandom -->
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 hidden-xs">
                        <div class="list-group">
                            <a href="/'.$generic->getLucky("RETURNURL").'" class="list-group-item">'.$generic->getLucky("RETURNNAME").'</a>
                            <a href="../images" class="list-group-item">Imágenes ('.$generic->Numbers($this->id,"IMAGES").')</a>
                            <a href="../publications" class="list-group-item">Publicaciones ('.$generic->Numbers($this->id,"PUBLICATIONS").')</a>
                            <a href="../notes" class="list-group-item active">Notas ('.$generic->Numbers($this->id,"NOTES").')</a>
                            <a href="../members" class="list-group-item">Miembros ('.$generic->Numbers($this->id,"FANS").')</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>&copy; 2014 Fanplusplus &middot; <a href="help.html" >Ayuda</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->';

        echo "\n";
        echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
        echo "\n";
        echo '<script src="../../static/js/bootstrap.min.js"></script>'; 
        echo "\n";
        echo '<script src="../../static/js/fluid.js"></script>';        
        echo "\n";
        echo '</body>';
        echo "\n";
        echo '</html>';
    }

}

?>