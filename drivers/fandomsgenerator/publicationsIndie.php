<?php
//Clase para usar en los perfiles en publications.php
class fandomPublicationsIndie
{
	private $id;
	private $username;
    private $publication_id;

	public function fandomPublicationsIndie($id,$mode,$publication_id)
	{
        $this->id = $id;
        $this->mode = $mode;
        $this->publication_id = $publication_id;
    }

	public function generatePublicationsIndie()
    {
        session_start();
        echo '<!DOCTYPE html>';
        echo "\n";
        echo '<html lang="es">';
        echo "\n";
        echo '<head>';
        echo "\n";
        echo '    <meta charset="utf-8">';
        echo "\n";
        echo '   <title>Publicación en ';
        require '../../drivers/houston.php';
        $generic = new Fandom($this->id);
        $generic->getLucky("NAME");
        echo ' - F++</title>';
        echo "\n";
        echo '    <meta http-equiv="X-UA-Compatible" content="IE=edge">';
        echo "\n";
        echo '    <meta name="viewport" content="width=device-width, initial-scale=1">';
        echo "\n";
        echo '    <meta name="description" content="Fandom donde todos los fans de watchdogs comparten notas, publicaciones, videos, links y más."/>';
        echo "\n";
        echo '    <link href="../../static/css/bootstrap.css" rel="stylesheet">';
        echo "\n";
        echo '    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">';
        echo "\n";
        echo '    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '</head>';
        echo "\n";
        echo '<body>';
        echo "\n";
        include "../../static/analyticstracking.php";
        echo "\n";
        include "../../static/navs/nav.php";
        //Inicio de contenido
        echo "\n";
        echo '<div class="container-fluid">
        <!-- Ads y perfil-->
        <section class="row">
            <!-- Modal para enviar error-->                    
            <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Informar sobre error</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para error-->
            <!-- Modal para ver foto de inicio-->                    
            <div class="modal fade bs-example-modal-sm-profileimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">';
                            $generic->getLucky("NAME");
echo                        '</h4>
                        </div>
                        <div class="modal-body">
                            <img src="../../';
                            $generic->getLucky("AVATAR");
echo                        '" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para ver foto de inicio-->
            <!-- Modal para reportar imagen -->                   
            <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Imágen de Max</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Spam</option>
                                        <option>Usuario Molesto</option>
                                        <option>Apología a la Violencia</option>
                                        <option>Desnudez</option>
                                        <option>Derechos de Autor</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar imagen -->  
            <!-- Modal para sugerir que imagen se quite del fandom-->                    
            <div class="modal fade bs-example-modal-sm-suger-i" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Eliminar una Imágen</h4>
                        </div>
                        <div class="modal-body">
                            <p>Cuando eres miembro de un fandom puedes votar para que un contenido se elimine, ¿Seguro deseas votar para que esta Imágen se elimine?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para sugerir que imagen se quite del fandom--> 
            <!-- Modal para reportar a fandom-->                   
            <div class="modal fade bs-example-modal-sm-report-f" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Reportar Fandom</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Spam</option>
                                <option>Fandom Repetido</option>
                                <option>Contenido no Apto</option>
                            </select>
                        </div>
                    </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal para reportar a fandom-->

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="';
                        $generic->getLucky("NAME");
echo                    '" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg" ><img src="../../';
                        $generic->getLucky("AVATAR");
echo                    '" class="img-responsive"></a>
                        <h1 class="fandom-h1">';
                        $generic->getLucky("NAME");
echo                    '</h1>
                        <p>';
                        $generic->getLucky("DESCRIPTION");
echo                    '</p>';
echo                    '<div class="row">';
echo                    '    <div class="col-md-9">';
                        if(isset($_SESSION['username']))
                        {
                            echo '<div id="secBTN">';;
                            $generic->ifFan($this->id, $_SESSION['user_id']);
                            echo '</div>';
                        }else
                        {
                            echo '<a href="../../signin.php" class="btn btn-primary" >Login</a>';
                        }
echo                    '    </div>';
echo                    '    <div class="col-md-3">';
echo '                           <!-- Boton dropdown-->
                                 <div class="btn-group pull-right">
                                     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                         <span class="caret"></span>
                                     </button>
                                     <ul class="dropdown-menu" role="menu">
                                         <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                         <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                     </ul>
                                 </div>';
echo                    '    </div>';
echo                    '</div>';
echo '              </div>';
echo '          </div>
                <div class="panel panel-default">
                    <div class="panel-body">';
                        //Sitio web
                        if($generic->getLucky("WEBSITE") != "")
                        {
                            echo '<p><a href="'.$generic->getLucky("WEBSITE").'" target="_blank" >Sitio Web</a></p>';
                        }
echo '                  <p><a href="https://www.facebook.com/';
                        $generic->getLucky("FB");
echo                    '" target="_blank" >';
                        $generic->getLucky("FB");
echo                    '</a></p>
                        <p><a href="https://www.twitter.com/';
                        $generic->getLucky("TWTR");
echo                    '" target="_blank" >';
                        $generic->getLucky("TWTR");
echo                    '</a></p>
                        <p>Categoría: <a href="../../fandoms/'.strtolower($generic->getLucky("CATEGORY")).'.php">'.$generic->getLucky("CATEGORY").'</a></p>
                    </div>
                </div>
            </div>
            <!-- Botonera para moverde dentro del fandom -->
            <div class="col-xs-12 visible-xs">
                <div class="list-group">
                    <a href="/'.$generic->getLucky("RETURNURL").'" class="list-group-item">'.$generic->getLucky("RETURNNAME").'</a>
                    <a href="../images" class="list-group-item">Imágenes ('.$generic->Numbers($this->id,"IMAGES").')</a>
                    <a href="../publications" class="list-group-item active">Publicaciones ('.$generic->Numbers($this->id,"PUBLICATIONS").')</a>
                    <a href="../notes" class="list-group-item">Notas ('.$generic->Numbers($this->id,"NOTES").')</a>
                    <a href="../members" class="list-group-item">Miembros ('.$generic->Numbers($this->id,"FANS").')</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <h2>Publicación en ';
                $generic->getLucky("NAME");
                echo '</h2>';
                /*echo '<!-- Form para publicar en el fandom si eres miembro -->
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default no-border">
                        <form role="form">
                            <p class="help-block">Puedes hacer un publicación</p>
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputEmail1" maxlength="160" placeholder="160 Caracteres">
                            </div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </form>
                    </div>
                </div>';*/
                //Empieza la publication
                include '../../link.php';
                $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
                if($this->mode == "fandom")
                {
                    $result = mysqli_query($con,"SELECT * FROM fan_publications WHERE ID='".$this->publication_id."'");
                    $row = mysqli_fetch_array($result);
                    $result2 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['DE_ID']."'");
                    $row2 = mysqli_fetch_array($result2);
                }elseif($this->mode == "profile")
                {
                    $result = mysqli_query($con,"SELECT * FROM profile_publications WHERE ID='".$this->id."'");
                    $row = mysqli_fetch_array($result);
                    $result2 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['DE_ID']."'");
                    $row2 = mysqli_fetch_array($result2);
                }
echo '          <!-- Publicacion 1 -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default post-panel">
                        <div class="panel-heading">
                            <a href="../../'.$row2['USERNAME'].'"><img src="../../'.$row2['AVATAR'].'" class="post-inside-image" /></a>
                            <a href="../../'.$row2['USERNAME'].'">Max</a>
                            <p class="pull-right"><a href="'.$this->publication_id.'.php">'.$row['FECHA'].'</a></p>
                        </div>
                        <div class="panel-body">
                            <p>'.$row['CONTENT'].'</p>
                        </div>
                        <div class="panel-footer">';
                            if (isset($_SESSION['username'])) 
                            {
                                $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                                $totalS = mysqli_num_rows($resultS);                                           

                              if($totalS > 0)
                                {
                                    echo '<button type="button" class="btn btn-danger" onclick="deletePublication('.$row['ID'].','.$_SESSION['user_id'].',1,this)" >Unsandbox</button>';
                                }else
                                {
                                    echo '<button type="button" class="btn btn-primary" onclick="addPublication('.$row['ID'].', '.$_SESSION['user_id'].', '.$row['DE_ID'].', '.$row['PARA_ID'].', 1, this)" >Sandbox</button>';
                                }
                            }else
                            {
                                echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                            }
                echo '      <!-- Boton dropdown-->
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-suger-p">Quitar de Fandom</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Botonera para moverde dentro del fandom -->
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 hidden-xs">
                        <div class="list-group">
                            <a href="/'.$generic->getLucky("RETURNURL").'" class="list-group-item">'.$generic->getLucky("RETURNNAME").'</a>
                            <a href="../images" class="list-group-item">Imágenes ('.$generic->Numbers($this->id,"IMAGES").')</a>
                            <a href="../publications" class="list-group-item active">Publicaciones ('.$generic->Numbers($this->id,"PUBLICATIONS").')</a>
                            <a href="../notes" class="list-group-item">Notas ('.$generic->Numbers($this->id,"NOTES").')</a>
                            <a href="../members" class="list-group-item">Miembros ('.$generic->Numbers($this->id,"FANS").')</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>&copy; 2014 Fanplusplus &middot; <a href="help.html" >Ayuda</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->';

        echo "\n";
        echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
        echo "\n";
        echo '<script src="../../static/js/bootstrap.min.js"></script>';        
        echo "\n";
        echo '<script src="../../static/js/file-input.js"></script>'; 
        echo "\n";
        echo '<script src="../../static/js/fluid.js"></script>';       
        echo "\n";
        echo '<!-- File input script -->';        
        echo "\n";
        echo '<script>';        
        echo "\n";
        echo "$('input[type=file]').bootstrapFileInput();";        
        echo "\n";
        echo "$('.file-inputs').bootstrapFileInput();";     
        echo "\n";
        echo '</script>';    
        echo "\n";
        echo '</body>';
        echo "\n";
        echo '</html>';
    }

}//Fin de clase
?>