<?php
//Clase para usar en la columna
class Cross
{
	public function commentForm($id, $pic, $mode)//Lleva y coloca valores al form de comentarios de fotos de media
	{
		if ($mode == "id") 
		{
			echo '<div id="commentsbox">Comentarios:
                 <form action="mediacomment.php" method="post" id="addCommentForm">
                     <input type="hidden" value="'.$pic.'" name="pic" />
                     <input type="hidden" value="'.$id.'" name="id" />
                     <textarea name="body" id="body" style="resize: none;" cols="50" rows="3" maxlength="160"></textarea><br>
                     <input type="submit" value="Comentar" id="submit" />
                 </form>
            </div>';
		}elseif($mode == "fandom")
		{
			echo '<div id="commentsbox">Comentarios:
                 <form action="mediacomment.php" method="post" id="addCommentForm">
                     <input type="hidden" value="'.$pic.'" name="pic" />
                     <input type="hidden" value="'.$id.'" name="fandom" />
                     <textarea name="body" id="body" style="resize: none;" cols="50" rows="3" maxlength="160"></textarea><br>
                     <input type="submit" value="Comentar" id="submit" />
                 </form>
            </div>';
		}else
		{
			echo "Sin modo para mostrar.";
		}
	}

	public function getComments($mode,$pic)//Recibe el parametro del ID del Fandom y el limite a traer.
	{
		if($mode == "profile") 
		{
			include 'link.php';
		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
            //Anti-SQL injection
            $pic =mysqli_real_escape_string($con, $pic);

			$result = mysqli_query($con,"SELECT COMMENT,FECHA,WHO_ID FROM media_profilecomments WHERE PIC='".$pic."' ORDER BY FECHA DESC");

			if (mysqli_num_rows($result) > 0) 
    		{
    			echo '<div id="commentsline"><table>
        		<tr>
        		<th>De</th>
        		<th>Comentario</th>
        		<th>Fecha</th>
        		</tr>';
        		while($row = mysqli_fetch_array($result))
        		{
        			$result2 = mysqli_query($con,"SELECT USERNAME,AVATAR FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                    $row2 = mysqli_fetch_array($result2);

                    echo "<tr>";
                    echo '<td><a href="'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'" width="50" height="50"></a></td>';
        			echo "<td>".$row['COMMENT']."</td>";
        			echo "<td>".$row['FECHA']."</td>";
        			echo "</tr>";
        		}
        		echo "</table></div>";
        	}

            mysqli_close($con);
		}elseif($mode == "fandom")
		{
			include 'link.php';
		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
            $pic = mysqli_real_escape_string($con, $pic);
			$result = mysqli_query($con,"SELECT COMMENT,FECHA,WHO_ID FROM media_fandomcomments WHERE PIC='".$pic."' ORDER BY FECHA DESC");

			if (mysqli_num_rows($result) > 0) 
    		{

    			echo '<div id="commentsline"><table>
        		<tr>
        		<th>De</th>
        		<th>Comentario</th>
        		<th>Fecha</th>
        		</tr>';
        		while($row = mysqli_fetch_array($result))
        		{
                    $result2 = mysqli_query($con,"SELECT USERNAME,AVATAR FROM profiles WHERE ID='".$row['WHO_ID']."' ");
                    $row2 = mysqli_fetch_array($result2);

        			echo "<tr>";
                    echo '<td><a href="'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'" width="50" height="50"></a></td>';
        			echo "<td>".$row['COMMENT']."</td>";
        			echo "<td>".$row['FECHA']."</td>";
        			echo "</tr>";
        		}
        		echo "</table></div>";
        	}
            mysqli_close($con);
		}else
		{
			echo "Sin modo para mostrar.";
		}
	}

    public function ifIssetOnSandbox($mode,$pic,$user_id,$fan_pro_id,$mode_js)
    {
        if($mode == "profile") 
        {
            include 'link.php';
            $con = mysqli_connect($host,$user,$pw,$db);
            $result = mysqli_query($con,"SELECT ID FROM sandbox_images WHERE IMAGE_ID='".$pic."' AND WHO_ADD='".$user_id."' AND MODE='".$mode."' ");
            $total = mysqli_num_rows($result);       

            if($total > 0)
            {
                echo '<button type="button" onclick="deleteSandbox('.$pic.','.$user_id.','.$mode_js.',this)" class="btn btn-danger" >Unsandbox</button>';
                //echo '<button id="btnDeleteSandbox" onclick="deleteSandbox('.$pic.','.$user_id.','.$mode_js.')">Delete in Sandbox</button>';
            }else
            {
                echo '<button type="button" onclick="addSandboxP('.$pic.','.$user_id.','.$_GET['id'].','.$_GET['id'].','.$mode_js.',this)" class="btn btn-primary" >Sandbox</button>';
                //echo '<button id="btnAddSandbox" onclick="addSandboxP('.$pic.','.$user_id.','.$_GET['id'].','.$_GET['id'].','.$mode_js.')">Add To Sandbox</button>';
            }

            mysqli_close($con);
        }elseif($mode == "fandom")
        {
            include 'link.php';
            $con = mysqli_connect($host,$user,$pw,$db);
            $result = mysqli_query($con,"SELECT ID FROM sandbox_images WHERE IMAGE_ID='$pic' AND WHO_ADD='$user_id' AND MODE='$mode' ");
            $total = mysqli_num_rows($result);        
            $result2 = mysqli_query($con,"SELECT USER_ID FROM fandom_images WHERE ID='".$pic."'");
            $row = mysqli_fetch_array($result2);

            if($total > 0)
            {
                echo '<button type="button" onclick="deleteSandbox('.$pic.','.$user_id.','.$mode_js.',this)" class="btn btn-danger" id="btnDeleteSandbox" >Unsandbox</button>';
            }else
            {
                echo '<button type="button" onclick="addSandboxF('.$pic.','.$user_id.','.$fan_pro_id.','.$_GET['fandom'].','.$mode_js.',this)" class="btn btn-primary" id="btnAddSandbox" >Sandbox</button>';
            }
            
            mysqli_close($con);
        }else
        {
            echo "Aun sin valores";
        }

    }//Fin de ifIssetOnSandbox


}//Fin de clase
?>