<?php

class Profile
{
	private $id;
	private $username;

	public function Profile($id,$username)
	{
		$this->id = $id;
		$this->username = $username;
	}

	public function generateProfile()
	{
	    session_start();
	    echo '<!DOCTYPE html>';
        echo "\n";
        echo '<html lang="es">';
        echo "\n";
        echo '<head>';
        echo "\n";
        echo '    <meta charset="utf-8">';
        echo "\n";
        echo '	  <title>';
        require "../drivers/columbia.php";
        $generic = new Usuario($this->id);
        $generic->getLucky("FIRSTNAME");
        echo ' en Fan Plus Plus</title>';
        echo "\n";
        echo '    <meta http-equiv="X-UA-Compatible" content="IE=edge">';
        echo "\n";
        echo '    <meta name="viewport" content="width=device-width, initial-scale=1">';
        echo "\n";
        echo '    <link href="../static/css/bootstrap.css" rel="stylesheet">';
        echo "\n";
        echo '    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />';
        echo "\n";
        echo '    <meta name="description" content="';
        $generic->getLucky("BIOGRAPHY");
        echo '" />';
        echo "\n";
        echo '    <link rel="stylesheet" type="text/css" href="../static/css/estilos.css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="../static/css/sticky-footer-navbar.css" rel="stylesheet">';
        echo "\n";
        //Fuentes personalizadas
        echo '    <link href="http://fonts.googleapis.com/css?family=Arimo" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Crafty+Girls" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Devonshire" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Electrolize" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Fanwood+Text" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Kite+One" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Over+the+Rainbow" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Raleway+Dots" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=The+Girl+Next+Door" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Walter+Turncoat" rel="stylesheet" type="text/css">';
        //Fuentes personalizadas
        echo "\n";
        echo '</head>';
        echo "\n";
        echo '<body>';
        echo "\n";
        include "../static/analyticstracking.php";
        echo "\n";
        if(isset($_SESSION['username']))
        {
            echo '<!-- Inicio del Nav-->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="http://www.fanplusplus.com">F++</a>
                </div>    
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="http://www.fanplusplus.com/fandoms">Fandoms</a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li><a href="http://www.fanplusplus.com/notifications"><span class="glyphicon glyphicon-bell"></span> Notificaciones</a></li>
                    </ul>
                    <form action="http://www.fanplusplus.com/search.php" method="get" class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input name="q" type="text" class="form-control" placeholder="Encuentra cosas">
                        </div>
                        <button type="submit" class="btn btn-default">Buscar</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="http://www.fanplusplus.com/sandbox">Sandbox <span class="badge">3</span></a>
                        </li>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">';
                            echo $_SESSION['fn'];
                            echo '<b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="http://www.fanplusplus.com/';
                                echo $_SESSION['username'];
                                echo '">Perfil</a></li>
                                <li><a href="http://www.fanplusplus.com/invite.php">Invitar Amigos</a></li>
                                <li class="divider"></li>
                                <li><a href="http://www.fanplusplus.com/settings">Configuración</a></li>
                            </ul>
                        </li>
                        <li><a href="http://www.fanplusplus.com/logout.php">Cerrar Sesión</a></li>
                    </ul>
                </div>
            </nav>
            <!-- Fin del Nav-->';
        }else
        {
            echo '<!-- Inicio del Nav-->
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="http://www.fanplusplus.com">F++</a>
                    </div>    
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="http://www.fanplusplus.com/fandoms">Fandoms</a></li>
                        </ul>
                        <form action="http://www.fanplusplus.com/search.php" method="get" class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input name="q" type="text" class="form-control" placeholder="Encuentra cosas">
                            </div>
                            <button type="submit" class="btn btn-default">Buscar</button>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="http://www.fanplusplus.com/signin.php">Iniciar Sesión</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Fin del Nav-->';
        }
        echo "\n";
        echo '<!-- Inicio de contenido -->';
        echo "\n";
        echo '    <div class="container-fluid">';
        echo "\n";
        echo '        <section class="row">';
        echo "\n";
        echo '            <div class="col-sm-1 col-md-2 col-lg-2"></div><!-- Relleno -->';
        echo "\n";
        //Seccion del perfil
        echo '            <div class="col-sm-10 col-md-8 col-lg-8">';
        echo "\n";

        //Seccion 1, para xs
        echo '                <div class="panel panel-default visible-xs">';
        echo "\n";
        echo '                    <div class="panel-body">';
        echo "\n";
        echo '                        <a href="" data-toggle="modal" data-target=".bs-example-modal-sm-profileimg"><img src="../profilesimages/7.jpg" class="img-responsive"></a>';
        echo "\n";
        echo '                        <h1 class="fandom-h1">Max</h1>';
        echo "\n";
        if (isset($_SESSION['username']) && ($generic->getLucky("USERNAMERETURN") != $this->username) ) 
        {
            $generic->ifFollow($this->id,$_SESSION["user_id"]);
        }else 
        {
            echo '<a href="../settings" class="btn btn-primary">Editar</a>';
        }
        echo "\n";
        echo '                        <div class="btn-group pull-right">';
        echo "\n";
        echo '                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">';
        echo "\n";
        echo '                                <span class="caret"></span>';
        echo "\n";
        echo '                            </button>';
        echo "\n";
        echo '                            <ul class="dropdown-menu" role="menu">';
        echo "\n";
        echo '                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>';
        echo "\n";
        echo '                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>';
        echo "\n";
        echo '                            </ul>';
        echo "\n";
        echo '                        </div>';
        echo "\n";
        echo '                    </div>';
        echo "\n";
        echo '                </div>';
        //Info
        echo "\n";
        echo '                <div class="panel panel-default visible-xs">';
        echo "\n";
        echo '                    <div class="panel-body">';
        echo "\n";
        echo '                        <div class="row">';
        echo "\n";
        echo '                            <div class="col-xs-12">';
        echo "\n";
        //Pais
        echo '                                <p>';
        $generic->getLucky("COUNTRY");
        echo '</p>';
        echo "\n";
        //Biografia
        echo '                                <p>';
        $generic->getLucky("BIOGRAPHY");
        echo '</p>';
        echo "\n";
        echo '                                <p>';
        $generic->getLucky("URL");
        echo '</p>';
        echo "\n";
        echo '                            </div>';
        echo "\n";
        echo '                        </div>';
        echo "\n";
        echo '                    </div>';
        echo "\n";
        echo '                </div>';
        echo "\n";
        //Balance
        echo '                <div class="panel panel-default visible-xs">';
        echo "\n";
        echo '                    <div class="panel-body">';
        echo "\n";
        echo '                        <div class="row">';
        echo "\n";
        echo '                            <div class="col-xs-12">';
        echo "\n";
        echo '                                <p>Seguidores: 8</p>';
        echo "\n";
        echo '                            </div>';
        echo "\n";
        echo '                        </div>';
        echo "\n";
        echo '                    </div>';
        echo "\n";
        echo '                </div>';
        //Carrete de 3 fotos
        echo "\n";
        echo '                <div class="panel panel-default visible-xs">';
        echo "\n";
        echo '                    <div class="panel-body">';
        echo "\n";
        echo '                        <div class="row">';
        echo "\n";
        echo '                            <div class="col-xs-4">';
        echo "\n";
        echo '                                <a href="../media.php"><img src="../profilesimages/7.jpg" class="img-responsive"></a>';
        echo "\n";
        echo '                            </div>';
        echo '                            <div class="col-xs-4">';
        echo "\n";
        echo '                                <a href="../media.php"><img src="../profilesimages/2.jpg" class="img-responsive"></a>';
        echo "\n";
        echo '                            </div>';
        echo '                            <div class="col-xs-4">';
        echo "\n";
        echo '                                <a href="../media.php"><img src="../profilesimages/9.jpg" class="img-responsive"></a>';
        echo "\n";
        echo '                            </div>';
        echo "\n";
        echo '                        </div>';
        echo "\n";
        echo '                    </div>';
        echo "\n";
        echo '                </div>';
        //Seccion 1, para xs

        echo "\n";
        echo '<!-- Modal para eliminar fotos-->                    
                <div class="modal fade bs-example-modal-sm-sand-dfoto" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Eliminar Imágen</h4>
                            </div>
                            <div class="modal-body">
                                <p>¿Estas seguro de eliminar esta Imágen?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-danger">Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para eliminar fotos-->
                <!-- Modal para enviar mensaje-->                    
                <div class="modal fade bs-example-modal-sm-msn" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Enviar Mensaje a Maximiliano</h4>
                            </div>
                            <form role="form" action="../drivers/formdirect/sendmessage.php" method="post" >
                            <div class="modal-body">
                                    <div class="form-group">
                                        <textarea name="content" class="form-control" rows="3"></textarea>
                                        <input type="hidden" value="'.$this->id.'" name="for_id" />
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Modal para mensaje--> 
                <!-- Modal para reportar a usuario-->                   
                <div class="modal fade bs-example-modal-sm-report-u" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Usuario</h4>
                            </div>
                            <div class="modal-body">
                                
                                    <div class="form-group" id="rpArea">
                                        <select class="form-control">
                                            <option value="spam">Spam</option>
                                            <option value="annoying">Usuario Molesto</option>
                                            <option value="unsuitable">Contenido no Apto</option>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary" onclick="reportUser('.$this->id.','.$_SESSION['user_id'].',this)" >Enviar</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para reportar a usuario-->
                <!-- Modal para reportar a fandom-->                   
                <div class="modal fade bs-example-modal-sm-report-f" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Fandom</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Fandom Repetido</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para reportar a fandom-->
                <!-- Modal de reporte de publicacion-->                   
                <div class="modal fade bs-example-modal-sm-report-p" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar una Publicación</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Contenido Ofensivo</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal de reporte de publicacion-->
                <!-- Modal de reporte de nota-->                   
                <div class="modal fade bs-example-modal-sm-report-n" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Nota</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Spam</option>
                                    <option>Contenido Ofensivo</option>
                                    <option>Contenido no Apto</option>
                                </select>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal de reporte de nota-->
                <!-- Modal para enviar error-->                    
                <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Informar sobre error</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para error--> 
                <!-- Modal para reportar imagen -->                   
                <div class="modal fade bs-example-modal-sm-report-img" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reportar Imágen de Max</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Spam</option>
                                            <option>Usuario Molesto</option>
                                            <option>Apología a la Violencia</option>
                                            <option>Desnudez</option>
                                            <option>Derechos de Autor</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal para reportar imagen -->';
        //Fin de modals
        //Seccion 1, para lg
        echo '                <!-- Seccion 1 -->';
        echo "\n";
        echo '                <div class="panel panel-default hidden-xs">';
        echo "\n";
        echo '                    <div class="panel-body">';
        echo "\n";
        echo '                        <div class="row">';
        echo "\n";
        echo '                            <div class="col-sm-10 col-md-10 col-lg-10">';
        echo "\n";
        echo '                                <!-- Seccion de nombre, info, seguidores -->';
        echo "\n";
        echo '                                <div class="row">';
        echo "\n";
        echo '                                    <!-- Nombre -->';
        echo "\n";
        echo '                                    <div class="col-sm-12 col-md-12 col-lg-12">';
        echo "\n";
        echo '                                        <div class="panel panel-default no-border">';
        echo "\n";
        echo '                                            <div class="panel-body">';
        echo "\n";
        echo '                                                <div clas="row">';
        echo "\n";
        echo '                                                    <div class="col-sm-8 col-md-8 col-lg-8">';
        echo "\n";
        echo '                                                        <p class="profile-name">';
        $generic->getLucky("FIRSTNAME");
        echo '</p>';
        echo "\n";
        echo '                                                    </div>';
        echo "\n";
        echo '                                                    <div class="col-sm-4 col-md-4 col-lg-4" >';
        echo "\n";
        if (isset($_SESSION['username']) && ($_SESSION["username"] != $generic->getLucky("USERNAMERETURN"))) 
        {
            $generic->ifFollow($this->id,$_SESSION["user_id"]);//Boton de Seguir
            echo "\n";
            echo '                                                        <div class="btn-group">';
            echo "\n";
            echo '                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">';
            echo "\n";
            echo '                                                            <span class="caret"></span>';
            echo "\n";
            echo '                                                        </button>';
            echo "\n";
            echo '                                                        <ul class="dropdown-menu" role="menu">';
            echo "\n";
            echo '                                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-msn">Mensaje</a></li>';
            echo "\n";
            echo '                                                            <li class="divider"></li>';
            echo "\n";
            echo '                                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>';
            echo "\n";
            echo '                                                        </ul>';
            echo "\n";
            echo '                                                    </div>';
        }elseif(isset($_SESSION['username']) && ($_SESSION["username"] == $generic->getLucky("USERNAMERETURN"))) 
        {
            echo '<a href="../settings" class="btn btn-default">Editar Perfil</a>';
        }
        echo "\n";
        echo '                                                </div>';
        echo "\n";
        echo '                                            </div>';
        echo "\n";
        echo '                                        </div>';
        echo "\n";
        echo '                                    </div>';
        echo "\n";
        echo '                                </div>';
        echo "\n";
        echo '                                <!-- Foto, info, seguidores y reciente -->';
        echo "\n";
        echo '                                <div class="col-sm-12 col-md-12 col-lg-12">';
        echo "\n";
        echo '                                    <!-- Foto de perfil-->';
        echo "\n";
        echo '                                    <div class="col-sm-4 col-md-4 col-lg-4">';
        echo "\n";
        echo '                                        <div class="panel panel-default">';
        echo "\n";
        echo '                                            <div class="panel-body">';
        echo "\n";
        echo '                                                <img src="';
        $generic->getLucky("AVATAR");
        echo '" class="profile-img">';
        echo "\n";
        echo '                                            </div>';
        echo "\n";
        echo '                                        </div>';
        echo "\n";
        echo '                                    </div>';
        echo "\n";
        echo '                                    <!-- Info -->';
        ///Info
        echo "\n";
        echo '                                    <div class="col-sm-8 col-md-8 col-lg-8">';
        echo "\n";
        echo '                                        <div class="panel panel-default">';
        echo "\n";
        echo '                                            <div class="panel-body">';
        echo "\n";
        echo '                                                <p>';
        $generic->getLucky("COUNTRY");
        echo '</p>';
        echo "\n";
        echo '                                                <p>';
        $generic->getLucky("BIOGRAPHY");
        echo '</p>';
        echo "\n";
        echo '                                                <p>';
        $generic->getLucky("URL");
        echo '</p>';
        echo "\n";
        echo '                                            </div>';
        echo "\n";
        echo '                                        </div>';
        echo "\n";
        echo '                                    </div>';
        echo "\n";
        echo '                                </div>';
        echo "\n";
        echo '                            </div>';
        echo "\n";
        echo '                        </div>';
        echo "\n";
        //Carrete de 3 fotos
        echo '                        <!-- Seccion de 3 fotos pegadas del perfil -->';
        echo "\n";
        echo '                        <div class="col-sm-2 col-md-2 col-lg-2">';
        echo "\n";
        echo '                            <div class="row">';
        $generic->getAllPhotos($this->id,"fan","LIMIT 3");
        echo "\n";
        echo '                            </div>';
        echo "\n";
        echo '                        </div>';
        echo "\n";
        echo '                    </div>';
        echo "\n";
        echo '                </div>';
        echo "\n";
        echo '            </div>';
        //Carrete de 3 fotos
        echo "\n";
        echo '  <!-- Seccion 2, Fotos-->
                <div class="panel panel-default" id="image-sec" >
                    <div class="panel-body">
                        <div class="row">
                            <!-- Formulario users-->
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default no-border">
                                    <h3>Fotos (<a href="images">Ver todas</a>)</h3>';
                                    if(isset($_SESSION["user_id"]) && ($_SESSION["user_id"] == $this->id))
                                    {
                                        echo '<form role="form" method="post" action="../drivers/formdirect/addphoto.php" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label>Descripción:</label>
                                                    <input name="description" type="text" class="form-control" id="exampleInputEmail1" maxlength="160" placeholder="160 Caracteres">
                                                </div>
                                                <div class="form-group">
                                                    <input name="image" type="file" title="Seleccionar Imágen">
                                                </div>
                                                <button type="submit" class="btn btn-primary">Subir</button>
                                            </form>';
                                    }                            
echo '                          </div>
                            </div>
                            <!-- Carrete de fotos-->
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default no-border">
                                    <div class="row">
                                        <!-- Inicia carrete de 4 fotos-->
                                        <!-- Imagen 1 -->';
                                        $generic->getAllPhotos($this->id,"fanImageSection", "LIMIT 4");
echo '                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Seccion 3, de notas y publicaciones-->
                <div class="panel panel-default" id="pn-sec">
                    <div class="panel-body">
                        <div class="row">
                            <!-- Publicaciones-->
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <!-- Formulario users-->
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default no-border">
                                        <h4>Publicaciones (<a href="publications">Ver todas</a>)</h4>';
                                        if(isset($_SESSION["user_id"]))//Solo quien este logueado podra mandar publicación
                                        {
                                            echo '<form role="form" method="post" action="../../drivers/formdirect/savepublication.php">
                                                    <div class="form-group">
                                                        <input type="hidden" name="profile" value="'.$this->username.'" />
                                                        <input type="hidden" name="para_id" value="'.$this->id.'" />
                                                        <input name="message" type="text" class="form-control" id="exampleInputEmail1" maxlength="160" placeholder="160 Caracteres">
                                                    </div>
                                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                                </form>';
                                        }
echo '                              </div>
                                </div>
                                <!-- Publicaciones -->
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default no-border">
                                        <div class="row">';
                                        $generic->getPublicationsOf($this->id,3,"profile");
echo '                                  </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Notas-->
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <!-- Formulario users-->
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default no-border">
                                        <h4>Notas (<a href="notes">Ver todas</a>)</h4>
                                        <label><a href="">Fuentes disponibles</a></label>';
                                        if(isset($_SESSION["user_id"]))
                                        {
                                            echo '<form role="form" method="post" action="../../drivers/formdirect/savenote.php">';
                                            echo '  <input type="hidden" name="profile2" value="'.$this->username.'" />';
                                            echo '  <input type="hidden" name="to_id" value="'.$this->id.'" />';
                                            echo '  <input type="hidden" name="mode" value="profile" />';
                                            echo '  <div class="form-group">
                                                        <input name="note" type="text" class="form-control" id="exampleInputEmail1" maxlength="160" placeholder="160 Caracteres">
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="font" class="form-control">
                                                            <option value="">Selecciona la Tipografía de tu nota</option>
                                                            <option value="font1">Arial</option>
                                                            <option value="font2">Arimo</option>
                                                            <option value="font3">Crafty Girls</option>
                                                            <option value="font4">Electrolize</option>
                                                            <option value="font5">Fanwood Text</option>
                                                            <option value="font6">Julius Sans One</option>
                                                            <option value="font7">Josefin Sans</option>
                                                            <option value="font8">Kite One</option>
                                                            <option value="font9">Over the Rainbow</option>
                                                            <option value="font10">Poiret One</option>
                                                            <option value="font11">Raleway Dots</option>
                                                            <option value="font12">Shadows Into Light</option>
                                                            <option value="font13">The Girl Next Door</option>
                                                            <option value="font14">Walter Turncoat</option>
                                                        </select>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                                </form>';
                                        }                               
echo '                              </div>
                                </div>
                                <!-- Carrete de fotos-->
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default no-border">
                                        <div class="row">
                                            <!-- Nota 1 -->';
                                            $generic->getNotes(3,"profile"); 
echo '                                  </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Seccion 4, de seguidores y miembros-->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <!-- Seguidores -->
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default no-border">
                                        <h4>Seguidores (<a href="followers">Ver todos</a>)</h4>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default no-border">
                                        <div class="row">
                                            <!-- Seguidor 1 -->';
                                            $generic->getFollowersOf($this->id,3,"index");
echo '                                 </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fandoms -->
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default no-border">
                                        <h4>Fandoms (<a href="fandoms">Ver todos</a>)</h4>
                                    </div>
                                </div>
                                <!-- Fandoms empiezan-->
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default no-border">
                                        <div class="row">
                                            <!-- Fandom 1 -->';
                                            $generic->getFandomsOf($this->id,'index');
echo '                                  </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-1 col-md-2 col-lg-2"></div><!-- Relleno -->
        </section>
    </div>
    <!-- Fin de contenido -->';

        echo '<div id="footer">
            <div class="container">
                <p class="footer-text">&copy; 2014 Fanplusplus &middot; <a href="#">Terminos y Condiciones</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
            </div>
        </div>';
        echo "\n";
        echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
        echo "\n";
        echo '<script src="../static/js/bootstrap.min.js"></script>';        
        echo "\n";
        echo '<script src="../static/js/fluid.js"></script>';        
        echo "\n";
        echo '<script src="../static/js/file-input.js"></script>';        
        echo "\n";
        echo '<!-- File input script -->';        
        echo "\n";
        echo '<script>';        
        echo "\n";
        echo "$('input[type=file]').bootstrapFileInput();";        
        echo "\n";
        echo "$('.file-inputs').bootstrapFileInput();";     
        echo "\n";
        echo '</script>';    
        echo "\n";
        echo '</body>';
        echo "\n";
        echo '</html>';
	}
}

?>