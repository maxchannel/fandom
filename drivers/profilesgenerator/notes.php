<?php

class ProfileNotes
{
	private $id;
	private $username;

	public function ProfileNotes($id,$username)
	{
		$this->id = $id;
		$this->username = $username;
	}

	public function generateNotes()
	{
        session_start();
        echo '<!DOCTYPE html>';
        echo "\n";
        echo '<html lang="es">';
        echo "\n";
        echo '<head>';
        echo "\n";
        echo '    <meta charset="utf-8">';
        echo '    <title>';
        require "../../drivers/columbia.php";
        $generic = new Usuario($this->id);
        $generic->getLucky("FIRSTNAME");
        echo ' en Fan Plus Plus</title>';
        echo "\n";
        echo '    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />';
        echo "\n";
        echo '    <meta http-equiv="X-UA-Compatible" content="IE=edge">';
        echo "\n";
        echo '    <meta name="viewport" content="width=device-width, initial-scale=1">';
        echo "\n";
        echo '    <link href="../../static/css/bootstrap.css" rel="stylesheet">';
        echo "\n";
        echo '    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">';
        echo "\n";
        echo '    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">';
        echo "\n";
        echo '    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">';
        echo "\n";
        echo '</head>';
        echo "\n";
        echo '<body>';
        echo "\n";
        include "../../static/analyticstracking.php";
        echo "\n";
        if(isset($_SESSION['username']))
        {
            echo '<!-- Inicio del Nav-->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="http://www.fanplusplus.com">F++</a>
                </div>    
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="http://www.fanplusplus.com/fandoms">Fandoms</a></li>
                    </ul>
                    <form action="http://www.fanplusplus.com/search.php" method="get" class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input name="q" type="text" class="form-control" placeholder="Encuentra cosas">
                        </div>
                        <button type="submit" class="btn btn-default">Buscar</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="http://www.fanplusplus.com/sandbox">Sandbox <span class="badge">3</span></a>
                        </li>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">';
                            echo $_SESSION['fn'];
                            echo '<b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="http://www.fanplusplus.com/';
                                echo $_SESSION['username'];
                                echo '">Perfil</a></li>
                                <li><a href="http://www.fanplusplus.com/invite.php">Invitar Amigos</a></li>
                                <li class="divider"></li>
                                <li><a href="http://www.fanplusplus.com/settings">Configuración</a></li>
                            </ul>
                        </li>
                        <li><a href="http://www.fanplusplus.com/logout.php">Cerrar Sesión</a></li>
                    </ul>
                </div>
            </nav>
            <!-- Fin del Nav-->';
        }else
        {
            echo '<!-- Inicio del Nav-->
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="http://www.fanplusplus.com">F++</a>
                    </div>    
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="http://www.fanplusplus.com/fandoms">Fandoms</a></li>
                        </ul>
                        <form action="http://www.fanplusplus.com/search.php" method="get" class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input name="q" type="text" class="form-control" placeholder="Encuentra cosas">
                            </div>
                            <button type="submit" class="btn btn-default">Buscar</button>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="http://www.fanplusplus.com/signin.php">Iniciar Sesión</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Fin del Nav-->';
        }
        echo "\n";
        echo '<!-- Inicio de contenido -->';
        echo "\n";
        echo '<div class="container-fluid">
        <!-- Ads y perfil-->
        <section class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default fandom-panel">
                                    <div class="panel-body">
                                        <a href="../../../../';
                                        $generic->getLucky("USERNAME");
echo '                                        "><img src="../../../../';
                                        $generic->getLucky("AVATAR");
echo '                                        " class="profile-inside-image" /></a>
                                        <p class="fandom-title"><a href="../../../../';
                                        $generic->getLucky("USERNAME");
echo '                                        ">';
                                        $generic->getLucky("FIRSTNAME");
echo                                    '</a></p>
                                    </div>
                                    <div class="panel-footer">';
                                        if (isset($_SESSION['username']) && ($this->username != $_SESSION['username']) ) 
                                        {
                                            $generic->ifFollow($this->id, $_SESSION['user_id']);
                                        }elseif($this->username == $_SESSION['username'])
                                        {
                                            echo '<a href="../../'.$_SESSION['username'].'" class="btn btn-primary">Mi Perfil</a>';
                                        }else
                                        {
                                            echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                                        }
echo '                                  <!-- Boton dropdown-->
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>
                                                <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                            </ul>
                                        </div>   
                                        <!-- Modal para reportar a usuario-->                   
                                        <div class="modal fade bs-example-modal-sm-report-u" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Reportar Usuario</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form role="form">
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>Spam</option>
                                                            <option>Usuario Molesto</option>
                                                            <option>Contenido no Apto</option>
                                                        </select>
                                                    </div>
                                                </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-primary">Enviar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal para reportar a usuario--> 
                                        <!-- Modal para enviar error-->                    
                                        <div class="modal fade bs-example-modal-sm-err" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Informar sobre error</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form role="form">
                                                            <div class="form-group">
                                                                <textarea class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-primary">Enviar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal para error-->    
                                        <!-- Modal de reporte de nota-->                   
                                        <div class="modal fade bs-example-modal-sm-report-n" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Reportar Nota</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form role="form">
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>Spam</option>
                                                            <option>Contenido Ofensivo</option>
                                                            <option>Contenido no Apto</option>
                                                        </select>
                                                    </div>
                                                </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-primary">Enviar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal de reporte de nota-->  
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default hidden-xs hidden-sm">
                                    <div class="panel-body">
                                        Ads
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        Ads
                                    </div>
                                </div>
                                <h1>Notas de ';
                                $generic->getLucky("FIRSTNAME");
echo '                          </h1>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="row">
                                            <!-- Nota 1 -->';
                                            $generic->getNotes(10,"notesDir");                                
echo '                                  </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-1"></div>
        </section>
    </div>
    <!-- Fin de contenido -->';



        echo '<div id="footer">
            <div class="container">
                <p class="footer-text">&copy; 2014 Fanplusplus &middot; <a href="#">Terminos y Condiciones</a> &middot; <a href="https://www.twitter.com/fanplusplus_es" target="_blank" >@fanplusplus_es</a></p>
            </div>
        </div>';
        echo "\n";
        echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
        echo "\n";
        echo '<script src="../../static/js/bootstrap.min.js"></script>';   
        echo "\n";
        echo '<script src="../../static/js/fluid.js"></script>';        
        echo "\n";
        echo '<script src="../../static/js/file-input.js"></script>';        
        echo "\n";
        echo '<!-- File input script -->';        
        echo "\n";
        echo '<script>';        
        echo "\n";
        echo "$('input[type=file]').bootstrapFileInput();";        
        echo "\n";
        echo "$('.file-inputs').bootstrapFileInput();";     
        echo "\n";
        echo '</script>';    
        echo "\n";
        echo '</body>';
        echo "\n";
        echo '</html>';
    }

}

?>