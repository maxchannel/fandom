<?php
//OBJETOS SOLO PARA EL INDEX
//Clase que lleva la actividad de cada usuario basandose en el usuario 

class Activity
{
    //Imprime en el index a los fandoms con mas miebros
	public function popularFandoms()
	{
        include 'houston.php';
	    include 'link.php';
	    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
	    //Query para saber los primeros 5 registors mas repetidos de la columna club_id
	    $result = mysqli_query($con,"SELECT FANDOM_ID, COUNT(FANDOM_ID) AS TOTAL FROM fandom_members GROUP BY FANDOM_ID ORDER BY TOTAL DESC LIMIT 6");

		while($row = mysqli_fetch_array($result))
		{
			$result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['FANDOM_ID']."' ORDER BY ID DESC LIMIT 1");
			$row2 = mysqli_fetch_array($result2);
			$result3 = mysqli_query($con,"SELECT FANDOM,MODE,URL FROM fandoms WHERE ID='".$row['FANDOM_ID']."' ");
			$row3 = mysqli_fetch_array($result3);

            echo '<!-- Fandom  -->
                <div class="col-md-12">
                    <div class="panel panel-default fandom-panel">
                        <div class="panel-body">';
                            //Avatar, Nombre y Miembros
                            if($row3['MODE'] == "fandom")
                            {
                                echo '<a href="'.strtolower($row3['FANDOM']).'" ><img src="'.$row2['PHOTO_SERVER'].'" class="fandom-inside-image" /></a>';
                                echo '<p class="fandom-title"><a href="'.strtolower($row3['FANDOM']).'">'.$row3['FANDOM'].'</a></p>';
                                echo '<p class="fandom-members">Miembros: <a href="'.strtolower($row3['FANDOM']).'/members" >'.$row['TOTAL'].'</a></p>';
                            }elseif($row3['MODE'] == "fans")
                            {
                                echo '<a href="'.$row3['URL'].'" ><img src="'.$row2['PHOTO_SERVER'].'" class="fandom-inside-image" /></a>';
                                echo '<p class="fandom-title"><a href="'.$row3['URL'].'">'.$row3['FANDOM'].'</a></p>';
                                echo '<p class="fandom-members">Miembros: <a href="'.$row3['URL'].'/members" >'.$row['TOTAL'].'</a></p>';
                            }
            echo '      </div>
                        <div class="panel-footer">';
                            //Boton de unirme o dejar
                            $fandoms = new Fandom($row['FANDOM_ID']);
                            $fandoms->ifFan($row['FANDOM_ID'], $_SESSION['user_id']);
            echo '          <!-- Boton dropdown-->
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>';
		}
        $fandoms = null;
	}

    //Lleva perfiles aleatoriamente al index para seguir
    public function randomProfiles()
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
        $result = mysqli_query($con,"SELECT * FROM profiles ORDER BY RAND() LIMIT 3");
        while($row = mysqli_fetch_array($result))
        {
            //Cantidad de followers
            $followers = mysqli_query($con,"SELECT ID FROM followers WHERE FOLLOWTO_ID='".$row['ID']."'");

            echo '<!-- Perfil  -->
                <div class="col-md-12">
                    <div class="panel panel-default fandom-panel">
                        <div class="panel-body">
                            <a href="'.$row['USERNAME'].'"><img src="'.$row['AVATAR'].'" class="fandom-inside-image" /></a>
                            <p class="fandom-title"><a href="'.$row['USERNAME'].'">'.$row['FIRSTNAME'].'</a></p>
                            <p class="fandom-members">Seguidores: <a href="'.$row['USERNAME'].'/followers">'.mysqli_num_rows($followers).'</a></p>
                        </div>
                        <div class="panel-footer">';
                        if (isset($_SESSION['user_id']) && ($row['USERNAME'] != $_SESSION['username']) ) 
                        {
                            //Decidiendo que boron mostrar: seguir o dejar de seguir
                            $resultM = mysqli_query($con,"SELECT ID FROM followers WHERE THIS_ID='".$_SESSION['user_id']."' AND FOLLOWTO_ID='".$row['ID']."'");
                            $total = mysqli_num_rows($resultM);                    
                            if($total > 0)
                            {
                                echo '<button type="button" onclick="unfollowUser('.$row['ID'].','.$_SESSION['user_id'].',this)" class="btn btn-danger" id="btn-unfollow" >Dejar de Seguir</button>';
                            }else
                            {
                                echo '<button type="button" onclick="followUser('.$row['ID'].','.$_SESSION['user_id'].', this)" class="btn btn-primary" id="btn-follow" >Seguir</button>';
                            }
                        }elseif($row['USERNAME'] == $_SESSION['username'])
                        {
                            echo '<a href="../'.$_SESSION['username'].'" class="btn btn-primary">Mi Perfil</a>';
                        }else
                        {
                            echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                        }
                echo        '<!-- Boton dropdown-->
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>';
        }
        mysqli_close($con);
    }

    public function lastFor($id)//Trae la actividad mas reciente del usuario en sesiona activa
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
        $result = mysqli_query($con,"SELECT FANDOM_ID FROM fandom_members WHERE MEMBER_ID='$id'");
        echo '<table>
        <tr>
        <td></td>
        <td>Fecha</td>
        </tr>';
        while($row = mysqli_fetch_array($result))
        {
            $result2 = mysqli_query($con,"SELECT PHOTO_SERVER,FECHA FROM fandom_images WHERE FANDOM_ID='".$row['FANDOM_ID']."'");
            $row2 = mysqli_fetch_array($result2);
            echo '<tr>';
            echo '<td><img src="'.$row2['PHOTO_SERVER'].'" height="100" width="100"></td>';
            echo '<td>'.$row2['FECHA'].'</td>';
            echo '</tr>';
        }
        echo '</table>';

        mysqli_close($con);
    }

    public function lastFandoms($limit)//Trae los 5 Fandoms mas recientes
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
        $limit = mysqli_real_escape_string($con, $limit);
        $result = mysqli_query($con,"SELECT * FROM fandoms ORDER BY CREATION DESC LIMIT $limit");//Para las 5 fotos que sea las que mas likes tienen
        echo '<table>
        <tr>
        <td></td>
        <td>Fecha</td>
        <td>Creación</td>
        </tr>';
        while($row = mysqli_fetch_array($result))
        {
            if($row['MODE'] == "fandom") 
            {
                $url = strtolower($row['FANDOM']);
                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY ID DESC LIMIT 1");
                $row2 = mysqli_fetch_array($result2);
                echo '<tr>';
                echo '<td><a href="'.$url.'"><img src="'.$row2['PHOTO_SERVER'].'" height="50" width="50"></a></td>';
                echo '<td><a href="#">'.$row['CATEGORY'].'</a></td>';
                echo '<td>'.$row['CREATION'].'</td>';
                echo '</tr>';
            }elseif($row['MODE'] == "fans")
            {
                $result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['ID']."' ORDER BY ID DESC LIMIT 1");
                $row2 = mysqli_fetch_array($result2);
                echo '<tr>';
                echo '<td><a href="'.$row['URL'].'"><img src="'.$row2['PHOTO_SERVER'].'" height="50" width="50"></a></td>';
                echo '<td><a href="#">'.$row['CATEGORY'].'</a></td>';
                echo '<td>'.$row['CREATION'].'</td>';
                echo '</tr>';
            }
        }
        echo '</table>';

        mysql_close($con);
    }


	public function getNotesIndex($limit)//Recibe el parametro del ID del Fandom y el limite a traer.
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        $limit = mysqli_real_escape_string($con, $limit);
		$result = mysqli_query($con,"SELECT * FROM fandom_notes WHERE FANDOM_ID='".$this->id."' ORDER BY REGISTER DESC LIMIT $limit");

		while($row = mysqli_fetch_array($result))
		{
			$result2 = mysqli_query($con,"SELECT USERNAME,AVATAR FROM profiles WHERE ID='".$row['PROFILE_ID']."' ");
            $row2 = mysqli_fetch_array($result2);
			echo '<article id="note">';
			echo '<div id="noteAvatar"><a href="../'.$row2['USERNAME'].'"><img src="../'.$row2['AVATAR'].'" height="50" width="50"></a></div>';
			echo '<div id="noteDate">'.$row['REGISTER'].'</div>';
			echo '<div style="font-family:'.$row['FONT'].'" >'.$row['NOTE'].'</div>';
			if (isset($_SESSION['username']) && !empty($_SESSION['username'])) 
			{
                $result3 = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row['ID']."' ");
                $total = mysqli_num_rows($result3);            

                if($total == 1)
                {
                    echo '<div id="noteSandbox"><button id="btnDelNotes" onclick="deleteSandbox('.$row['ID'].','.$_SESSION['user_id'].',this)" >Eliminar del Sandbox</button></div>';
                }else
                {
                    echo '<div id="noteSandbox"><button id="btnSandNotes" onclick="noteSandbox('.$row['ID'].','.$_SESSION['user_id'].',1,'.$row['FANDOM_ID'].',this)" >Agregar al Sandbox</button></div>';
                }
			}
			echo '</article>';
		}

		mysqli_close($con);
	}

	//METODOS CUANDO SE ESTE LOGUEADO
    //Va por las notas escritas en fandoms que sigue el usuario
	public function getFandomNotes($user_id,$limit)//Recibe el parametro del ID del Fandom y el limite a traer.
	{
	    include 'link.php';
	    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        $user_id = mysqli_real_escape_string($con, $user_id);
        $limit = mysqli_real_escape_string($con, $limit);

        //$result = mysqli_query($con, "SELECT ID,FANDOM_ID FROM fandom_members WHERE MEMBER_ID='".$user_id."' ");
        $result = mysqli_query($con,"SELECT * FROM fandom_notes LIMIT ".$limit." ");

	    while($row = mysqli_fetch_array($result))
	    {
            $resultTotal = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$row['FANDOM_ID']."' AND MEMBER_ID='".$user_id."'");
            if(mysqli_num_rows($resultTotal) == 1)//If que determina si el usuario es miembro del fandom para imprimir la nota sino simplemente pasa de ella
            {
                // $result2 = mysqli_query($con,"SELECT * FROM fandom_notes WHERE FANDOM_ID='".$row['FANDOM_ID']."'");
                // $row2 = mysqli_fetch_array($result2);
                $result2 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['PROFILE_ID']."' ");
                $row2 = mysqli_fetch_array($result2);    

                echo '<article id="notaArea">';
                echo '    <div id="nota1">';
                echo '        <div id="notaAvatar">';
                echo '            <a href="'.strtolower($row2['USERNAME']).'"><img src="'.$row2['AVATAR'].'" height="50" width="50" ></a>';
                echo '        </div>';
                echo '        <div id="notaname"><a href="'.$row2['USERNAME'].'">'.$row2['USERNAME'].'</a></div>';
                echo '        <div id="notaDate">'.$row['REGISTER'].'</div>';
                if (isset($_SESSION['username']) && !empty($_SESSION['username'])) 
                {
                $result4 = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                $total = mysqli_num_rows($result4);                

                if($total == 1)
                {
                    echo '<div id="notaSandbox"><button id="btnDelNotes" onclick="deleteSandbox('.$row['ID'].','.$_SESSION['user_id'].',1,this)" >Eliminar del Sandbox</button></div>';
                }else
                {
                    echo '<div id="notaSandbox"><button id="btnSandNotes" onclick="noteSandbox('.$row['ID'].', '.$_SESSION['user_id'].', '.$row['PROFILE_ID'].', '.$row['FANDOM_ID'].', 1, this)" >Agregar al Sandbox</button></div>';
                }
                }
                echo '    </div>';
                echo '    <div id="nota2">';
                echo '        <div id="" style="font-family: '.$row['FONT'].'" >'.$row['NOTE'].'</div>';
                echo '    </div>';
                echo '</article>';
            }

		}

		mysqli_close($con);
	}

    //Va por las notas escritas en perfiles que sigue el usuario
    public function getProfileNotes($limit)//Recibe el parametro del ID del Fandom y el limite a traer.
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        $limit = mysqli_real_escape_string($con, $limit);
        $result = mysqli_query($con, "SELECT FOLLOWTO_ID FROM followers WHERE THIS_ID='".$_SESSION['user_id']."' ");

        while($row = mysqli_fetch_array($result))
        {
            $result2 = mysqli_query($con,"SELECT * FROM profile_notes WHERE FROM_ID='".$row['FOLLOWTO_ID']."'");
            $row2 = mysqli_fetch_array($result2);
            $result3 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row2['FROM_ID']."' ");
            $row3 = mysqli_fetch_array($result3);

            echo '<article id="notaArea">';
            echo '    <div id="nota1">';
            echo '        <div id="notaAvatar">';
            echo '            <a href="'.strtolower($row3['USERNAME']).'"><img src="'.$row3['AVATAR'].'" height="50" width="50" ></a>';
            echo '        </div>';
            echo '        <div id="notaname"><a href="'.$row3['USERNAME'].'">'.$row3['USERNAME'].'</a></div>';
            echo '        <div id="notaDate">'.$row2['REGISTER'].'</div>';
            if (isset($_SESSION['username']) && !empty($_SESSION['username'])) 
            {
                $result4 = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                $total = mysqli_num_rows($result4);            

                if($total == 1)
                {
                    echo '<div id="notaSandbox"><button id="btnDelNotes" onclick="deleteSandbox('.$row2['ID'].','.$_SESSION['user_id'].',0,this)" >Eliminar del Sandbox</button></div>';
                }else
                {
                    echo '<div id="notaSandbox"><button id="btnSandNotes" onclick="noteSandbox('.$row2['ID'].', '.$_SESSION['user_id'].', '.$row2['FROM_ID'].', '.$row2['TO_ID'].', 0,this)" >Agregar al Sandbox</button></div>';
                }
            }
            echo '    </div>';
            echo '    <div id="nota2">';
            echo '        <div id="" style="font-family: '.$row2['FONT'].';" >'.$row2['NOTE'].'</div>';
            echo '    </div>';
            echo '</article>';
        }
        
        mysqli_close($con);
    }

	//Trayendo la ultima actividad en publicaciones que se escribieron en fandoms que sigues
	public function getFandomPublications($limit)//Recibe el parametro del ID del Fandom y el limite a traer.
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Probelmas al conectar");
        $limit = mysqli_real_escape_string($con, $limit);
		//Seleccionando a los fandoms que sigue el usuario
        $result = mysqli_query($con, "SELECT * FROM fandom_members WHERE MEMBER_ID='$_SESSION[user_id]' ");

        while($row = mysqli_fetch_array($result))
        {
        	//Seleccionando las publicaciones mas recientes que se han publicado en esos fandoms que sigue el usuario
            $result2 = mysqli_query($con,"SELECT * FROM fan_publications WHERE PARA_ID='".$row['FANDOM_ID']."'");
            $row2 = mysqli_fetch_array($result2);
            $result3 = mysqli_query($con,"SELECT FANDOM,MODE,URL FROM fandoms WHERE ID='".$row['FANDOM_ID']."'");
        	$row3 = mysqli_fetch_array($result3); 
        	$result4 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['FANDOM_ID']."' ORDER BY FECHA DESC LIMIT 1");
        	$row4 = mysqli_fetch_array($result4);

            echo '<article id="post">';
        	echo '    <div id="post1">';
        	echo '    <div id="postAvatar">';
        	if($row3['MODE'] == "fandom") 
        	{
        		$url = strtolower($row3['FANDOM']); 
        		echo '<a href="';
        		echo $url;
        		echo '"><img src="';
        		echo $row4['PHOTO_SERVER'];
        		echo '" height="50" width="50" ></a>'; 
        	}elseif($row3['MODE'] == "fans")
        	{
        		$url = strtolower($row3['URL']); 
        		echo '<a href="';
        		echo $url;
        		echo '"><img src="';
        		echo $row4['PHOTO_SERVER'];
        		echo '" height="50" width="50" ></a>'; 
        	}
        	echo '    </div>';
        	echo '    <div id="postDate">'.$row2['FECHA'].'</div>';
        	if (isset($_SESSION['username']) && !empty($_SESSION['username'])) 
			{
                $result3 = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                $total = mysqli_num_rows($result3);                
                if($total == 1)
                {
                    echo '<div id="noteSandbox"><button id="btnDelNotes" onclick="deletePublication('.$row2['ID'].', '.$_SESSION['user_id'].',1,this)" >Eliminar del Sandbox</button></div>';
                }else
                {
                    echo '<div id="noteSandbox"><button id="btnSandNotes" onclick="addPublication('.$row2['ID'].', '.$_SESSION['user_id'].', '.$row2['DE_ID'].', '.$row2['PARA_ID'].', 1, this)" >Add To Sandbox</button></div>';
                }
			}
        	echo '    </div>';
        	echo '    <div id="post2">';
        	echo '        <div id="postNote" style="font-family:" >'.$row2['CONTENT'].'</div>';
        	echo '    </div>';
        	echo '</article>';
        }

		mysqli_close($con);
	}

    //Trayendo la ultima actividad en publicaciones que se escribieron en perfiles que sigues
    public function getProfilesPublications($limit)//Recibe el parametro del ID del Fandom y el limite a traer.
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Probelmas al conectar");
        $limit = mysqli_real_escape_string($con, $limit);
        //Seleccionando a los fandoms que sigue el usuario
        $result = mysqli_query($con, "SELECT FOLLOWTO_ID FROM followers WHERE THIS_ID='".$_SESSION['user_id']."' ");

        while($row = mysqli_fetch_array($result))
        {
            //Seleccionando las publicaciones mas recientes que se han publicado en esos fandoms que sigue el usuario
            $result2 = mysqli_query($con,"SELECT * FROM profile_publications WHERE DE_ID='".$row['FOLLOWTO_ID']."'");
            $row2 = mysqli_fetch_array($result2);
            $result3 = mysqli_query($con,"SELECT AVATAR,USERNAME FROM profiles WHERE ID='".$row['FOLLOWTO_ID']."'");
            $row3 = mysqli_fetch_array($result3);

            echo '<article id="post">';
            echo '    <div id="post1">';
            echo '    <div id="postAvatar">';
            $url = strtolower($row3['USERNAME']); 
            echo '<a href="';
            echo $url;
            echo '"><img src="';
            echo $row3['AVATAR'];
            echo '" height="50" width="50" ></a>'; 
            echo '    </div>';

            echo '    <div id="postDate">'.$row2['FECHA'].'</div>';
            if (isset($_SESSION['username']) && !empty($_SESSION['username'])) 
            {
                $result4 = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row2['ID']."' ");
                $total = mysqli_num_rows($result4);                
                if($total == 1)
                {
                    echo '<div id="noteSandbox"><button id="btnDelNotes" onclick="deletePublication('.$row2['ID'].','.$_SESSION['user_id'].',0,this)" >Eliminar del Sandbox</button></div>';
                }else
                {
                    echo '<div id="noteSandbox"><button id="btnSandNotes" onclick="addPublication('.$row2['ID'].','.$_SESSION['user_id'].','.$row2['DE_ID'].','.$row2['PARA_ID'].',0, this)" >Agregar al Sandbox</button></div>';
                }
            }
            echo '    </div>';
            echo '    <div id="post2">';
            echo '        <div id="postNote" style="font-family:" >'.$row2['CONTENT'].'</div>';
            echo '    </div>';
            echo '</article>';
        }

        mysqli_close($con);
    }

	//Va por las imagenes subidas a fandoms que sigue el usuario
	public function getFandomImages($limit,$user_id)
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        $limit = mysqli_real_escape_string($con, $limit);
        $user_id = mysqli_real_escape_string($con, $user_id);
		$result = mysqli_query($con, "SELECT FANDOM_ID FROM fandom_members WHERE MEMBER_ID='$user_id' ");
		//Si es miembro por lo menos de 1 fandom empieza a iterar con las fotos
		if(mysqli_num_rows($result) > 0)
		{
    		while($row = mysqli_fetch_array($result))
            {
            	$result2 = mysqli_query($con,"SELECT * FROM fandom_images WHERE FANDOM_ID='".$row['FANDOM_ID']."'");
                $row2 = mysqli_fetch_array($result2);
                $result3 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['FANDOM_ID']."' ORDER BY FECHA DESC LIMIT 1");
        	$row3 = mysqli_fetch_array($result3);
        	$result4 = mysqli_query($con,"SELECT MODE,URL,FANDOM FROM fandoms WHERE ID='".$row['FANDOM_ID']."'");
                $row4 = mysqli_fetch_array($result4);

            	echo '<article id="imagepost">';
                echo '    <div id="imagepost1">';
                echo '        <div id="postAvatar">';
                if($row4['MODE'] == "fandom")
                {
                    echo '            <a href="'.strtolower($row4['FANDOM']).'"><img src="'.$row3['PHOTO_SERVER'].'" height="50" width="50" ></a> ';
                    echo '        </div>';
                    echo '        <div id="imagename"><a href="'.strtolower($row4['FANDOM']).'">'.$row4['FANDOM'].'</a></div>';
                }elseif($row4['MODE'] == "fans")
                {
                    echo '            <a href="'.$row4['URL'].'"><img src="'.$row3['PHOTO_SERVER'].'" height="50" width="50" ></a> ';
                    echo '        </div>';
                    echo '        <div id="imagename"><a href="'.$row4['URL'].'">'.$row4['FANDOM'].'</a></div>';
                }
                echo '        <div id="postDate">'.$row2['FECHA'].'</div>';
                //Trayendo los botones si hay una sesion
                $result5 = mysqli_query($con,"SELECT ID FROM sandbox_images WHERE IMAGE_ID='".$row2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom' ");
                $total = mysqli_num_rows($result5);            

                if($total > 0)
                {
                    echo '<button id="btnDeleteSandbox" onclick="deleteImageSandbox('.$row2['ID'].','.$user_id.',1,this)">Delete in Sandbox</button>';
                }else
                {
                    echo '<button id="btnAddSandbox" onclick="addSandboxP('.$row2['ID'].','.$user_id.','.$row2['USER_ID'].','.$row2['FANDOM_ID'].',1,this)">Add To Sandbox</button>';
                }
                echo '    </div>';
                echo '    <div id="imagepost2">';
                echo '        <div id="imagearea" ><a href="media.php?fandom='.$row['FANDOM_ID'].'&pic='.$row2['ID'].'"><img src="'.$row2['PHOTO_SERVER'].'" height="240" width="100%" ></a></div>';
                echo '    </div>';
                echo ' </article>';
            }
		}

		mysqli_close($con);
	}

    //Va por las ultimas fotos de usuarios que sigue el logueado
    public function getProfileImages($limit,$user_id)
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        $limit = mysqli_real_escape_string($con, $limit);
        $user_id = mysqli_real_escape_string($con, $user_id);
        $result = mysqli_query($con, "SELECT FOLLOWTO_ID FROM followers WHERE THIS_ID='$user_id' ");
        //Si es miembro por lo menos de 1 fandom empieza a iterar con las fotos
        if(mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_array($result))
            {
                $result2 = mysqli_query($con,"SELECT * FROM profile_photos WHERE USER_ID='".$row['FOLLOWTO_ID']."'");
                $row2 = mysqli_fetch_array($result2);
                $result3 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['FOLLOWTO_ID']."'");
                $row3 = mysqli_fetch_array($result3);

                echo '<article id="imagepost">';
                echo '    <div id="imagepost1">';
                echo '        <div id="postAvatar">';
                echo '            <a href="'.strtolower($row3['USERNAME']).'"><img src="'.$row3['AVATAR'].'" height="50" width="50" ></a> ';
                echo '        </div>';
                echo '        <div id="imagename"><a href="'.strtolower($row3['USERNAME']).'">'.$row3['USERNAME'].'</a></div>';
                echo '        <div id="postDate">'.$row2['FECHA'].'</div>';
                //Trayendo los botones si hay una sesion
                $result5 = mysqli_query($con,"SELECT ID FROM sandbox_images WHERE IMAGE_ID='".$row2['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile' ");
                $total = mysqli_num_rows($result5);            

                if($total == 1)
                {
                    echo '<button id="btnDeleteSandbox" onclick="deleteImageSandbox('.$row2['ID'].','.$user_id.',0,this)">Delete in Sandbox</button>';
                }else
                {
                    echo '<button id="btnAddSandbox" onclick="addSandboxP('.$row2['ID'].','.$user_id.','.$row2['USER_ID'].','.$row2['USER_ID'].',0,this)">Add To Sandbox</button>';
                }
                echo '    </div>';
                echo '    <div id="imagepost2">';
                echo '        <div id="imagearea" ><a href="media.php?id='.$row3['ID'].'&pic='.$row2['ID'].'"><img src="'.$row2['PHOTO_SERVER'].'" height="240" width="100%" ></a></div>';
                echo '    </div>';
                echo ' </article>';
            }
        }

        mysqli_close($con);
    }

    //Borrar cuando se tenga publicaciones de fandom y profiles
    // public function getPublicationsFandom($id,$limit)//mode para profile o visto desde el sandbox
    // {
    //     include 'link.php';
    //     $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    //     $result = mysqli_query($con,"SELECT * FROM fan_publications WHERE PARA_ID='".$id."' ORDER BY FECHA DESC LIMIT $limit"); 
    //     while($row = mysqli_fetch_array($result))
    //     {
    //         $result2 = mysqli_query($con,"SELECT * FROM fan_publications WHERE ID='".$row['ID']."'");
    //         $row2 = mysqli_fetch_array($result2);
    //         $result3 = mysqli_query($con,"SELECT PHOTO_SERVER,FANDOM_ID FROM fandom_images WHERE FANDOM_ID='".$row['PARA_ID']."' ORDER BY FECHA DESC LIMIT 1");
    //         $row3 = mysqli_fetch_array($result3);
    //         $result4 = mysqli_query($con,"SELECT FANDOM FROM fandoms WHERE ID='".$row2['PARA_ID']."'");
    //         $row4 = mysqli_fetch_array($result4);

    //         echo '<article id="post">';
    //         echo '<div id="post1">';
    //         $result2 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['DE_ID']."'");
    //         $row2 = mysqli_fetch_array($result2);
    //         echo '<div id="postAvatar"><a href="../'.$row2['USERNAME'].'"><img src="../'.$row2['AVATAR'].'" width="50" height="50"></a></div>';
    //         echo '<div id="postDate">'.$row['FECHA'].'</div>';

    //         if (isset($_SESSION['username']) && !empty($_SESSION['username'])) 
    //         {
    //             $result3 = mysqli_query($con,"SELECT * FROM sandbox_publications WHERE PUBLICATION_ID='".$row['ID']."' ");
    //             $total = mysqli_num_rows($result3);                
    //             if($total == 1)
    //             {
    //                 echo '<div id="noteSandbox"><button id="btnDelNotes" onclick="deletePublication('.$row['ID'].','.$_SESSION['user_id'].',this)" >Eliminar del Sandbox</button></div>';
    //             }else
    //             {
    //                 echo '<div id="noteSandbox"><button id="btnSandNotes" onclick="addPublication('.$row['ID'].','.$_SESSION['user_id'].',1,'.$row['DE_ID'].',this)" >Agregar al Sandbox</button></div>';
    //             }
    //         }
    //         echo '</div>';
    //         echo '<div id="post2">';
    //         echo '    <div id="postNote" style="font-family:" >'.$row['CONTENT'].'</div>';
    //         echo '</div>';
    //         echo '</article>';
    //     }
    //     mysqli_close($con);
    // }

}//Fin de clase

?>