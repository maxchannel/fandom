<?php
//Clase para las busquedas
#Tener una vista por defecto llamada top que tiene en cuenta: Que tan confiable es el usuario que publico, Republicacion, Velocidad de RP
//Despues agregar $afinidad, $user, $language

class Busqueda
{
    public $busqueda;

    public function Busqueda($busqueda)
	{
	    $this->busqueda = test_input($busqueda);
	}

    public function ifFan($a,$b) //Id del Fandom y id de la sesion
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db);
        $a = mysqli_real_escape_string($con, $a);
        $b = mysqli_real_escape_string($con, $b);
        $result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$a."' AND MEMBER_ID='".$b."'");
        $total = mysqli_num_rows($result);    

        if($total > 0)
        {
            echo '<button id="btnUnfollow" onclick="noFan('.$a.','.$b.',this)" type="button" class="btn btn-danger">Dejar</button>';
            //echo '<button id="btnUnfollow" onclick="noFan('.$a.','.$b.',this)" >Dejar el Fandom</button>';
        }else
        {
            echo '<button id="btnFollow" onclick="makeFan('.$a.','.$b.',this)" type="button" class="btn btn-success">Unirme</button>';
            //echo '<button id="btnFollow" onclick="makeFan('.$a.','.$b.',this)">Unirme al Fandom</button>';
        }

        mysqli_close($con);
    }

	public function Buscar()
	{
		if(isset($this->busqueda) && !empty($this->busqueda))
		{
		    include 'link.php';
		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");    
    		$date = date("Y-m-d");    

            //Insertando al historial de busquedas
    		if (isset($_SESSION['username'])) 
    		{
    			mysqli_query($con, "INSERT INTO searches (BUSQUEDA,USUARIO,FECHA) VALUES('".mysqli_real_escape_string($con, $this->busqueda)."','".$_SESSION['username']."','".$date."')");
    		}else
    		{
    			mysqli_query($con, "INSERT INTO searches (BUSQUEDA,USUARIO,FECHA) VALUES('".$this->busqueda."','Sin Valor','".$date."') ");
    		}    

    		//Lo primero a buscar es en Fandoms
    		$result = mysqli_query($con,"SELECT ID,FANDOM,MODE,URL FROM fandoms WHERE FANDOM LIKE '%".mysqli_real_escape_string($con, $this->busqueda)."%' ");
    		$total = mysqli_num_rows($result);
    		if($total > 0)
    		{
                echo '<!-- Fandoms -->
                <div class="panel panel-default fandom-panel">
                    <div class="panel-body">
                        <div class="row">';
          		while($row = mysqli_fetch_array($result))
          		{
          			if($row['MODE'] == "fandom")
          			{
                        include 'houston.php';
                        $search = new Fandom($row['ID']);

                        echo '<!-- Fandom  -->
                        <div class="col-md-6">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="'.strtolower($row['FANDOM']).'"><img src="'.$search->getLucky("RETURNAVATAR").'" class="fandom-inside-image" /></a>
                                    <p class="fandom-title"><a href="'.strtolower($row['FANDOM']).'">'.$row['FANDOM'].'</a></p>
                                    <p class="fandom-members">Miembros: <a href="/'.strtolower($row['FANDOM']).'/members">'.$search->Numbers($row['ID'],"FANS").'</a></p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                        $this->ifFan($row['ID'], $_SESSION['user_id']);
                                    }else
                                    {
                                        echo '<a href="../signin.php" class="btn btn-primary" >Login</a>';
                                    }
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
          			}elseif($row['MODE'] == "fans")
          			{
                        //Instanciando la clase para poder imprimir info del fandom
                        include 'houston.php';
                        $search = new Fandom($row['ID']);

                        echo '<!-- Fandom  -->
                        <div class="col-md-6">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="'.$row['URL'].'" ><img src="'.$search->getLucky("RETURNAVATAR").'" class="fandom-inside-image" /></a>
                                    <p class="fandom-title"><a href="'.$row['URL'].'">'.$row['FANDOM'].'</a></p>
                                    <p class="fandom-members">Miembros: <a href="/'.$row['URL'].'/members">'.$search->Numbers($row['ID'],"FANS").'</a></p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                        $this->ifFan($row['ID'], $_SESSION['user_id']);
                                    }else
                                    {
                                        echo '<a href="../signin.php" class="btn btn-primary" >Login</a>';
                                    }
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
          			}
          		}   
                echo '</div>
                        </div><!-- fin de row -->
                        <div class="panel-footer">
                            <span class="label label-danger">Fandoms</span>
                        </div>
                    </div>';
    		}else
    		{
    			echo '<div class="panel panel-default">
                        <div class="panel-body">
                            <p>No se encontraron Fandoms.</p>
                        </div>
                    </div>';
    		}

            function ifFollow($a,$b) //a.- usuario de quien es el perfil, b.- sesion del visitante
            {
                include 'link.php';
                $con = mysqli_connect($host,$user,$pw,$db);
                $a = mysqli_real_escape_string($con, $a);
                $b = mysqli_real_escape_string($con, $b);
                $result = mysqli_query($con,"SELECT ID FROM followers WHERE THIS_ID='".$b."' AND FOLLOWTO_ID='".$a."'");
                $total = mysqli_num_rows($result);        

                if($total > 0)
                {
                    echo '<button type="button" onclick="unfollowUser('.$a.','.$b.',this)" class="btn btn-danger" id="btn-unfollow" >Dejar de Seguir</button>';
                }else
                {
                    echo '<button type="button" onclick="followUser('.$a.','.$b.', this)" class="btn btn-primary" id="btn-follow" >Seguir</button>';
                }        

                mysqli_close($con);
            }

            //Busqueda de perfiles
    		$result7 = mysqli_query($con,"SELECT ID,USERNAME,FIRSTNAME,AVATAR FROM profiles WHERE USERNAME LIKE '%".mysqli_real_escape_string($con, $this->busqueda)."%' ");
    		$total6 = mysqli_num_rows($result7);
    		if($total6 > 0)
    		{
                echo '<!-- Fandoms -->
                <div class="panel panel-default fandom-panel">
                    <div class="panel-body">
                        <div class="row">';
                while($row7 = mysqli_fetch_array($result7))
                {
                    $followers = mysqli_query($con,"SELECT ID FROM followers WHERE FOLLOWTO_ID='".$row2['ID']."'");

                    echo '<!-- Perfil -->
                        <div class="col-md-6">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="'.$row7['USERNAME'].'"><img src="../'.$row7['AVATAR'].'" class="fandom-inside-image" /></a>
                                    <p class="fandom-title"><a href="'.strtolower($row7['USERNAME']).'">'.$row7['FIRSTNAME'].'</a></p>
                                    <p class="fandom-members">Seguidores: <a href="'.$row7['USERNAME'].'/followers" >'.mysqli_num_rows($followers).'</a></p>
                                </div>
                                <div class="panel-footer">';
                                    //<button type="button" class="btn btn-primary">Seguir</button>
                                    if (isset($_SESSION['username']) && ($row2['USERNAME'] != $_SESSION['username']) ) 
                                    {
                                        ifFollow($row7['ID'],$_SESSION['user_id']);
                                    }elseif($row2['USERNAME'] == $_SESSION['username'])
                                    {
                                        echo '<a href="../'.$_SESSION['username'].'" class="btn btn-primary">Mi Perfil</a>';
                                    }else
                                    {
                                        echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                                    }

                echo                '<!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
                }   
                echo '</div>
                        </div><!-- fin de row -->
                        <div class="panel-footer">
                            <span class="label label-primary">Perfiles</span>
                        </div>
                    </div>';

    		}else
    		{
                echo '<div class="panel panel-default">
                        <div class="panel-body">
                            <p>No se encontraron usuarios.</p>
                        </div>
                    </div>';
            }

    		//Buscando en imagenes subidas al fandom donde la descripcion coincida con la busqueda
    		$result2 = mysqli_query($con,"SELECT USER_ID,PHOTO_SERVER,FANDOM_ID,ID,FECHA FROM fandom_images WHERE PHOTO_NAME LIKE '%".mysqli_real_escape_string($con, $this->busqueda)."%' ");
    		$total2 = mysqli_num_rows($result2);
    		if($total2 > 0)
    		{
                echo '<!-- Imagenes -->
                <div class="panel panel-default fandom-panel">
                    <div class="panel-body">
                        <div class="row">';
          		while($row2 = mysqli_fetch_array($result2))
          		{
                    $resultP = mysqli_query($con, "SELECT FIRSTNAME,AVATAR,USERNAME FROM profiles WHERE ID='".$row2['USER_ID']."' ");
                    $rowP = mysqli_fetch_array($resultP);

                    echo '<!-- Imagen  -->
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p><a href="media.php?fandom='.$row2['FANDOM_ID'].'&pic='.$row2['ID'].'" >'.$row2['FECHA'].'</a></p>
                            </div>
                            <a href="media.php?fandom='.$row2['FANDOM_ID'].'&pic='.$row2['ID'].'" ><img src="'.$row2['PHOTO_SERVER'].'" class="img-responsive"></a>
                            <div class="panel-footer">
                                <a href="'.$rowP['USERNAME'].'"><img src="'.$rowP['AVATAR'].'" class="post-inside-image" /></a>
                                <a href="'.$rowP['USERNAME'].'">'.$rowP['FIRSTNAME'].'</a>
                                <!-- Boton dropdown-->
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-img">Reportar</a></li>
                                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>';
          		}
                echo '</div>
                    </div>
                    <div class="panel-footer">
                        <span class="label label-success">Imágenes</span>
                    </div>
                </div>';
    		}

    		//Buscando notas publicadas en fandoms
    		$result3 = mysqli_query($con,"SELECT NOTE,FONT,PROFILE_ID FROM fandom_notes WHERE NOTE LIKE '%".mysqli_real_escape_string($con, $this->busqueda)."%' ");
    		$total2 = mysqli_num_rows($result3);
    		if($total2 > 0)
    		{
                echo '<!-- Notas -->
                <div class="panel panel-default fandom-panel">
                    <div class="panel-body">';
          		while($row3 = mysqli_fetch_array($result3))
          		{
                    $resultP = mysqli_query($con, "SELECT FIRSTNAME,AVATAR,USERNAME FROM profiles WHERE ID='".$row3['PROFILE_ID']."' ");
                    $rowP = mysqli_fetch_array($resultP);

                    echo '<!-- Nota s -->
                        <div class="col-md-12 col-lg-12">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="'.$rowP['USERNAME'].'"><img src="'.$rowP['AVATAR'].'" class="post-inside-image" /></a>
                                    <a href="'.$rowP['USERNAME'].'">'.$rowP['FIRSTNAME'].'</a>
                                    <p class="pull-right">16.05.2014</p>
                                </div>
                                <div class="panel-body">
                                    <p class="'.$row3['FONT'].'">'.$row3['NOTE'].'</p>
                                </div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary">Sandbox</button>
                                    <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
          		}    
                echo '</div>
                    <div class="panel-footer">
                        <span class="label label-warning">Notas en Fandoms</span>
                    </div>
                </div>';
    		}

    		//Buscando notas publicadas en fandoms
    		$result4 = mysqli_query($con,"SELECT CONTENT,DE_ID FROM fan_publications WHERE CONTENT LIKE '%".mysqli_real_escape_string($con, $this->busqueda)."%' ");
    		$total3 = mysqli_num_rows($result4);
    		if($total3 > 0)
    		{
                echo '<!-- Publicaciones -->
                <div class="panel panel-default fandom-panel">
                    <div class="panel-body">
                        <div class="row">';
          		while($row4 = mysqli_fetch_array($result4))
          		{
                    $resultP = mysqli_query($con, "SELECT FIRSTNAME,AVATAR,USERNAME FROM profiles WHERE ID='".$row4['DE_ID']."' ");
                    $rowP = mysqli_fetch_array($resultP);

                    echo '<!-- Publicacion  -->
                        <div class="col-md-12 col-lg-12">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="'.$rowP['USERNAME'].'"><img src="'.$rowP['AVATAR'].'" class="post-inside-image" /></a>
                                    <a href="'.$rowP['USERNAME'].'">'.$rowP['FIRSTNAME'].'</a>
                                    <p class="pull-right"><a href="#">16.05.2014</a></p>
                                </div>
                                <div class="panel-body">
                                    <p>'.$row4['CONTENT'].'</p>
                                </div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary">Sandbox</button>
                                    <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
          		}    
                echo '</div>
                    </div>
                    <div class="panel-footer">
                        <span class="label label-info">Publicaciones en Fandoms</span>
                    </div>
                </div>';
    		}

    		mysqli_close($con);

		}//Fin de if que revisa la existencia y que no este vacia la busqueda
	}//Fin de funcion buscar

	public function loMasBuscado()
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar."); 
		$result = mysqli_query($con, "SELECT BUSQUEDA, COUNT(*) Total FROM searches GROUP BY BUSQUEDA HAVING COUNT(*) > 1 ORDER BY Total DESC LIMIT 20");

		while($row = mysqli_fetch_array($result))
		{
			echo '<a href="search.php?q='.$row['BUSQUEDA'].'" >'.$row['BUSQUEDA'].'</a><br>';
		}

         mysqli_close($con);
	}//Fin de funcion de los mas buscados
}//Fin de clase


?>