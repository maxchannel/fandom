<?php
//PARA LOS FANDOMS
//Ultima ves editado: 29 de Septiembre, al parecer no se incluye la clase al llamarla con require por lo tanto no llama a la clase
class Fandom
{
	public $id, $name, $description, $category, $creation,$url, $website, $twtr, $fb;

	//Funcion constructora
	public function Fandom($id)//Recibe el id del club por el que obtendremos la informacion y la columna en la db
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar en la db.");

		//Haciendo consulta del fandom
		$id = mysqli_real_escape_string($con, $id);
		$result = mysqli_query($con,"SELECT * FROM fandoms WHERE ID='".$id."'");
		$row = mysqli_fetch_array($result);

		if(mysqli_num_rows($result) != 0)
		{
			$this->id = $row['ID'];
			$this->name = $row['FANDOM'];
			$this->description = $row['DESCRIPTION'];
			$this->category = $row['CATEGORY'];
			$this->creation = $row['CREATION'];
			if($row['MODE'] == "fans")
			{
				$this->url = $row['URL'];
			}elseif($row['MODE'] == "fandom")
			{
				$this->url = strtolower($row['FANDOM']);
			}
			$this->twtr = $row['TWTR'];
			$this->website = $row['WEBSITE'];
			$this->fb = $row['FB'];
		}

		mysqli_close($con);
	}

	//Funcion para mostrar los atributos
	public function getLucky($atributo) //Recibe por que atributo ira
	{
		switch ($atributo) 
		{
			case 'ID':
				echo $this->id;
				break;

			case 'RETURNID':
			    return $this->id;
			    break;

			case 'NAME':
				echo $this->name;
				break;

			case 'RETURNNAME':
				return $this->name;
				break;

			case 'DESCRIPTION':
				echo $this->description;
				break;

			case 'CATEGORY':
				return $this->category;
				break;
				
		    case 'RETURNCATEGORY':
				return $this->category;
				break;

			case 'CREATION':
				echo $this->creation;
				break;

			case 'AVATAR':
    			include 'link.php';
        		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
        		$result = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$this->id."' ORDER BY FECHA DESC LIMIT 1");
        		$row = mysqli_fetch_array($result);    
        		echo $row['PHOTO_SERVER'];
        		mysqli_close($con);
			    break;

			case 'RETURNAVATAR':
    			include 'link.php';
        		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
        		$result = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$this->id."' ORDER BY FECHA DESC LIMIT 1");
        		$row = mysqli_fetch_array($result);    
        		return $row['PHOTO_SERVER'];
        		mysqli_close($con);
			    break;

			case 'URL':
			    echo $this->url;
			    break;

			case 'RETURNURL':
			    return $this->url;
			    break;

			case 'TWTR':
			    echo $this->twtr;
			    break;

			case 'WEBSITE':
			    return $this->website;
			    break;

			case 'FB':
			    echo $this->fb;
			    break;
			   
    			
    		default:
    			echo "Sin valores aún.";
    			break;
		}
	}

	public function Numbers($id,$columna)//Recibe la columna que se desea contabilizar en la base de datos
	{
		if($this->id == $id)
		{
			switch ($columna) 
    		{
    			case 'IMAGES':
    			    include 'link.php';
        		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        		    $result = mysqli_query($con,"SELECT ID FROM fandom_images WHERE FANDOM_ID='".$this->id."'");
        		    if(mysqli_num_rows($result) == 0)
        		    {
        		    	return 0;
        		    }else
        		    {
        		    	return mysqli_num_rows($result);
        		    }
        		    mysqli_close($con);
    			    break;      

    			case 'FANS':
    			    include 'link.php';
        		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        		    $result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$this->id."'");
        		    return mysqli_num_rows($result);
        		    mysqli_close($con);
    			    break;    

    			case 'PUBLICATIONS':
    			    include 'link.php';
        		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        		    $result = mysqli_query($con,"SELECT ID FROM fan_publications WHERE PARA_ID='".$this->id."'");
        		    return mysqli_num_rows($result);
        		    mysqli_close($con);
    			    break;   

    			case 'NOTES':
    			    include 'link.php';
        		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        		    $result = mysqli_query($con,"SELECT ID FROM fandom_notes WHERE FANDOM_ID='".$this->id."'");
        		    return mysqli_num_rows($result);
        		    mysqli_close($con);
    			    break;
    			
    			default:
    				echo "Aún sin valores";
    				break;
    		}
		}
	}

	//Funcion que retorna si es miembro del fandom para poder publicar
	public function ifMember($fandomID,$userID) //Id del Fandom y id de la sesion
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db);
        $fandomID = mysqli_real_escape_string($con, $fandomID);
        $userID = mysqli_real_escape_string($con, $userID);
        $result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$fandomID."' AND MEMBER_ID='".$userID."'");
        $total = mysqli_num_rows($result);    

        if($total > 0)
        {
        	return 1;
        }

        mysqli_close($con);
    }

	//Funcion para mostrar boton de seguir
	public function ifFan($a,$b) //Id del Fandom y id de la sesion
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db);
        $a = mysqli_real_escape_string($con, $a);
        $b = mysqli_real_escape_string($con, $b);
        $result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$a."' AND MEMBER_ID='".$b."'");
        $total = mysqli_num_rows($result);    

        if($total > 0)
        {
        	echo '<button id="btnUnfollow" onclick="noFan('.$a.','.$b.',this)" type="button" class="btn btn-danger">Dejar</button>';
        	//echo '<button id="btnUnfollow" onclick="noFan('.$a.','.$b.',this)" >Dejar el Fandom</button>';
        }else
        {
        	echo '<button id="btnFollow" onclick="makeFan('.$a.','.$b.',this)" type="button" class="btn btn-success">Unirme</button>';
        	//echo '<button id="btnFollow" onclick="makeFan('.$a.','.$b.',this)">Unirme al Fandom</button>';
        }

        mysqli_close($con);
    }

    //EL ifan 2 es para los publications, photos, notes, followers por que ya existe uno para el index y el boton va en diferente posicion
	public function ifFan2($a,$b) //Id del Fandom y id de la sesion
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db);
        $a = mysqli_real_escape_string($con, $a);
        $b = mysqli_real_escape_string($con, $b);
        $result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$a."' AND MEMBER_ID='".$b."'");
        $total = mysqli_num_rows($result);    

        if($total == 1)
        {
        	echo '<button id="btnUnfollow2" onclick="noFan('.$a.','.$b.',this)">Dejar el Fandom</button>';
        }else
        {
        	echo '<button id="btnFollow2" onclick="makeFan('.$a.','.$b.',this)">Unirme al Fandom</button>';
        }

        mysqli_close($con);
    }

    public function ifFollow($a,$b) //a.- usuario de quien es el perfil, b.- sesion del visitante
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db);
		$a = mysqli_real_escape_string($con, $a);
		$b = mysqli_real_escape_string($con, $b);
		$result = mysqli_query($con,"SELECT ID FROM followers WHERE THIS_ID='".$b."' AND FOLLOWTO_ID='".$a."'");
		$total = mysqli_num_rows($result);

		if($total > 0)
		{
			echo '<button type="button" onclick="unfollowUser('.$a.','.$b.',this)" class="btn btn-danger" id="btn-unfollow" >Dejar de Seguir</button>';
		}else
		{
			echo '<button type="button" onclick="followUser('.$a.','.$b.', this)" class="btn btn-primary" id="btn-follow" >Seguir</button>';
		}

		mysqli_close($con);
	}

    public function getFansOf($id,$forma,$limit)//La funcion va por los seguidores del Fandom del id e imprime un mosaico de fotos o el numero
	{
		if($forma == "MOSAICO")
		{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$id = mysqli_real_escape_string($con, $id);
    		$result = mysqli_query($con,"SELECT MEMBER_ID FROM fandom_members WHERE FANDOM_ID='".$id."' ORDER BY FECHA DESC LIMIT $limit");
    		while($row = mysqli_fetch_array($result))
    		{
    			$result2 = mysqli_query($con,"SELECT ID,USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".$row['MEMBER_ID']."'");
    			$row2 = mysqli_fetch_array($result2);

    			$fans = mysqli_query($con,"SELECT ID FROM followers WHERE FOLLOWTO_ID='".$row2['ID']."'");

    			echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="panel panel-default fandom-panel">
                        <div class="panel-body">
                            <a href="../../'.$row2['USERNAME'].'"><img src="../..'.$row2['AVATAR'].'" class="fandom-inside-image" /></a>
                            <p class="fandom-title"><a href="../../'.$row2['USERNAME'].'" >'.$row2['FIRSTNAME'].'</a></p>
                            <p class="fandom-members">Seguidores: <a href="../../'.$row2['USERNAME'].'/followers" >'.mysqli_num_rows($fans).'</a></p>
                        </div>
                        <div class="panel-footer">';
                            if (isset($_SESSION['username']) && ($row2['USERNAME'] != $_SESSION['username']) ) 
                            {
                                $this->ifFollow($row2['ID'],$_SESSION['user_id']);
                            }elseif($row2['USERNAME'] == $_SESSION['username'])
                            {
                            	echo '<a href="../'.$_SESSION['username'].'" class="btn btn-primary">Mi Perfil</a>';
                            }else
                            {
                            	echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                            }

                echo                '<!-- Boton dropdown-->
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>';
    		}

    		mysqli_close($con);
		}elseif($forma == "NUMEROS")
		{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='$id'");
    		echo mysqli_num_rows($result);

    		mysqli_close($con);
		}else
		{
			echo "Sin valores aún";
		}
	}

	//Funcion que lleva la actividad de cada usuario basandose en el usuario
	public function lastFor($id)//Trae la actividad mas reciente del usuario en sesiona activa
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
		$id = mysqli_real_escape_string($con, $id);
		$result = mysqli_query($con,"SELECT FANDOM_ID FROM fandom_members WHERE MEMBER_ID='".$id."'");
		echo '<table>
		<tr>
		<td></td>
		<td>Fecha</td>
		</tr>';
		while($row = mysqli_fetch_array($result))
		{
			$result2 = mysqli_query($con,"SELECT PHOTO_SERVER,FECHA FROM clubs_photos WHERE FANDOM_ID='".$row['FANDOM_ID']."'");
			$row2 = mysqli_fetch_array($result2);
			echo '<tr>';
			echo '<td><img src="'.$row2['PHOTO_SERVER'].'" height="100" width="100"></td>';
			echo '<td>'.$row2['FECHA'].'</td>';
			echo '</tr>';
		}
		echo '</table>';

		mysqli_close($con);
	}

	public function recentFor($id)//Trae la actividad mas reciente 
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
		$id = mysqli_real_escape_string($con, $id);
		$result = mysqli_query($con,"SELECT FANDOM,AVATAR,CATEGORY,CREATION FROM fandoms ORDER BY CREATION DESC LIMIT 5");
		echo '<table>
		<tr>
		<td></td>
		<td>Fecha</td>
		<td>Creación</td>
		</tr>';
		while($row = mysqli_fetch_array($result))
		{
			$url = strtolower($row['FANDOM']);
			echo '<tr>';
			echo '<td><a href="'.$url.'.php"><img src="'.$row['AVATAR'].'" height="50" width="50"></a></td>';
			echo '<td><a href="#">'.$row['CATEGORY'].'</a></td>';
			echo '<td>'.$row['CREATION'].'</td>';
			echo '</tr>';
		}
		echo '</table>';

		mysqli_close($con);
	}

	public function popularAllTimes()
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
		//Query para saber los primeros 5 registors mas repetidos de la columna FANDOM_ID
		$result = mysqli_query($con,"SELECT FANDOM_ID, COUNT(FANDOM_ID) AS TOTAL FROM fandom_members GROUP BY FANDOM_ID ORDER BY TOTAL DESC LIMIT 5");

		echo '<table>
		<tr>
		<td>Club</td>
		<td>ID</td>
		<td>Fans</td>
		</tr>';
		while($row = mysqli_fetch_array($result))
		{
			$result2 = mysqli_query($con,"SELECT AVATAR FROM fandoms WHERE ID='".$row['FANDOM_ID']."'");
			$row2 = mysqli_fetch_array($result2);
			echo '<tr>';
			echo '<td><a href="#"><img src="'.$row2['AVATAR'].'" height="50" width="50"></a></td>';
			echo '<td>'.$row['FANDOM_ID'].'</td>';
			echo '<td>'.$row['TOTAL'].'</td>';
			echo '</tr>';
		}
		echo '</table>';

		mysqli_close($con);
	}

	public function getAllPhotos($id,$type,$limite)//$type decide si es para un club o para un fan
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");

		if($type == "USER")
		{
			$result = mysqli_query($con,"SELECT USER_ID,ID,PHOTO_SERVER FROM media_photos WHERE USER_ID='".$id."' ORDER BY FECHA DESC $limite");
			while($row = mysqli_fetch_array($result))
    		{
    			echo '<a href="../media.php?fandom='.$row['USER_ID'].'&pic='.$row['ID'].'"><img src="../'.$row['PHOTO_SERVER'].'" width="100" height="100"></a>';
    		}
		}elseif($type == "FANDOM")
		{
			$result = mysqli_query($con,"SELECT FANDOM_ID,ID,PHOTO_SERVER,USER_ID,FECHA FROM fandom_images WHERE FANDOM_ID='$id' ORDER BY FECHA DESC LIMIT $limite");
			while($row = mysqli_fetch_array($result))
    		{
    			$result2 = mysqli_query($con, "SELECT * FROM profiles WHERE ID='".$row['USER_ID']."' ");
    			$row2 = mysqli_fetch_array($result2);

    			//Imprimiendo la columna que corresponde a cada foto en individual
    			echo '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="panel panel-default">
                                  <div class="panel-heading">
                                      <p><a href="../../media.php?fandom='.$row['FANDOM_ID'].'&pic='.$row['ID'].'" >'.$row['FECHA'].'</a></p>
                                  </div>
                                  <a href="../../media.php?fandom='.$row['FANDOM_ID'].'&pic='.$row['ID'].'" ><img src="../../'.$row['PHOTO_SERVER'].'" class="img-responsive"></a>
                                  <div class="panel-footer">
                                      <a href="../../'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'" class="post-inside-image" /></a>
                                      <a href="../../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                                      <!-- Boton dropdown-->
                                      <div class="btn-group pull-right">
                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                              <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" role="menu">
                                              <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dfoto">Eliminar Imágen</a></li>
                                              <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-img">Reportar</a></li>
                                              <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                          </div>';
    		}
		}else
		{
			echo "ERROR 1 - F++"."<br>";
			echo '<a href="#">Report Here</a>';
		}
		mysqli_close($con);
	}

	//Trae todas las notas del fandom al que se instancio la clase, necesita un limite
	public function getNotes($limit)
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
		$result = mysqli_query($con,"SELECT * FROM fandom_notes WHERE FANDOM_ID='".$this->id."' ORDER BY REGISTER DESC LIMIT $limit");

		while($row = mysqli_fetch_array($result))
		{
			$result2 = mysqli_query($con,"SELECT USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".$row['PROFILE_ID']."' ");
            $row2 = mysqli_fetch_array($result2);

            echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default post-panel">
                        <div class="panel-heading">
                            <a href="/'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'" class="post-inside-image" /></a>
                            <a href="/'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                            <p class="pull-right"><a href="'.$row['ID'].'.php" >'.$row['REGISTER'].'</a></p>
                        </div>
                        <div class="panel-body">
                            <p class="'.$row['FONT'].'" >'.$row['NOTE'].'</p>
                        </div>
                        <div class="panel-footer">';
                            if(isset($_SESSION['username']))
                            {
                            	$resultS = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                                $totalS = mysqli_num_rows($resultS);                                    

                                if($totalS > 0)
                                {
                                	echo '<button type="button" class="btn btn-danger" onclick="noteDeleteSandbox('.$row['ID'].','.$_SESSION['user_id'].',1,this)" >Unsandbox</button>';
                                }else
                                {
                                	echo '<button type="button" class="btn btn-primary" onclick="noteSandbox('.$row['ID'].','.$_SESSION['user_id'].', '.$row['PROFILE_ID'].', '.$row['FANDOM_ID'].',1, this)" >Sandbox</button>';
                                }
                            }else
                    		{
                    			echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                    		}
                echo '      <!-- Boton dropdown-->
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-suger-n">Quitar de Fandom</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-n">Reportar</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>';
		}

		mysqli_close($con);
	}

	//Genera una nota independiente necesita el id de la nota
	public function getNotesIndie($id)
	{
		include '../../link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
		$result = mysqli_query($con,"SELECT * FROM fandom_notes WHERE FANDOM_ID='".$this->id."' AND ID='".$id."' ");

		while($row = mysqli_fetch_array($result))
		{
			$result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME,AVATAR FROM profiles WHERE ID='".$row['PROFILE_ID']."' ");
            $row2 = mysqli_fetch_array($result2);

            echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default post-panel">
                        <div class="panel-heading">
                            <a href="/'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'" class="post-inside-image" /></a>
                            <a href="/'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                            <p class="pull-right"><a href="'.$row['ID'].'.php" >'.$row['REGISTER'].'</a></p>
                        </div>
                        <div class="panel-body">
                            <p class="'.$row['FONT'].'" >'.$row['NOTE'].'</p>
                        </div>
                        <div class="panel-footer">';
                            if(isset($_SESSION['username']))
                            {
                            	$resultS = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                                $totalS = mysqli_num_rows($resultS);                                    

                                if($totalS > 0)
                                {
                                	echo '<button type="button" class="btn btn-danger" onclick="noteDeleteSandbox('.$row['ID'].','.$_SESSION['user_id'].',1,this)" >Unsandbox</button>';
                                }else
                                {
                                	echo '<button type="button" class="btn btn-primary" onclick="noteSandbox('.$row['ID'].','.$_SESSION['user_id'].', '.$row['PROFILE_ID'].', '.$row['FANDOM_ID'].',1, this)" >Sandbox</button>';
                                }
                            }else
                    		{
                    			echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                    		}
                echo '      <!-- Boton dropdown-->
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-suger-n">Quitar de Fandom</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-n">Reportar</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>';
		}

		mysqli_close($con);
	}

	public function getPublicationsFandom($id,$limit)//mode para profile o visto desde el sandbox
	{
		include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
        $result = mysqli_query($con,"SELECT * FROM fan_publications WHERE PARA_ID='".$id."' ORDER BY FECHA DESC LIMIT $limit"); 
        while($row = mysqli_fetch_array($result))
        {
        	$result2 = mysqli_query($con,"SELECT * FROM fan_publications WHERE ID='".$row['ID']."'");
            $row2 = mysqli_fetch_array($result2);
            $result3 = mysqli_query($con,"SELECT PHOTO_SERVER,FANDOM_ID FROM fandom_images WHERE FANDOM_ID='".$row['PARA_ID']."' ORDER BY FECHA DESC LIMIT 1");
            $row3 = mysqli_fetch_array($result3);
            $result4 = mysqli_query($con,"SELECT FANDOM FROM fandoms WHERE ID='".$row2['PARA_ID']."'");
            $row4 = mysqli_fetch_array($result4);
            $result5 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['DE_ID']."'");
        	$row5 = mysqli_fetch_array($result5);

            echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default post-panel">
                        <div class="panel-heading">
                            <a href="../'.$row5['USERNAME'].'"><img src="../../'.$row5['AVATAR'].'" class="post-inside-image" /></a>
                            <a href="../'.$row5['USERNAME'].'">'.$row5['FIRSTNAME'].'</a>
                            <p class="pull-right"><a href="'.$row['ID'].'.php" >'.$row['FECHA'].'</a></p>
                        </div>
                        <div class="panel-body">
                            <p>'.$row['CONTENT'].'</p>
                        </div>
                        <div class="panel-footer">';
                            if (isset($_SESSION['username'])) 
                    		{
                                $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='fandom'");
                                $totalS = mysqli_num_rows($resultS);                                           

                              if($totalS > 0)
                                {
                                	echo '<button type="button" class="btn btn-danger" onclick="deletePublication('.$row['ID'].','.$_SESSION['user_id'].',1,this)" >Unsandbox</button>';
                                }else
                                {
                                	echo '<button type="button" class="btn btn-primary" onclick="addPublication('.$row['ID'].', '.$_SESSION['user_id'].', '.$row['DE_ID'].', '.$row['PARA_ID'].', 1, this)" >Sandbox</button>';
                                }
                    		}else
                    		{
                    			echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                    		}
                echo '      <!-- Boton dropdown-->
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-suger-p">Quitar de Fandom</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>';
        }

        mysqli_close($con);
	}

}//Fin de la Clase Fandom
?>