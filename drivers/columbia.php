<?php
//OBJETOS PARA LOS PERFILES DE USUARIO
//La clase user sirve para traer la imformación del usuario.
class Usuario
{
    public $id, $firstname, $username, $biography, $email, $country, $sex, $birthyear, $birthmonth, $birthday, $register_date, $avatar, $language, $verified, $url;

	//Funcion constructora
	public function Usuario($id)//Recibe el id del club por el que obtendremos la informacion y la columna en la db
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db);
		$id = mysqli_real_escape_string($con, $id);
		if(mysqli_connect_errno())
		{
			echo "Error al conectar: ".mysqli_connect_errno();
		}

		$result = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$id."'");
		if(mysqli_num_rows($result) == 1)
		{
			$query = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$id."'");
			$row = mysqli_fetch_array($query);

			$this->id = $row['ID'];
			$this->firstname = $row['FIRSTNAME'];
			$this->username = $row['USERNAME'];
			$this->biography = $row['BIOGRAPHY'];
			$this->email = $row['EMAIL'];
			$this->country = $row['COUNTRY'];
			$this->sex  = $row['SEX'];
			$this->birthday = $row['BIRTHDAY'];
			$this->register_day = $row['REGISTER_DATE'];
			$this->avatar = $row['AVATAR'];
			$this->birthday = $row['LANGUAGE'];
			$this->verified = $row['VERIFIED'];
			$this->url = $row['URL'];
		}else
		{
			echo "Sin Valores Aún.";
		}

		mysqli_close($con);
	}

	//Funcion para mostrar los atributos
	public function getLucky($atributo) //Recibe por que atributo ira
	{
		switch ($atributo) 
		{
			case 'ID':
				echo $this->id;
				break;

			case 'RETURNID':
			    return $this->id;
			    break;

			case 'IDRETURN':
				return $this->id;
				break;

			case 'FIRSTNAME':
				echo $this->firstname;
				break;

			case 'USERNAME':
				echo $this->username;
				break;

			case 'USERNAMERETURN'://Para que no imprima y compare en el if de media que distingue el usuario
				return $this->username;
				break;

			case 'EMAIL':
				echo $this->email;
				break;

			case 'COUNTRY':
			    echo $this->country;
			    break;

			case 'SEX':
			    echo $this->sex;
			    break;

			case 'BIRTHDAY':
			    echo $this->birthday;
			    break;

			case 'REGISTER_DAY':
			    echo $this->register_day;
			    break;

			case 'AVATAR':
			    echo $this->avatar;
			    break;

			case 'BIOGRAPHY':
			    echo $this->biography;
			    break;

			case 'VERIFIED':
			    echo $this->verified;
			    break;

			case 'URL':
			    echo $this->url;
			    break;
    			
    		default:
    			echo "Sin valores aún.";
    			break;
		}
	}

	//La funcion sirve para traer contadores como num. de publicaciones, seguidores, siguiendo, etc
	public function counterPublicationsOf($userID)
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
		$userID = mysqli_real_escape_string($con, $userID);
		$result = mysqli_query($con,"SELECT ID FROM profile_publications WHERE DE_ID='".$userID."'");
		echo mysqli_num_rows($result);

		mysqli_close($con);
	}

	public function counterNotesOf($userID)
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
		$userID = mysqli_real_escape_string($con, $userID);
		$result = mysqli_query($con,"SELECT ID FROM profile_notes WHERE FROM_ID='$userID'");
		echo mysqli_num_rows($result);

		mysqli_close($con);
	}

	public function ifFollow($a,$b) //a.- usuario de quien es el perfil, b.- sesion del visitante
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db);
		$a = mysqli_real_escape_string($con, $a);
		$b = mysqli_real_escape_string($con, $b);
		$result = mysqli_query($con,"SELECT ID FROM followers WHERE THIS_ID='".$b."' AND FOLLOWTO_ID='".$a."'");
		$total = mysqli_num_rows($result);

		if($total > 0)
		{
			echo '<button type="button" onclick="unfollowUser('.$a.','.$b.',this)" class="btn btn-danger" id="btn-unfollow" >Dejar de Seguir</button>';
		}else
		{
			echo '<button type="button" onclick="followUser('.$a.','.$b.', this)" class="btn btn-primary" id="btn-follow" >Seguir</button>';
		}

		mysqli_close($con);
	}

	public function ifFollow2($a,$b) //a.- usuario de quien es el perfil, b.- sesion del visitante
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db);
		$result = mysqli_query($con,"SELECT ID FROM followers WHERE THIS_ID='$a' AND FOLLOWTO_ID='$b' OR THIS_ID='$b' AND FOLLOWTO_ID='$a'");
		$a = mysqli_real_escape_string($con, $a);
		$b = mysqli_real_escape_string($con, $b);
		$total = mysqli_num_rows($result);

		if($total > 0)
		{
			echo '<button id="btnUnfollow2" onclick="unfollowUser('.$a.','.$b.',this)">Dejar de Seguir</button>';
		}else
		{
			echo '<button id="btnFollow2" onclick="followUser('.$a.','.$b.',this)">Seguir</button>';
		}

		mysqli_close($con);
	}

	public function getFollowersOf($id,$limite,$mode)//La funcion va por los seguidorea del id
	{
		if($mode=="index")
		{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$id = mysqli_real_escape_string($con, $id);
    		$limite = mysqli_real_escape_string($con, $limite);
    		$result = mysqli_query($con,"SELECT THIS_ID FROM followers WHERE FOLLOWTO_ID='".$id."' LIMIT $limite");    

    		while($row = mysqli_fetch_array($result))
    		{
    			$result2 = mysqli_query($con,"SELECT ID,USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".$row['THIS_ID']."'");
    		    $row2 = mysqli_fetch_array($result2);

    		    $fans = mysqli_query($con,"SELECT ID FROM followers WHERE FOLLOWTO_ID='".$row2['ID']."'");

    			echo '  <div class="col-md-12">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="../../'.$row2['USERNAME'].'" ><img src="../../'.$row2['AVATAR'].'" class="fandom-inside-image" /></a>
                                    <p class="fandom-title"><a href="../../'.$row2['USERNAME'].'" >'.$row2['FIRSTNAME'].'</a></p>
                                    <p class="fandom-members">Seguidores: <a href="../../'.$row2['USERNAME'].'/followers" >'.mysqli_num_rows($fans).'</a></p>
                                </div>
                                <div class="panel-footer">';
                                    //<button type="button" class="btn btn-primary">Seguir</button>
                                    if (isset($_SESSION['username']) && ($row2['USERNAME'] != $_SESSION['username']) ) 
                                    {
                                        $this->ifFollow($row2['ID'],$_SESSION['user_id']);
                                    }elseif($row2['USERNAME'] == $_SESSION['username'])
                                    {
                                    	echo '<a href="../'.$_SESSION['username'].'" class="btn btn-primary">Mi Perfil</a>';
                                    }else
                                    {
                                    	echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                                    }

                echo                '<!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
    		}    

    		mysqli_close($con);
		}elseif ($mode=='section') 
		{
			{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$id = mysqli_real_escape_string($con, $id);
    		$limite = mysqli_real_escape_string($con, $limite);
    		$result = mysqli_query($con,"SELECT THIS_ID FROM followers WHERE FOLLOWTO_ID='".$id."' LIMIT $limite");    

    		while($row = mysqli_fetch_array($result))
    		{
    			$result2 = mysqli_query($con,"SELECT ID,USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".$row['THIS_ID']."'");
    		    $row2 = mysqli_fetch_array($result2);

    		    $fans = mysqli_query($con,"SELECT ID FROM followers WHERE FOLLOWTO_ID='".$row2['ID']."'");
    		    
    			echo '  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="../../'.$row2['USERNAME'].'" ><img src="../../'.$row2['AVATAR'].'" class="fandom-inside-image" /></a>
                                    <p class="fandom-title"><a href="../../'.$row2['USERNAME'].'" >'.$row2['FIRSTNAME'].'</a></p>
                                    <p class="fandom-members">Seguidores: <a href="../../'.$row2['USERNAME'].'/followers" >'.mysqli_num_rows($fans).'</a></p>
                                </div>
                                <div class="panel-footer">';
                                    //<button type="button" class="btn btn-primary">Seguir</button>
                                    if (isset($_SESSION['username']) && ($row2['USERNAME'] != $_SESSION['username']) ) 
                                    {
                                        $this->ifFollow($row2['ID'],$_SESSION['user_id']);
                                    }elseif($row2['USERNAME'] == $_SESSION['username'])
                                    {
                                    	echo '<a href="../../'.$_SESSION['username'].'" class="btn btn-primary">Mi Perfil</a>';
                                    }else
                                    {
                                    	echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                                    }

                echo                '<!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-report-u">Reportar</a></li>
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
    		}    

    		mysqli_close($con);
		}
		}

	}

	//Funcion contadora de seguidores
	public function followersOf($id)//Requiere el ID del perfil
	{
		include 'link.php';
		$con = mysqli_connect($host,$user,$pw,$db);
		$id = mysqli_real_escape_string($con, $id);
		$result = mysqli_query($con,"SELECT ID FROM followers WHERE THIS_ID='".$id."' OR FOLLOWTO_ID='".$id."'");
		$total = mysqli_num_rows($result);
		echo $total;

		mysqli_close($con);
	}

	public function seeBTN1($a,$b)
	{
		echo '<button onclick="insertPublish('.$a.','.$b.')">Publicar</button>';
	}

	public function getAllPhotos($id,$type,$limite)//$type decide si es para un club o para un fan
	{
		if($type == "fan")//Imprime carrete en arbol
		{
			include 'link.php';
		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
		    $id = mysqli_real_escape_string($con, $id);
		    $limite = mysqli_real_escape_string($con, $limite);
			$result = mysqli_query($con,"SELECT USER_ID,ID,PHOTO_SERVER FROM profile_photos WHERE USER_ID='".$id."' ORDER BY FECHA DESC $limite");
			while($row = mysqli_fetch_array($result))
    		{
    			echo '<div class="col-sm-12 col-md-12 col-lg-12">';
    			echo '    <a href="../media.php?id='.$row['USER_ID'].'&pic='.$row['ID'].'" ><img src="../'.$row['PHOTO_SERVER'].'" class="img-responsive"></a>';
    			echo '</div>';
    		}
		}elseif($type == "fanImageSection")//Imprimime un carrete en la seccion de fotos
		{
			include 'link.php';
		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
		    $id = mysqli_real_escape_string($con, $id);
		    $limite = mysqli_real_escape_string($con, $limite);
			$result = mysqli_query($con,"SELECT USER_ID,ID,PHOTO_SERVER,FECHA FROM profile_photos WHERE USER_ID='".$id."' ORDER BY FECHA DESC ".$limite." ");
			while($row = mysqli_fetch_array($result))
    		{
    			$result2 = mysqli_query($con,"SELECT FIRSTNAME,USERNAME,AVATAR FROM profiles WHERE ID='".$row['USER_ID']."' ");
    			$row2 = mysqli_fetch_array($result2);

    			//Imprimiendo la columna que corresponde a cada foto en individual
    			echo '    <div class="col-sm-3 col-md-3 col-lg-3">
                              <div class="panel panel-default">
                                  <div class="panel-heading">
                                      <p><a href="../media.php?id='.$row['USER_ID'].'&pic='.$row['ID'].'" >'.$row['FECHA'].'</a></p>
                                  </div>
                                  <a href="../media.php?id='.$row['USER_ID'].'&pic='.$row['ID'].'" ><img src="../'.$row['PHOTO_SERVER'].'" class="img-responsive"></a>
                                  <div class="panel-footer">
                                      <a href="../'.$row2['USERNAME'].'"><img src="../'.$row2['AVATAR'].'" class="post-inside-image" /></a>
                                      <a href="../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                                      <!-- Boton dropdown-->
                                      <div class="btn-group pull-right">
                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                              <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" role="menu">
                                              <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dfoto">Eliminar Imágen</a></li>
                                              <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-img">Reportar</a></li>
                                              <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                          </div>';
    		}
		}elseif($type == "club")
		{
			include 'link.php';
		    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
		    $id = mysqli_real_escape_string($con, $id);
		    $limite = mysqli_real_escape_string($con, $limite);
			$result = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE CLUB_ID='".$id."' ORDER BY FECHA DESC LIMIT $limite");
			while($row = mysqli_fetch_array($result))
    		{
    			echo '<img src="'.$row['PHOTO_SERVER'].'" width="100" height="100">';
    		}

    		mysql_close($con);
		}else
		{
			echo "ERROR 1 - F++"."<br>";
			echo '<a href="#">Report Here</a>';
		}
	}

	//Funcion para mostrar boton de seguir
	public function ifFan($a,$b) //Id del Fandom y id de la sesion
    {
        include 'link.php';
        $con = mysqli_connect($host,$user,$pw,$db);
        $a = mysqli_real_escape_string($con, $a);
        $b = mysqli_real_escape_string($con, $b);
        $result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$a."' AND MEMBER_ID='".$b."'");
        $total = mysqli_num_rows($result);    

        if($total > 0)
        {
        	echo '<button id="btnUnfollow" onclick="noFan('.$a.','.$b.',this)" type="button" class="btn btn-danger">Dejar</button>';
        	//echo '<button id="btnUnfollow" onclick="noFan('.$a.','.$b.',this)" >Dejar el Fandom</button>';
        }else
        {
        	echo '<button id="btnFollow" onclick="makeFan('.$a.','.$b.',this)" type="button" class="btn btn-success">Unirme</button>';
        	//echo '<button id="btnFollow" onclick="makeFan('.$a.','.$b.',this)">Unirme al Fandom</button>';
        }

        mysqli_close($con);
    }

	public function getFandomsOf($user_id,$mode)//La funcion va por los clubes de los que eres
	{
		if($mode == "index") 
		{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$user_id = mysqli_real_escape_string($con, $user_id);
    		$result = mysqli_query($con,"SELECT FANDOM_ID FROM fandom_members WHERE MEMBER_ID='".$user_id."' LIMIT 3");
    		
    		while($row = mysqli_fetch_array($result))
    		{  
        		$result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['FANDOM_ID']."' ORDER BY FECHA DESC");
        		$row2 = mysqli_fetch_array($result2);
        		$result3 = mysqli_query($con,"SELECT FANDOM,MODE,URL,ID FROM fandoms WHERE ID='".$row['FANDOM_ID']."'");
        		$row3 = mysqli_fetch_array($result3);  

       			if($row3['MODE'] == "fans")
       			{
       				$url = strtolower($row3['URL']); 
       			}elseif($row3['MODE'] == "fandom")
       			{
       				$url = strtolower($row3['FANDOM']); 
       			}
       			
       			$fans = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$row['FANDOM_ID']."'");

      			echo '<div class="col-md-12">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="../'.$url.'"><img src="../'.$row2['PHOTO_SERVER'].'" class="fandom-inside-image" /></a>
                                    <p class="fandom-title"><a href="../'.$url.'">'.$row3['FANDOM'].'</a></p>
                                    <p class="fandom-members">Miembros: <a href="../../'.$url.'/members">'.mysqli_num_rows($fans).'</a></p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                        $this->ifFan($row3['ID'], $_SESSION['user_id']);
                                    }else
                                    {
                                        echo '<a href="../signin.php" class="btn btn-primary" >Login</a>';
                                    }
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';

        	}
		}elseif($mode == "section") 
		{
			include '../../link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$user_id = mysqli_real_escape_string($con, $user_id);
    		$result = mysqli_query($con,"SELECT FANDOM_ID FROM fandom_members WHERE MEMBER_ID='".$user_id."'");

    		while($row = mysqli_fetch_array($result))
    		{  
        		$result2 = mysqli_query($con,"SELECT PHOTO_SERVER FROM fandom_images WHERE FANDOM_ID='".$row['FANDOM_ID']."' ORDER BY FECHA DESC");
        		$row2 = mysqli_fetch_array($result2);
        		$result3 = mysqli_query($con,"SELECT FANDOM,MODE,URL FROM fandoms WHERE ID='".$row['FANDOM_ID']."'");
        		$row3 = mysqli_fetch_array($result3); 
        		$result3 = mysqli_query($con,"SELECT * FROM fandoms WHERE ID='".$row['FANDOM_ID']."'");
        		$row3 = mysqli_fetch_array($result3);   

       			if($row3['MODE'] == "fans")
       			{
       				$url = strtolower($row3['URL']); 
       			}elseif($row3['MODE'] == "fandom")
       			{
       				$url = strtolower($row3['FANDOM']); 
       			}

       			$fans = mysqli_query($con,"SELECT ID FROM fandom_members WHERE FANDOM_ID='".$row['FANDOM_ID']."'");

      			echo '<div class="col-xm-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="panel panel-default fandom-panel">
                                <div class="panel-body">
                                    <a href="../../'.$url.'"><img src="../../'.$row2['PHOTO_SERVER'].'" class="fandom-inside-image" /></a>
                                    <p class="fandom-title"><a href="../../'.$url.'">'.$row3['FANDOM'].'</a></p>
                                    <p class="fandom-members">Miembros: <a href="../../'.$url.'/members">'.mysqli_num_rows($fans).'</a></p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                        $this->ifFan($row3['ID'], $_SESSION['user_id']);
                                    }else
                                    {
                                        echo '<a href="../signin.php" class="btn btn-primary" >Login</a>';
                                    }
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-report-f">Reportar</a></li>
                                            <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
                        
        	}


		}elseif($mode == "NUMBERS")
		{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
    		$user_id = mysqli_real_escape_string($con, $user_id);
    		$result = mysqli_query($con,"SELECT ID FROM fandom_members WHERE MEMBER_ID='".$user_id."'");
    		echo mysqli_num_rows($result);

    		mysqli_close($con);
		}
	}

	public function getNotes($limit, $mode)//Recibe el parametro del ID del Fandom y el limite a traer.
	{
		if($mode == "notesDir")
		{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$limit = mysqli_real_escape_string($con, $limit);
    		$result = mysqli_query($con,"SELECT * FROM profile_notes WHERE TO_ID='".$this->id."' ORDER BY REGISTER DESC LIMIT $limit");    

    		while($row = mysqli_fetch_array($result))
    		{
    			//Trayendo datos de quien publico
            	$result2 = mysqli_query($con,"SELECT USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".$row['FROM_ID']."'");
            	$row2 = mysqli_fetch_array($result2);
            	//Trayendo datos a quien le publicaron
            	$result3 = mysqli_query($con,"SELECT USERNAME FROM profiles WHERE ID='".$row['TO_ID']."' ");
            	$row3 = mysqli_fetch_array($result3);

                echo '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="../../../'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'" class="post-inside-image" /></a>
                                    <a href="../../../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                                    <p class="pull-right"><a href="../../../'.$row3['USERNAME'].'/notes/'.$row['ID'].'.php" >'.$row['REGISTER'].'</a></p>
                                </div>
                                <div class="panel-body">
                                    <p class="'.$row['FONT'].'">'.$row['NOTE'].'</p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                    	$resultS = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                        $totalS = mysqli_num_rows($resultS);                                    

                                        if($totalS > 0)
                                        {
                                        	echo '<button type="button" class="btn btn-danger" onclick="noteDeleteSandbox('.$row['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                        }else
                                        {
                                        	echo '<button type="button" class="btn btn-primary" onclick="noteSandbox('.$row['ID'].','.$_SESSION['user_id'].', '.$row['FROM_ID'].', '.$row['TO_ID'].', 0, this)" >Sandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnSandNotes" >Agregar al Sandbox</button></div>';
                                        }
                                    }else
                           			{
                           				echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                           			}
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-n">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';  

            }

    		mysqli_close($con);
		}elseif ($mode == "notesIndie")
		{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$limit = mysqli_real_escape_string($con, $limit);
    		$result = mysqli_query($con,"SELECT * FROM profile_notes WHERE ID='' ");    

    		while($row = mysqli_fetch_array($result))
    		{
    			//Trayendo datos de quien publico
            	$result2 = mysqli_query($con,"SELECT USERNAME,AVATAR FROM profiles WHERE ID='".$row['FROM_ID']."'");
            	$row2 = mysqli_fetch_array($result2);
            	//Trayendo datos a quien le publicaron
            	$result3 = mysqli_query($con,"SELECT USERNAME FROM profiles WHERE ID='".$row['TO_ID']."' ");
            	$row3 = mysqli_fetch_array($result3);

                echo '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="../../../'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'" class="post-inside-image" /></a>
                                    <a href="../../../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                                    <p class="pull-right"><a href="../../'.$row['ID'].'.php" >'.$row['REGISTER'].'</a></p>
                                </div>
                                <div class="panel-body">
                                    <p class="'.$row['FONT'].'">'.$row['NOTE'].'</p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                    	$resultS = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                        $totalS = mysqli_num_rows($resultS);                                    

                                        if($totalS > 0)
                                        {
                                        	echo '<button type="button" class="btn btn-danger" onclick="noteDeleteSandbox('.$row['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                        }else
                                        {
                                        	echo '<button type="button" class="btn btn-primary" onclick="noteSandbox('.$row['ID'].','.$_SESSION['user_id'].', '.$row['FROM_ID'].', '.$row['TO_ID'].', 0, this)" >Sandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnSandNotes" >Agregar al Sandbox</button></div>';
                                        }
                                    }else
                           			{
                           				echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                           			}
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-n">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';  

            }

    		mysqli_close($con);
		}elseif($mode == "profile")
		{
			include 'link.php';
    		$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
    		$limit = mysqli_real_escape_string($con, $limit);
    		$result = mysqli_query($con,"SELECT * FROM profile_notes WHERE TO_ID='".$this->id."' ORDER BY REGISTER DESC LIMIT $limit");    

    		while($row = mysqli_fetch_array($result))
    		{
    			//Trayendo datos de quien publico
            	$result2 = mysqli_query($con,"SELECT USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".$row['FROM_ID']."'");
            	$row2 = mysqli_fetch_array($result2);
            	//Trayendo datos a quien le publicaron
            	$result3 = mysqli_query($con,"SELECT USERNAME FROM profiles WHERE ID='".$row['TO_ID']."' ");
            	$row3 = mysqli_fetch_array($result3);

                echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="../../'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'" class="post-inside-image" /></a>
                                    <a href="../../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                                    <p class="pull-right"><a href="../'.$row3['USERNAME'].'/notes/'.$row['ID'].'.php" >'.$row['REGISTER'].'</a></p>
                                </div>
                                <div class="panel-body">
                                    <p class="'.$row['FONT'].'">'.$row['NOTE'].'</p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                    	$resultS = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                        $totalS = mysqli_num_rows($resultS);                                    

                                        if($totalS > 0)
                                        {
                                        	echo '<button type="button" class="btn btn-danger" onclick="noteDeleteSandbox('.$row['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                        }else
                                        {
                                        	echo '<button type="button" class="btn btn-primary" onclick="noteSandbox('.$row['ID'].','.$_SESSION['user_id'].', '.$row['FROM_ID'].', '.$row['TO_ID'].', 0, this)" >Sandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnSandNotes" >Agregar al Sandbox</button></div>';
                                        }
                                    }else
                           			{
                           				echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                           			}
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-n">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';  

            }

    		mysqli_close($con);
		}

		
	}

	public function getPublicationsOf($id,$limit,$mode)//mode para profile o visto desde el sandbox
	{
		if($mode == "profile") 
		{
			include 'link.php';
            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
            $id = mysqli_real_escape_string($con, $id);
            $limit = mysqli_real_escape_string($con, $limit);
            $result = mysqli_query($con,"SELECT * FROM profile_publications WHERE PARA_ID='".$id."' ORDER BY FECHA DESC LIMIT $limit"); 
            while($row = mysqli_fetch_array($result))
            {
            	//Trayendo datos de quien publico
            	$result2 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['DE_ID']."'");
            	$row2 = mysqli_fetch_array($result2);
            	//Trayendo datos a quien le publicaron
            	$result3 = mysqli_query($con,"SELECT USERNAME FROM profiles WHERE ID='".$row['PARA_ID']."' ");
            	$row3 = mysqli_fetch_array($result3);

            	echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="../'.$row2['USERNAME'].'" ><img src="'.$row2['AVATAR'].'"class="post-inside-image" /></a>
                                    <a href="../'.$row2['USERNAME'].'" >'.$row2['FIRSTNAME'].'</a>
                                    <p class="pull-right"><a href="../'.$row3['USERNAME'].'/publications/'.$row['ID'].'.php" >'.$row['FECHA'].'</a></p>
                                </div>
                                <div class="panel-body">
                                    <p>'.$row['CONTENT'].'</p>
                                </div>
                                <div class="panel-footer">';
                                    if (isset($_SESSION['username'])) 
                           			{
                                        $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                        $totalS = mysqli_num_rows($resultS);                                       

                                        if($totalS > 0)
                                        {
                                        	echo '<button type="button" class="btn btn-danger" onclick="deletePublication('.$row['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                        }else
                                        {
                                        	echo '<button type="button" class="btn btn-primary" onclick="addPublication('.$row['ID'].', '.$_SESSION['user_id'].', '.$row['DE_ID'].', '.$row['PARA_ID'].', 0, this)" >Sandbox</button>';
                                        }
                           			}else
                           			{
                           				echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                           			}
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dfoto">Eliminar de mi Perfil</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';          

            }

            mysqli_close($con);
		}elseif($mode == "pubSection") 
		{
			include 'link.php';
            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
            $id = mysqli_real_escape_string($con, $id);
            $limit = mysqli_real_escape_string($con, $limit);
            $result = mysqli_query($con,"SELECT * FROM profile_publications WHERE PARA_ID='".$id."' ORDER BY FECHA DESC LIMIT $limit"); 
            while($row = mysqli_fetch_array($result))
            {
            	//Trayendo datos de quien publico
            	$result2 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['DE_ID']."'");
            	$row2 = mysqli_fetch_array($result2);
            	//Trayendo datos a quien le publicaron
            	$result3 = mysqli_query($con,"SELECT USERNAME FROM profiles WHERE ID='".$row['PARA_ID']."' ");
            	$row3 = mysqli_fetch_array($result3);

            	echo '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="../../'.$row2['USERNAME'].'" ><img src="'.$row2['AVATAR'].'"class="post-inside-image" /></a>
                                    <a href="../../'.$row2['USERNAME'].'" >'.$row2['FIRSTNAME'].'</a>
                                    <p class="pull-right"><a href="../../'.$row3['USERNAME'].'/publications/'.$row['ID'].'.php" >'.$row['FECHA'].'</a></p>
                                </div>
                                <div class="panel-body">
                                    <p>'.$row['CONTENT'].'</p>
                                </div>
                                <div class="panel-footer">';
                                    if (isset($_SESSION['username'])) 
                           			{
                                        $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                        $totalS = mysqli_num_rows($resultS);                                       

                                        if($totalS > 0)
                                        {
                                        	echo '<button type="button" class="btn btn-danger" onclick="deletePublication('.$row['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                        }else
                                        {
                                        	echo '<button type="button" class="btn btn-primary" onclick="addPublication('.$row['ID'].', '.$_SESSION['user_id'].', '.$row['DE_ID'].', '.$row['PARA_ID'].', 0, this)" >Sandbox</button>';
                                        }
                           			}else
                           			{
                           				echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                           			}
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-sand-dfoto">Eliminar de mi Perfil</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';          

            }

            mysqli_close($con);
		}elseif($mode == "sandbox")
		{
            if (isset($_SESSION['user_id'])) 
            {
                include '../link.php';
                $user_id = $_SESSION['user_id'];
                $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                $id = mysqli_real_escape_string($con, $id);
                $limit = mysqli_real_escape_string($con, $limit);
                $result = mysqli_query($con,"SELECT * FROM sandbox_publications WHERE WHO_ADD_ID='".$user_id."' ORDER BY FECHA DESC LIMIT $limit");
                echo '<table border="0">
                <tr>
                    <th>De</th>
                    <th>Nota</th>
                    <th>Fecha</th>
                </tr>';
                while($row = mysqli_fetch_array($result))
                {
                    if($row['MODE'] == "fandom")
                    {
                        $result2 = mysqli_query($con,"SELECT * FROM fan_publications WHERE ID='".$row['PUBLICATION_ID']."'");
                        $row2 = mysqli_fetch_array($result2);
                        $result3 = mysqli_query($con,"SELECT PHOTO_SERVER,FANDOM_ID FROM fandom_images WHERE FANDOM_ID='".$row['FAN_PRO_ID']."' ORDER BY FECHA DESC LIMIT 1");
                        $row3 = mysqli_fetch_array($result3);
                        $result4 = mysqli_query($con,"SELECT FANDOM FROM fandoms WHERE ID='".$row2['PARA_ID']."'");
                        $row4 = mysqli_fetch_array($result4);
                        echo '<tr>';
                        echo '<td><a href="../'.lcfirst($row4['FANDOM']).'"><img src="../'.$row3['PHOTO_SERVER'].'" height="50" width="50"></a></td>';
                        echo '<td>'.$row2['CONTENT'].'</td>';
                        echo '<td>'.$row['FECHA'].'</td>';
                        echo '</tr>';
                    }elseif($row['MODE'] == "profile") 
                    {
                        $result2 = mysqli_query($con,"SELECT * FROM profile_publications WHERE ID='".$row['PUBLICATION_ID']."'");
                        $row2 = mysqli_fetch_array($result2);
                        $result3 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row2['DE_ID']."' ");
                        $row3 = mysqli_fetch_array($result3);
                        echo '<tr>';
                        echo '<td><a href="../'.$row3['USERNAME'].'"><img src="../'.$row3['AVATAR'].'" height="50" width="50"></a></td>';
                        echo '<td>'.$row2['CONTENT'].'</td>';
                        echo '<td>'.$row['FECHA'].'</td>';
                        echo '</tr>';
                    }
                }
                echo '</table>';
                
                mysqli_free_result($result);
                mysqli_close($con);
            }else
            {
                echo "Logueate para ver tus mensajes";
            }
		}

	}

	//Para ir por publicaciones de manera independiente
	public function getPostIndie($mode, $userid, $postid)
	{
		if($mode == "publication")
		{
			include 'link.php';
            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
            $id = mysqli_real_escape_string($con, $id);
            $limit = mysqli_real_escape_string($con, $limit);
            $result = mysqli_query($con,"SELECT * FROM profile_publications WHERE PARA_ID='".$userid."' AND ID='".$postid."' "); 
            while($row = mysqli_fetch_array($result))
            {
            	$result2 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['DE_ID']."'");
            	$row2 = mysqli_fetch_array($result2);
            	echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="../../../'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'"class="post-inside-image" /></a>
                                    <a href="../../../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                                    <p class="pull-right"><a href="'.$row['ID'].'.php" >'.$row['FECHA'].'</a></p>
                                </div>
                                <div class="panel-body">
                                    <p>'.$row['CONTENT'].'</p>
                                </div>
                                <div class="panel-footer">';
                                    if (isset($_SESSION['username'])) 
                           			{
                                        $resultS = mysqli_query($con,"SELECT ID FROM sandbox_publications WHERE PUBLICATION_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                        $totalS = mysqli_num_rows($resultS);                                       

                                        if($totalS > 0)
                                        {
                                        	echo '<button type="button" class="btn btn-danger" onclick="deletePublication('.$row['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                        }else
                                        {
                                        	echo '<button type="button" class="btn btn-primary" onclick="addPublication('.$row['ID'].', '.$_SESSION['user_id'].', '.$row['DE_ID'].', '.$row['PARA_ID'].', 0, this)" >Sandbox</button>';
                                        }
                           			}else
                           			{
                           				echo '<a href="../signin.php" class="btn btn-primary">Login</a>';
                           			}
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';              

            }    
            mysqli_close($con);
		}elseif($mode == 'notes')
		{
			include 'link.php';
            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar");
            $id = mysqli_real_escape_string($con, $id);
            $limit = mysqli_real_escape_string($con, $limit);
            $result = mysqli_query($con,"SELECT * FROM profile_notes WHERE ID='".$postid."' "); 

            while($row = mysqli_fetch_array($result))
            {
            	$result2 = mysqli_query($con,"SELECT * FROM profiles WHERE ID='".$row['FROM_ID']."' ");
            	$row2 = mysqli_fetch_array($result2);
            	echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading">
                                    <a href="../../../'.$row2['USERNAME'].'"><img src="'.$row2['AVATAR'].'"class="post-inside-image" /></a>
                                    <a href="../../../'.$row2['USERNAME'].'">'.$row2['FIRSTNAME'].'</a>
                                    <p class="pull-right"><a href="'.$row['ID'].'.php" >'.$row['FECHA'].'</a></p>
                                </div>
                                <div class="panel-body">
                                    <p class="'.$row['FONT'].'" >'.$row['NOTE'].'</p>
                                </div>
                                <div class="panel-footer">';
                                    if(isset($_SESSION['username']))
                                    {
                                    	$resultS = mysqli_query($con,"SELECT ID FROM sandbox_notes WHERE NOTE_ID='".$row['ID']."' AND WHO_ADD='".$_SESSION['user_id']."' AND MODE='profile'");
                                        $totalS = mysqli_num_rows($resultS);                                    

                                        if($totalS > 0)
                                        {
                                        	echo '<button type="button" class="btn btn-danger" onclick="noteDeleteSandbox('.$row['ID'].','.$_SESSION['user_id'].',0,this)" >Unsandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnDelNotes"  >Eliminar del Sandbox</button></div>';
                                        }else
                                        {
                                        	echo '<button type="button" class="btn btn-primary" onclick="noteSandbox('.$row['ID'].','.$_SESSION['user_id'].', '.$row['FROM_ID'].', '.$row['TO_ID'].', 0, this)" >Sandbox</button>';
                                            //echo '<div id="noteSandbox"><button id="btnSandNotes" >Agregar al Sandbox</button></div>';
                                        }
                                    }else
                           			{
                           				echo '<a href="../../signin.php" class="btn btn-primary">Login</a>';
                           			}
                echo '              <!-- Boton dropdown-->
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-p">Reportar</a></li>
                                            <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';              

            }    
            mysqli_close($con);
		}

	}

	// public function reportArea($user_id)
	// {
	// 	echo '<script>
	// 	$(document).ready(function(){
 //    		$("#btnRep").click(function(){
 //    			$.post("../sendreport.php",
 //    			{
 //    				user_id: "'.$user_id.'"
 //    			},
 //    			function(status){
 //    				alert("Mensaje: Insertado con Exito"+"\nStatus:"+status);
 //    			});
 //    		});
 //    	});
	// 	</script>';
	// }

	/*public function setRanking($id)//Recibe el id de usuario al que se le fijara el ranking
	{
		$this->ranking = $id;
	}*/

}//Fin de la clase

?>