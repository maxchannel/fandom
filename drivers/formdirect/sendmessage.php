<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Guardar Publicacion en Perfil</title>
    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />
    <meta name="description" content="Invitar Amigos a unirse a Fan Plus Plus por medio de correo electrónico."/>
    <link href="../../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes personalizadas -->
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "../../static/analyticstracking.php" ?>
    <?php include '../../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php
                        function test_input($data) 
                        {
                           $data = trim($data);
                           $data = stripslashes($data);
                           $data = htmlspecialchars($data);
                           return $data;
                        }

                        //ERRORES EN EL FORMULARIO
                        $contentErr="";
                        if($_SERVER["REQUEST_METHOD"] == "POST")
                        {
                            if(empty($_POST["content"])) 
                            {
                                $contentErr = "Escribe algo en el mensaje";
                            }else
                            {
                                $content = test_input($_POST["content"]);
                            }
                        }
                        //Mostrando errores
                        if(isset($contentErr) && ($contentErr!="")) 
                        {
                            echo '<div class="alert alert-danger">
                                    <p>'.$contentErr.'</p>
                                </div>';
                        }

                        //Se manda mensaje
                        if(isset($content) && isset($_SESSION['user_id']) && is_numeric($_SESSION['user_id']))
                        {
                            require '../../link3.php';
                            $con = mysqli_connect($linked['host'],$linked['user'],$linked['pw'],$linked['db']);

                            $for_id = mysqli_real_escape_string($con, $_POST['for_id']);
                            $from_id = mysqli_real_escape_string($con, $_SESSION['user_id']);
                            $content = mysqli_real_escape_string($con, $_POST['content']);
                            $fecha = date("d/m/Y h:i");

                            //Viendo si hay conversacion previa a este mensaje
                            $sql = "SELECT ID FROM conversation WHERE USER1_ID='".$for_id."' AND USER2_ID='".$from_id."' OR USER1_ID='".$from_id."' AND USER2_ID='".$for_id."' ";
                            $result = mysqli_query($con,$sql);
                            $numero_conversaciones = mysqli_num_rows($result);

                            if($numero_conversaciones == 1)//Si ya hay conversacion hecha se mandan los mensajes nada mas
                            {
                                $row = mysqli_fetch_array($result);
                                $sql = "INSERT INTO messages(CONTENT,USER_ID,CONVERSATION_ID,FECHA) VALUES('".$content."','".$from_id."','".$row['ID']."','".$fecha."') ";
                                //Actualizando la ultima ves que se modifico la conversacion
                                $sql2 = "UPDATE conversation SET LAST_MODI='".$fecha."',PIV='".$for_id."',VISTO='0' WHERE ID='".$row['ID']."' ";

                                if(mysqli_query($con,$sql) && mysqli_query($con,$sql2))
                                {
                                    echo '<div class="alert alert-success">
                                            <p>Mensaje Enviado</p>
                                        </div>';
                                }else
                                {
                                    echo mysqli_error($con);
                                }
                            }elseif($numero_conversaciones == 0)//Sino hay conversacion previa se crea y se mandan los mensajes
                            {
                                $sql = "INSERT INTO conversation(USER1_ID,WANT_SEE1,USER2_ID,WANT_SEE2,VISTO,PIV,LAST_MODI) VALUES('".$for_id."','1','".$from_id."','1','0','".$for_id."','".$fecha."')";
                                
                                if(mysqli_query($con,$sql))
                                {
                                    //Creando primero la conversacion para que se genere id
                                    $sql2 = "INSERT INTO messages(CONTENT,USER_ID,CONVERSATION_ID,FECHA) VALUES('".$content."','".$from_id."','".mysqli_insert_id($con)."','".$fecha."') ";
                                    if(mysqli_query($con,$sql2))
                                    {
                                        echo '<div class="alert alert-success">
                                            <p>Mensaje Enviado</p>
                                        </div>';
                                    }
                                }
                            }

                        }           

                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-8">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../static/js/bootstrap.min.js"></script>
</body>
</html>