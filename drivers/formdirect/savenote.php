<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Guardar Publicacion en Perfil</title>
    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />
    <meta name="description" content="Invitar Amigos a unirse a Fan Plus Plus por medio de correo electrónico."/>
    <link href="../../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes personalizadas -->
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "../../static/analyticstracking.php" ?>
    <?php include '../../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
                <?php

                //Recibia de fandom pero aun recibe de profiles
                if($_POST['mode'] == "profile")
                {

                    //VALIDACION DEL FOMULARIO E IMPRESION DE ERRORES
                    $noteErr=$fontErr="";    

                    //Validacion
                    if ($_SERVER["REQUEST_METHOD"] == "POST")
                    {
                        function test_input($data)
                        {
                            $data = trim($data);
                            $data = stripslashes($data);
                            $data = htmlspecialchars($data);
                            return $data;
                        }        

                        //Validando nota
                        if (empty($_POST['note']))
                        {
                           $noteErr = "Las publicación no puede estar vacía.";
                        }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['note']))//Caracteres permitidos
                        {
                            $noteErr = "Publicación: solo se permiten Letras, Números y Espacios en Blanco";
                        }elseif(strlen($_POST['note']) > 160)//Longitud mínima de 5 Caracteres
                        {
                            $noteErr = "Publicación: escribe 160 caracteres o menos";
                        }else
                        {
                            $note = test_input($_POST['note']);
                        }       

                        if(empty($_POST["font"]))
                        {
                           $fontErr = "Selecciona la fuente de tu nota";
                        }else
                        {
                            $font = test_input($_POST["font"]);
                        }       

                    }    

                    //Mostrando errores
                    if(isset($noteErr) && ($noteErr!="")) 
                    {
                        $volver = test_input($_POST['profile2']);
                        echo '<div class="alert alert-danger">
                                <p>'.$noteErr.'</p>
                                <a href="/'.$volver.'/#pn-sec" class="btn btn-default" >Volver</a>
                            </div>';
                    }
                    if(isset($fontErr) && ($fontErr!="")) 
                    {
                        $volver = test_input($_POST['profile2']);
                        echo '<div class="alert alert-danger fade in" role="alert">
                                <p>'.$fontErr.'</p>
                                <a href="/'.$volver.'/#pn-sec" class="btn btn-default" >Volver</a>
                            </div>';
                    }    

                    //Creando post
                    if(isset($note) && isset($font) && isset($_SESSION['username']))
                    {
                        require '../../link.php';
                        $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                        $register = date("Y-m-d");
                        $note = mysqli_real_escape_string($con, $_POST['note']);
                        $font = mysqli_real_escape_string($con, $_POST['font']);
                        $profile_id = mysqli_real_escape_string($con, $_SESSION['user_id']); 
                        $to_id = mysqli_real_escape_string($con, $_POST['to_id']);                        

                        $sql = "INSERT INTO profile_notes(NOTE,TO_ID,FROM_ID,REGISTER,FONT) VALUES('".$note."','".$to_id."','".$profile_id."','".$register."','".$font."')";                            

                        if(mysqli_query($con,$sql))
                        {
                            $sql2 = "SELECT ID FROM profile_notes WHERE NOTE='".$note."' AND FROM_ID='".$profile_id."' AND TO_ID='".$to_id."' AND FONT='".$font."'";
                            $result = mysqli_query($con,$sql2);
                            $row = mysqli_fetch_array($result);
                            
                            $result2 = mysqli_query($con,"SELECT ID,USERNAME FROM profiles WHERE ID='".$to_id."'");
                            $row2 = mysqli_fetch_array($result2);

                            //Insertando valor para las notificaciones
                            $date = date("d/m/Y h:i");
                            $sql = "INSERT INTO notifications_profile(CODE,VIEW,USER_ID,WHO_ID,POST_ID,FECHA) VALUES('3','0','".$to_id."','".$profile_id."','".$row['ID']."','".$date."') ";
                            mysqli_query($con,$sql); 

                            if($fi = fopen("/home/django83/public_html/".$row2['USERNAME']."/notes/".$row['ID'].".php","w"))
                            {
                                fwrite($fi, "<?php"."\n");
                                fwrite($fi, "require '/home/django83/public_html/drivers/profilesgenerator/notesIndie.php';"."\n");
                                fwrite($fi, '$nota = new ProfileNotesIndie('.$row2['ID'].','.$row2['USERNAME'].','.$row['ID'].');'."\n");
                                fwrite($fi, '$nota->generateNotesIndie();'."\n");
                                fwrite($fi, "?>"."\n");
                                fclose($fi);
                                echo '<div class="alert alert-success"><p>Publicación creada con éxito.</p></div>';
                                echo '<script> window.location="/'.$row2['USERNAME'].'/#pn-sec"; </script>'; 
                            }else
                            { 
                                echo '<div class="alert alert-danger"><p>Error al guardar Nota.</p></div>';
                            }
                        }else
                        {
                            echo '<div class="alert alert-danger"><p>Error al guardar Nota.</p></div>';
                        }                            

                        mysqli_close($con);
                    }

                }                    

                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-8">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../static/js/bootstrap.min.js"></script>
</body>
</html>