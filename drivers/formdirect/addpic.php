<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invitar Amigos a unirse a Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />
    <meta name="description" content="Invitar Amigos a unirse a Fan Plus Plus por medio de correo electrónico."/>
    <link href="../../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes personalizadas -->
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "../../static/analyticstracking.php" ?>
    <?php include '../../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php
                        //FORMULARIO que viene de los fandoms
                        if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name']) && isset($_POST['photo_name']) && !empty($_POST['photo_name']) &&
                            isset($_POST['fandom_id']) && !empty($_POST['fandom_id']))
                        {  
                            include '../../link.php';
                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                            $user_id = mysqli_real_escape_string($con, $_SESSION['user_id']);
                            $fandom_id = mysqli_real_escape_string($con, $_POST['fandom_id']);
                            $fecha = date("Y-m-d");
                            $photo_name = mysqli_real_escape_string($con, $_POST['photo_name']);
                            // $album_id = uniqid(); no se usa se elimino esta columna de la base de datos
                            $name = $_FILES['foto']['name'];
                            $temp = explode(".", $name);
                            $ext = end($temp);
                            $photo_server = md5($name).".".$ext;
                            $photo_server2 = "fandomimages/".$photo_server;              

                            $sql = "INSERT INTO fandom_images(FANDOM_ID,USER_ID,PHOTO_NAME,FECHA,PHOTO_SERVER) VALUES('".$fandom_id."','".$user_id."','".$photo_name."','".$fecha."','".$photo_server2."')";                

                            if($_FILES['foto']['type'] == "image/gif" || $_FILES['foto']['type'] == "image/jpg" || $_FILES['foto']['type'] == "image/png" ||
                               $_FILES['foto']['type'] == "image/jpeg" && $_FILES['foto']['error'] == 0)
                            {
                                if(move_uploaded_file($_FILES['foto']['tmp_name'], "../../fandomimages/".$photo_server) && mysqli_query($con,$sql))
                                {

                                    //Aqui enviar al media.php para visualizar la foto
                                    echo '<div class="alert alert-success"><p>Imágen Subida Con Éxito</p></div>';
                                    echo '<script> window.location="/'.$_POST['fandom_url'].'/images/"; </script>';
                                }else
                                {
                                    echo '<div class="alert alert-danger"><p>ERROR, contactanos: @fanplusplus_es</p></div>';
                                }
                            }else
                            {
                                echo '<div class="alert alert-danger"><p>Por favor selecciona una imágen.</p></div>';
                            }            

                            mysqli_close($con);
                        }else
                        {
                            echo '<div class="alert alert-danger"><p>Debes seleccionar una Imágen y escribir su descripción.</p></div>';
                            //echo '<a href="#">Subir más de 1 Foto</a>'; Disponible despues como promo
                        }                

                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-8">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../static/js/bootstrap.min.js"></script>
</body>
</html>