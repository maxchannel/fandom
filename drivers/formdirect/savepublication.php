<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Guardar Publicacion en Perfil</title>
    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />
    <meta name="description" content="Invitar Amigos a unirse a Fan Plus Plus por medio de correo electrónico."/>
    <link href="../../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes personalizadas -->
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
</head>
<body>
	<?php include "../../static/analyticstracking.php" ?>
    <?php include '../../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
            	<?php
            	//FORMULARIO de los Perfiles
                $messageErr="";

                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                } 

                if($_SERVER["REQUEST_METHOD"] == "POST")
                {           
                    //Validando el primer campo: firstname
                    if (empty($_POST['message']))
                    {
                       $messageErr = "Las publicación no puedes estar vacía.";
                    }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['message']))//Caracteres permitidos
                    {
                        $messageErr = "Publicación: solo se permiten Letras, Números y Espacios en Blanco";
                    }elseif(strlen($_POST['message']) > 160)//Longitud mínima de 5 Caracteres
                    {
                        $messageErr = "Publicación: escribe 160 caracteres o menos";
                    }else
                    {
                        $message = test_input($_POST['message']);
                    }            

                }        

                //Mostrando errores
                if(isset($messageErr) && ($messageErr!="")) 
                {
                    $volver = test_input($_POST['profile']);
                    echo '<div class="alert alert-danger">
                            <p>'.$messageErr.'</p>
                            <a href="/'.$volver.'/#pn-sec" class="btn btn-default" >Volver</a>
                        </div>';
                }

                //Creando post
                if(isset($message) && isset($_SESSION['username']))
                {
                    function toLink($text)
                    {
                        $text = html_entity_decode($text);
                        $text = " ".$text;
                        $text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                        '<a href="\1" target="_blank" >\1</a>', $text);
                        $text = eregi_replace('(((f|ht){1}tps://)[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                        '<a href="\1" target="_blank" >\1</a>', $text);
                        $text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                        '\1<a href="http://\2" target="_blank">\2</a>', $text);
                        $text = eregi_replace('([_.0-9a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,3})',
                        '<a href="mailto:\1" target="_blank">\1</a>', $text);
                        return $text;
                    } 

                    include '../../link.php';
                    $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                    $message = mysqli_real_escape_string($con, $_POST['message']);
                    $de = mysqli_real_escape_string($con, $_SESSION['user_id']);
                    $fecha = date("Y-m-d");
                    $para_id = mysqli_real_escape_string($con, $_POST['para_id']);              

                    if(mysqli_query($con,"INSERT INTO profile_publications(DE_ID,PARA_ID,CONTENT,FECHA) VALUES('$de','$para_id','".tolink($message)."','$fecha')"))
                    {
                        $sql2 = "SELECT ID FROM profile_publications WHERE CONTENT='".tolink($message)."' AND PARA_ID='".$para_id."' AND DE_ID='".$de."' ";
                        $result = mysqli_query($con,$sql2);
                        $row = mysqli_fetch_array($result);

                        //Insertando valor para las notificaciones
                        $date = date("d/m/Y h:i");
                        $sql = "INSERT INTO notifications_profile(CODE,VIEW,USER_ID,WHO_ID,POST_ID,FECHA) VALUES('2','0','".$para_id."','".$de."','".$row['ID']."','".$date."') ";
                        mysqli_query($con,$sql); 
                        
                        $result2 = mysqli_query($con,"SELECT ID,USERNAME FROM profiles WHERE ID='".$para_id."'");
                        $row2 = mysqli_fetch_array($result2);
                        if($fi = fopen("/home/django83/public_html/".$row2['USERNAME']."/publications/".$row['ID'].".php","w"))
                        {
                            fwrite($fi, "<?php"."\n");
                            fwrite($fi, "require '/home/django83/public_html/drivers/profilesgenerator/publicationsIndie.php';"."\n");
                            fwrite($fi, '$publication = new ProfilePublicationsIndie('.$row2['ID'].','.$row2['USERNAME'].','.$row['ID'].');'."\n");
                            fwrite($fi, '$publication->generatePublicationsIndie();'."\n");
                            fwrite($fi, "?>"."\n");
                            fclose($fi);
                            echo '<div class="alert alert-success"><p>Publicación creada con éxito.</p></div>';
                            echo '<script> window.location="/'.$row2['USERNAME'].'/#pn-sec"; </script>'; 
                        }else
                        { 
                            echo '<div class="alert alert-danger"><p>Problemas al crear Publicación.</p></div>';
                        }
                    }else
                    {
                        echo '<div class="alert alert-danger"><p>Error al guardar Publicación.</p></div>';
                    }            

                    mysqli_close($con);

                }                     

                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-8">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../static/js/bootstrap.min.js"></script>
</body>
</html>