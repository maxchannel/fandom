<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Guardar Publicacion en Perfil</title>
    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />
    <meta name="description" content="Invitar Amigos a unirse a Fan Plus Plus por medio de correo electrónico."/>
    <link href="../../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes personalizadas -->
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "../../static/analyticstracking.php" ?>
    <?php include '../../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php
                        //Recibia informacion de fandom/publications
                        function toLink($text)
                        {
                            $text = html_entity_decode($text);
                            $text = " ".$text;
                            $text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                            '<a href="\1" target="_blank" >\1</a>', $text);
                            $text = eregi_replace('(((f|ht){1}tps://)[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                            '<a href="\1" target="_blank" >\1</a>', $text);
                            $text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_+.~#?&//=]+)',
                            '\1<a href="http://\2" target="_blank">\2</a>', $text);
                            $text = eregi_replace('([_.0-9a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,3})',
                            '<a href="mailto:\1" target="_blank">\1</a>', $text);
                            return $text;
                        }            

                        if (isset($_POST['message']) && !empty($_POST['message'])) 
                        {   
                            include '../../link.php';
                            $con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                            $de = mysqli_real_escape_string($con, $_SESSION['user_id']);
                            $message = mysqli_real_escape_string($con, $_POST['message']);
                            $fecha = date("Y-m-d");
                            $para_id = mysqli_real_escape_string($con, $_POST['fandom_id']);            

                            if(mysqli_query($con,"INSERT INTO fan_publications(PARA_ID,DE_ID,CONTENT,FECHA) VALUES('".$para_id."','$de','".tolink($message)."','$fecha')"))
                            {
                                $sql2 = "SELECT ID FROM fan_publications WHERE CONTENT='".tolink($message)."' AND PARA_ID='".$para_id."' AND DE_ID='".$de."' ";
                                $result = mysqli_query($con,$sql2);
                                $row = mysqli_fetch_array($result);
                                
                                $result2 = mysqli_query($con,"SELECT ID,FANDOM,MODE,URL FROM fandoms WHERE ID='".$para_id."'");
                                $row2 = mysqli_fetch_array($result2);
                                if($row2['MODE'] == "fandom")
                                {
                                    if($fi = fopen("/home/django83/public_html/".strtolower($row2['FANDOM'])."/publications/".$row['ID'].".php","w"))
                                    {
                                        fwrite($fi, "<?php"."\n");
                                        fwrite($fi, "require '/home/django83/public_html/drivers/fandomsgenerator/publicationsIndie.php';"."\n");               
                                        fwrite($fi, '$publication = new fandomPublicationsIndie('.$row2['ID'].',"fandom",'.$row['ID'].');'."\n");
                                        fwrite($fi, '$publication->generatePublicationsIndie();'."\n");
                                        fwrite($fi, "?>"."\n");
                                        fclose($fi);
                                        echo '<div class="alert alert-success"><p>Publicación guardada :)</p></div>';
                                        echo '<script> window.location="/'.strtolower($row2['FANDOM']).'/publications"; </script>';
                                    }
                                }elseif($row2['MODE'] == "fans")
                                {
                                    if($fi = fopen("/home/django83/public_html/".$row2['URL']."/publications/".$row['ID'].".php","w"))
                                    {
                                        fwrite($fi, "<?php"."\n");
                                        fwrite($fi, "require '/home/django83/public_html/drivers/fandomsgenerator/publicationsIndie.php';"."\n");               
                                        fwrite($fi, '$publication = new fandomPublicationsIndie('.$row2['ID'].',"fandom",'.$row['ID'].');'."\n");
                                        fwrite($fi, '$publication->generatePublicationsIndie();'."\n");
                                        fwrite($fi, "?>"."\n");
                                        fclose($fi);
                                        echo '<div class="alert alert-success"><p>Publicación guardada :)</p></div>';
                                        echo '<script> window.location="/'.$row2['URL'].'/publications"; </script>';
                                    }
                                }
                            
                            }else
                            {
                                echo '<div class="alert alert-danger"><p>ERROR, contactanos: @fanplusplus_es</p></div>';
                            }            

                            mysqli_close($con);
                        }else
                        {
                            echo '<div class="alert alert-danger"><p>La publicación no debe estar vacia.</p></div>';
                        }            

                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-8">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../static/js/bootstrap.min.js"></script>
</body>
</html>