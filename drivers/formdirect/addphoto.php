<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invitar Amigos a unirse a Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="../../static/favicon.ico" />
    <meta name="description" content="Invitar Amigos a unirse a Fan Plus Plus por medio de correo electrónico."/>
    <link href="../../static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="../../static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes personalizadas -->
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
</head>
<body>
	<?php include "../../static/analyticstracking.php" ?>
    <?php include '../../static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                    	<?php
                    	//FORMULARIO que viene de los profiles
                 		if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name']) && isset($_POST['description']) && !empty($_POST['description'])) 
                 		{
                 			include '../../link.php';
                 			$con = mysqli_connect($host,$user,$pw,$db) or die("Problemas al conectar.");
                 			$name = $_POST['description'];//Poner de que php viene esto
                 			$user_id = mysqli_real_escape_string($con, $_SESSION['user_id']);
                 			$fecha = date("Y-m-d");
                 			$temp = explode(".", $name);
                 			$ext = end($temp);
                 			$photo_server = md5($name).".".$ext;
                 			$album_id = uniqid();
                 			$photo_name = mysqli_real_escape_string($con, $_POST['description']);
                 			$photo_server2 = "profilesimages/".$photo_server;                 

                 			$sql = "INSERT INTO profile_photos(USER_ID,PHOTO_NAME,FECHA,PHOTO_SERVER) VALUES('".$user_id."','".$name."','".$fecha."','".$photo_server2."')";                 

                 			if($_FILES['image']['type'] == "image/gif" || $_FILES['image']['type'] == "image/jpg" || $_FILES['image']['type'] == "image/png" || 
                 			   $_FILES['image']['type'] == "image/jpeg" && $_FILES['image']['error'] == 0)
                 			{
                 				if(move_uploaded_file($_FILES['image']['tmp_name'], "../../profilesimages/".$photo_server) && mysqli_query($con,$sql))
                 				{
                 					echo '<div class="alert alert-success"><p>Imágen Subida Con Éxito</p></div>';
                 					echo '<script> window.location="/'.$_SESSION['username'].'/#image-sec"; </script>'; 
                 				}else
                 				{
                 					echo '<div class="alert alert-danger"><p>Problemas al subir Imágen</p></div>';
                 				}
                 			}else
                 			{
                 				echo '<div class="alert alert-warning"><p>El Archivo debe ser una Imágen.</p></div>';
                 			}                 

                 			mysqli_close($con);
                 		}else
                 		{
                 			echo '<div class="alert alert-danger"><p>Debes seleccionar una Imágen y escribir su descripción.</p></div>';
                 		}                 

                 		?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-8">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include '../../static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../static/js/bootstrap.min.js"></script>
</body>
</html>