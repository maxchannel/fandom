<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Mensajes - Fan Plus Plus</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <meta name="description" content="Tus notificaciones en Fan Plus Plus"/>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
</head>
<body>
    <?php include "static/analyticstracking.php" ?>
    <?php include 'static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <?php
            function test_input($data) 
            {
               $data = trim($data);
               $data = stripslashes($data);
               $data = htmlspecialchars($data);
               return $data;
            }

            if(isset($_SESSION['username']) && is_string($_SESSION['username']) && isset($_SESSION['user_id']) && is_numeric($_SESSION['user_id']))
            {
                $username = test_input($_SESSION['username']);
                $user_id = test_input($_SESSION['user_id']);

                require 'link3.php';
                $con = mysqli_connect($linked['host'],$linked['user'],$linked['pw'],$linked['db']);
                //Conectando a db para mostrar perfil
                require 'link.php';
                $con2 = mysqli_connect($host,$user,$pw,$db);

                $result = mysqli_query($con,"SELECT * FROM conversation WHERE USER1_ID='".mysqli_real_escape_string($con,$_SESSION['user_id'])."' AND WANT_SEE1='1' OR USER2_ID='".mysqli_real_escape_string($con,$_SESSION['user_id'])."' AND WANT_SEE2='1' ORDER BY LAST_MODI DESC");     

                //NAVEGACION
                echo '<div class="col-xs-12 col-sm-1 col-md-1 col-lg-3">
                    <div class="list-group">';
                    if(mysqli_num_rows($result) > 0)
                    {
                        while($row = mysqli_fetch_array($result))
                        {
                            if($row['USER1_ID'] != $user_id)
                            {
                                $result2 = mysqli_query($con2,"SELECT FIRSTNAME FROM profiles WHERE ID='".mysqli_real_escape_string($con2,$row['USER1_ID'])."' ");
                                $row2 = mysqli_fetch_array($result2);
                                if(isset($_GET['con']) && $_GET['con']==$row['ID'])
                                {
                                    echo '<a href="m?con='.$row['ID'].'" class="list-group-item active">'.$row2['FIRSTNAME'].'</a>';
                                }else
                                {
                                    echo '<a href="m?con='.$row['ID'].'" class="list-group-item">'.$row2['FIRSTNAME'].'</a>';
                                }
                            }elseif($row['USER2_ID'] != $user_id)
                            {
                                $result2 = mysqli_query($con2,"SELECT FIRSTNAME FROM profiles WHERE ID='".mysqli_real_escape_string($con2,$row['USER2_ID'])."' ");
                                $row2 = mysqli_fetch_array($result2);
                                if(isset($_GET['con']) && $_GET['con']==$row['ID'])
                                {
                                    echo '<a href="m?con='.$row['ID'].'" class="list-group-item active">'.$row2['FIRSTNAME'].'</a>';
                                }else
                                {
                                    echo '<a href="m?con='.$row['ID'].'" class="list-group-item">'.$row2['FIRSTNAME'].'</a>';
                                }
                            }
                        }
                    }
                echo '  </div>
                    </div>';

                //MENSAJES
                echo '<div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">';

                    if(mysqli_num_rows($result) > 0)
                    {
                        if(isset($_GET['con']) && is_numeric($_GET['con']))
                        {
                            $conversationID = test_input($_GET['con']);
                            //Si hay algo en NO VISTO y el pivote le toque al actual USER se actualiza
                            mysqli_query($con,"UPDATE conversation SET VISTO='1' WHERE VISTO='0' AND PIV='".$user_id."' AND ID='".$conversationID."' ");

                            echo '<div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Conversación con Jared Leto</h3>
                            </div>
                            <div class="panel-body">';

                            //Actualizando todos los estados no visto cuando el usuario entre aquí
                            mysqli_query($con,"UPDATE messages SET VISTO=1 WHERE VISTO='0' AND CONVERSATION_ID='".mysqli_real_escape_string($con,$row['ID'])."' ");

                            $result3 = mysqli_query($con,"SELECT * FROM messages WHERE CONVERSATION_ID='".mysqli_real_escape_string($con,$conversationID)."' "); 
                            while($row3 = mysqli_fetch_array($result3))
                            {
                                $result4 = mysqli_query($con2,"SELECT USERNAME,AVATAR,FIRSTNAME FROM profiles WHERE ID='".mysqli_real_escape_string($con2,$row3['USER_ID'])."'");
                                $row4 = mysqli_fetch_array($result4);        

                                echo '<!-- Mensaje 1 -->
                                    <div class="panel panel-default post-panel">
                                        <div class="panel-heading">
                                            <a href="../'.strtolower($row4['USERNAME']).'"><img src="../'.$row4['AVATAR'].'" class="post-inside-image" /></a>
                                            <a href="../'.strtolower($row4['USERNAME']).'">'.$row4['FIRSTNAME'].'</a>
                                            <p class="pull-right">'.$row3['FECHA'].'</p>
                                        </div>
                                        <div class="panel-body">
                                            <p>'.$row3['CONTENT'].'</p>
                                        </div>
                                        <div class="panel-footer">
                                            <button type="button" class="btn btn-danger" onclick="deleteMessage('.$row['ID'].',this)" >Eliminar</button>
                                            <!-- Boton dropdown-->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-report-msn">Reportar</a></li>
                                                    <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm-err">Informar de Error</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>';    

                            }
                            //ERRORES EN EL FORMULARIO
                            $contentErr="";
                            if($_SERVER["REQUEST_METHOD"] == "POST")
                            {
                                if(empty($_POST["content"])) 
                                {
                                    $contentErr = "Escribe algo en el mensaje";
                                }else
                                {
                                    $content = test_input($_POST["content"]);
                                }
                            }
                            //Mostrando errores
                            if(isset($contentErr) && ($contentErr!="")) 
                            {
                                echo '<div class="alert alert-danger fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <p>'.$contentErr.'</p>
                                    </div>';
                            }

                            //Se manda mensaje
                            if(isset($content))
                            {
                                $resultP = mysqli_query($con,"SELECT USER1_ID,USER2_ID FROM conversation WHERE USER1_ID='".mysqli_real_escape_string($con,$_SESSION['user_id'])."' AND WANT_SEE1='1' OR USER2_ID='".mysqli_real_escape_string($con,$_SESSION['user_id'])."' AND WANT_SEE2='1' ORDER BY LAST_MODI DESC");
                                //Creando pivote
                                $rowP = mysqli_fetch_array($resultP);
                                if($rowP['USER1_ID'] != $user_id)
                                {
                                    $pivote = $rowP['USER1_ID'];
                                }elseif($rowP['USER2_ID'] != $user_id)
                                {
                                    $pivote = $rowP['USER2_ID'];
                                }

                                $fecha = date("d/m/Y h:i");
                                $sql = "INSERT INTO messages(CONTENT,USER_ID,CONVERSATION_ID,FECHA) 
                                    VALUES('".mysqli_real_escape_string($con,$content)."','".mysqli_real_escape_string($con,$user_id)."','".$conversationID."', '".$fecha."') ";
                                $sql2 = "UPDATE conversation SET LAST_MODI='".$fecha."',PIV='".$pivote."',VISTO='0' WHERE ID='".$conversationID."' ";

                                if(mysqli_query($con,$sql) && mysqli_query($con,$sql2))
                                {
                                    echo '<div class="alert alert-success"><p>Mensaje Enviado</p></div>';
                                    echo '<script> window.location="/m?con='.$conversationID.'"; </script>'; 
                                }

                            }

                            //RESPONDER
                            echo '  <form role="form" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'?con='.$conversationID.'" method="post">
                                        <div class="form-group">
                                            <textarea name="content" class="form-control" rows="3"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default">Enviar</button>
                                    </form>';
                            echo '</div>
                                </div>';
                        }else
                        {
                            echo '<div class="alert alert-warning" role="alert">
                                    <p>Contenido No Disponible</p>
                                </div>';
                        }
                    }else
                    {
                        echo '<div class="panel panel-default">
                                <div class="panel-body">
                                    <p>Sin mensajes</p>
                                </div>
                            </div>';
                    }

                echo '</div>';
            }else
            {
                echo '<div class="col-xs-12 col-sm-1 col-md-1 col-lg-3">
                    </div>';

                echo '<div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
                        <div class="alert alert-warning" role="alert">
                            <p>Contenido No Disponible</p>
                        </div>
                    </div>';
            }


            ?>

            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ads</h3>
                    </div>
                    <div class="panel-body">
                        <p>Hola mundo</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <script src="static/js/fluid.js"></script>
</body>
</html>