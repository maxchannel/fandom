<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Logout - Fan Plus Plus</title>
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <meta name="description" content="Fan Plus Plus - Logout"/>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="static/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Fuentes personalizadas -->
    <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "static/analyticstracking.php" ?>
    <?php include 'static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
                <?php
                //Solo si existe la sesion se puede destruir
                if(isset($_SESSION['username']) && isset($_SESSION['user_id']))
                {
                    //Eliminando datos 
                    /*include "link.php";
                    $con = mysqli_connect($host,$user,$pw,$db);
                    $result = mysqli_query($con,"SELECT ID FROM profiles WHERE USERNAME='".$_SESSION['username']."' AND ID='".$_SESSION['user_id']."'");
                    $row = mysqli_fetch_array($result);
                    if(mysqli_query($con,"DELETE FROM control_ip WHERE USERNAME_ID='".$row['ID']."'"))
                    {
                        echo "Se elimino de la tabla los datos"."</br>";
                    }else
                    {
                        echo "No se elimino nada de control_ip"."</br>";
                    }*/

                    //Cerrando sesion
                    if(session_destroy())
                    {
                        echo '<div class="alert alert-success"><p>Saliendo del Fan Plus Plus</p></div>';
                        echo '<script> window.location="/"; </script>';
                    }else
                    {
                        echo "Problemas al cerrar sesión."."</br>";
                    }    

                }else
                { 
                    echo '<div class="alert alert-info" role="alert">';
                    echo '<p>Debes <a href="signin.php">Iniciar Sesión</a> o <a href="signup.php">Registrate</a> en F++</p>';
                    echo '</div>';
                }
                        
                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-2 col-lg-8">
            </div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <!-- Inicio Footer -->
    <?php include 'static/footer.php'; ?>
    <!-- Fin Footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
</body>
</html>