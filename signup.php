<?php 
session_start(); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registro en F++</title>
    <link rel="shortcut icon" type="image/x-icon" href="static/favicon.ico" />
    <meta name="description" content="Formulario de registro para unirse a Fan Plus Plus. 2"/>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="static/css/estilos.css">
    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <link href="static/css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body>
    <?php include "static/analyticstracking.php" ?>
    <?php include 'static/navs/nav.php'; ?>

    <!-- Inicio de contenido -->
    <div class="container-fluid">
        <section class="row">
            <?php
            //Codigo para validar todas las entradas
            //Definiendo valores vacios
            //$firstname = $username = $email = $pw = $country = $birthday = $birthmonth = $birthyear = $sex = "";
            $nameErr=$emailErr=$pwErr=$userErr;

            if ($_SERVER["REQUEST_METHOD"] == "POST")
            {
                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }

                //Validando el primer campo: firstname
                if (empty($_POST["firstname"]))
                {
                   $nameErr = "Nombre es un Campo Obligatorio";
                }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST["firstname"]))//Caracteres permitidos
                {
                    $nameErr = "Nombre: solo se permiten Letras, Números y Espacios en Blanco";
                }elseif(strlen($_POST["firstname"]) > 30)//Longitud mínima de 5 Caracteres
                {
                    $nameErr = "Nombre: escribe 30 caracteres o menos";
                }else
                {
                    $firstname = test_input($_POST["firstname"]);
                }

                //Validando: email
                if(empty($_POST["email"]))
                {
                    $emailErr = "Email es un Campo Obligatorio";
                }elseif(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$_POST["email"]))
                {
                    $emailErr = "Email no Válido";
                }else
                {
                    //Revisando si ya existe en la base de datos ese email
                    //include 'link.php';
                    $con = mysqli_connect('localhost','django83_90js','supermassive_(38)','django83_fpp');
                    $totalEmail = mysqli_num_rows(mysqli_query($con,"SELECT ID FROM profiles WHERE EMAIL='".mysqli_real_escape_string($con,$_POST["email"])."'"));
                    if($totalEmail == 1)
                    {
                        $emailErr = "Email ya existente";
                    }elseif($totalEmail == 0)
                    {
                        $email = test_input($_POST["email"]);
                    }
                    mysqli_close($con);                
                }

                //Validando el password
                if(empty($_POST['pw2']))
                {
                    $pwErr = "Contraseña es un Campo Obligatorio";
                }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['pw2']))
                {
                    $pwErr = "Contraseña: Solo puede contener a-z, A-Z, 0-9 y Espacios en Blanco";
                }elseif(strlen($_POST['pw2']) < 6)
                {
                    $pwErr = "Contraseña: debe contener mínimo 6 caracteres";
                }else
                {
                    $pw2 = test_input($_POST['pw2']);
                }      

                //Validando el segundo campo: username
                if(empty($_POST['username']))
                {
                    $userErr = "Username es un Campo Obligatorio";
                }elseif(!preg_match("/^[a-zA-Z0-9_]*$/", $_POST["username"]))
                {
                    $userErr = "Username: se permiten los siguientes caracteres: a-z, A-Z, 0-9 y _";
                }elseif(strlen($_POST["username"]) > 30)
                {
                    $userErr = "Username: Escribe 30 caracteres o menos";
                }else
                {
                    //Revisando que el username no existe ya en la db
                    $con = mysqli_connect('localhost','django83_90js','supermassive_(38)','django83_fpp');
                    $totalUsers = mysqli_num_rows(mysqli_query($con,"SELECT ID FROM profiles WHERE USERNAME='".mysqli_real_escape_string($con,$_POST["username"])."'"));
                    $totalFandoms = mysqli_num_rows(mysqli_query($con,"SELECT ID FROM fandoms WHERE FANDOM='".mysqli_real_escape_string($con,$_POST["username"])."'"));
                    if($totalUsers == 1)
                    {
                        $userErr = "Este nombre de usuario ya fue tomado, elige uno nuevo.";
                    }elseif($totalFandoms == 1)
                    {
                        $userErr = "Este nombre de usuario ya fue tomado, elige uno nuevo.";
                    }elseif($totalUsers == 0)
                    {
                        $username = test_input($_POST["username"]);
                    }
                    mysqli_close($con);
                }

                if(empty($_POST['terms']))
                {
                    $termsErr = "Debes Aceptar los Términos y Condiciones";
                }elseif($_POST['terms'] == "acepted")
                {
                    $terms = "acepted";
                }

            }            

            ?>
            <div class="col-xs-12 col-sm-1 col-md-3 col-lg-3"></div>
            <div class="col-xs-12 col-sm-10 col-md-6 col-lg-6">
                <h1>Regístrate en Fan Plus Plus</h1>
                <?php
                //Mostrando errores
                if(isset($nameErr) && ($nameErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$nameErr.'</p>
                        </div>';
                }
                if(isset($emailErr) && ($emailErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$emailErr.'</p>
                        </div>';
                }
                if(isset($pwErr) && ($pwErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$pwErr.'</p>
                        </div>';
                }
                if(isset($userErr) && ($userErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$userErr.'</p>
                        </div>';
                }
                if(isset($termsErr) && ($termsErr!="")) 
                {
                    echo '<div class="alert alert-danger fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p>'.$termsErr.'</p>
                        </div>';
                }
                ?>
                <?php
                //Si no hay mensajes de error se manda al server
                if(isset($firstname) && isset($email) && isset($pw2) && isset($username) && isset($terms) && !isset($_SESSION['username'])) 
                {
                    $fecha = date("d/m/Y");

                    //include 'link.php';
                    $conn = new PDO('mysql:host=localhost;dbname=django83_fpp','django83_90js','supermassive_(38)'); 

                    $sql = "INSERT INTO profiles(FIRSTNAME,EMAIL,USERNAME,REGISTER_DATE) VALUES(:firstname, :email, :username, :fecha)";
                    $sentencia = $conn->prepare($sql);
                    //Preparacion de parametros para la db
                    $sentencia->bindParam(':firstname', $firstname);  
                    $sentencia->bindParam(':username', $username); 
                    $sentencia->bindParam(':email', $email);     
                    $sentencia->bindParam(':fecha', $fecha);

                    //Insertando datos en la db
                    if($sentencia->execute()) 
                    {
                        //Consulta para insertar la contraseña en una db diferente
                        $user_id = $conn->lastInsertId();
                        $pw3 = md5($pw2);
                        $connPW = new PDO('mysql:host=localhost;dbname=django83_jal3001','django83_3dmx','smuse_(6)'); 
                        $sentenciaPW = $connPW->prepare("INSERT INTO pws_rellic(PW,USER_ID) VALUES(:pass, :user_id)");
                        $sentenciaPW->bindParam(':pass', $pw3);  
                        $sentenciaPW->bindParam(':user_id', $user_id); 
                        //Ejecutando sentencia
                        $sentenciaPW->execute();

                        //Creando la carpeta en el server con el nombre del usuario 
                        if(mkdir(strtolower($username))) 
                        {
                            //Creando el index.php
                            $fi = fopen($username."/index.php", "a") or die("Problemas al crear archivo.");
                            fwrite($fi, "<?php"."\n");
                            fwrite($fi, "include '../drivers/profilesgenerator/profile.php';"."\n");
                            fwrite($fi, '$generic = new Profile('.$user_id.',"'.$username.'");'."\n");
                            fwrite($fi, '$generic->generateProfile();'."\n");
                            fwrite($fi, "?>");
                            fclose($fi);

                            //Carpeta fotos
                            if(mkdir("/home/django83/public_html/".$username."/images"))
                            {
                                //Creando el index.php
                                $fi = fopen("/home/django83/public_html/".$username."/images/index.php", "a") or die("Problemas al crear archivo.");
                                fwrite($fi, "<?php"."\n");
                                fwrite($fi, "require '../../drivers/profilesgenerator/images.php';"."\n");
                                fwrite($fi, '$generic = new ProfilePhotos('.$user_id.',"'.$username.'");'."\n");
                                fwrite($fi, '$generic->generatePhotos(20);'."\n");
                                fwrite($fi, "?>"."\n");
                                fclose($fi); 
                            }

                            //Carpeta publicaciones
                            if(mkdir("/home/django83/public_html/".$username."/publications"))
                            {
                                //Creando el index.php
                                $fi = fopen("/home/django83/public_html/".$username."/publications/index.php", "a") or die("Problemas al crear archivo.");
                                fwrite($fi, "<?php"."\n");
                                fwrite($fi, "require '/home/django83/public_html/drivers/profilesgenerator/publications.php';"."\n");
                                fwrite($fi, '$generic  = new ProfilePublications('.$user_id.',"'.$username.'");'."\n");
                                fwrite($fi, '$generic->generatePublications();'."\n");
                                fwrite($fi, "?>"."\n");
                                fclose($fi); 
                            }

                            //Carpeta notas
                            if(mkdir("/home/django83/public_html/".$username."/notes"))
                            {
                                //Creando el index.php
                                $fi = fopen("/home/django83/public_html/".$username."/notes/index.php", "a") or die("Problemas al crear archivo.");
                                fwrite($fi, "<?php"."\n");
                                fwrite($fi, "require '/home/django83/public_html/drivers/profilesgenerator/notes.php';"."\n");
                                fwrite($fi, '$generic = new ProfileNotes('.$user_id.',"'.$username.'");'."\n");
                                fwrite($fi, '$generic->generateNotes();'."\n");
                                fwrite($fi, "?>"."\n");
                                fclose($fi); 
                            }

                            //Carpeta seguidores
                            if(mkdir("/home/django83/public_html/".$username."/followers"))
                            {
                                //Creando el index.php
                                $fi = fopen("/home/django83/public_html/".$username."/followers/index.php", "a") or die("Problemas al crear archivo.");
                                fwrite($fi, "<?php"."\n");
                                fwrite($fi, "require '../../drivers/profilesgenerator/followers.php';"."\n");
                                fwrite($fi, '$generic = new ProfileFollowers('.$user_id.',"'.$username.'");'."\n");
                                fwrite($fi, '$generic->generateFollowers();'."\n");
                                fwrite($fi, "?>"."\n");
                                fclose($fi); 
                            }

                            //Carpeta fandoms
                            if(mkdir("/home/django83/public_html/".$username."/fandoms"))
                            {
                                //Creando el index.php
                                $fi = fopen("/home/django83/public_html/".$username."/fandoms/index.php", "a") or die("Problemas al crear archivo.");
                                fwrite($fi, "<?php"."\n");
                                fwrite($fi, "require '../../drivers/profilesgenerator/fandoms.php';"."\n");
                                fwrite($fi, '$generic = new ProfileFandoms('.$user_id.',"'.$username.'");'."\n");
                                fwrite($fi, '$generic->generateFandoms();'."\n");
                                fwrite($fi, "?>"."\n");
                                fclose($fi); 
                            }

                        }//Fin del if para crear carpeta username    

                        //Funcion para validar email
                        function spamcheck($field)
                        {
                            $field = filter_var($field,FILTER_SANITIZE_EMAIL);
                            if(filter_var($field,FILTER_VALIDATE_EMAIL))
                            {
                                return TRUE;
                            }else
                            {
                                return FALSE;
                            }
                        }    

                        require("link.php");
                        $con = mysqli_connect($host,$user,$pw,$db);

                        //Insertando el codigo confirmacion en la db
                        //Enviando por email el codigo unico a cada usuario
                        $codigo_confirmacion = uniqid(); //Generando codigo
                        $sql2 = "INSERT INTO verify(EMAIL,CODIGO) VALUES(:email, :code) ";
                        $sentencia2 = $conn->prepare($sql2);
                        $sentencia2->bindParam(':email', $email); 
                        $sentencia2->bindParam(':code', $codigo_confirmacion); 

                        if(isset($email))
                        {
                            $mailcheck = spamcheck($email);
                            if($mailcheck==FALSE)
                            {
                                echo '<div class="alert alert-danger">Email no valido.</div>';
                            }else
                            {
                                $subject = "Codigo de Confirmacion en F++";
                                if($sentencia2->execute() && mail($email, $subject, "Tu código de confirmación es: $codigo_confirmacion","From: register@fanplusplus.com"))
                                {
                                    $emailShow = $email;
                                    $firstname=$email=$pw2=$terms="";  

                                    $result = mysqli_query($con, "SELECT ID,FIRSTNAME FROM profiles WHERE USERNAME='".$username."' "); 
                                    $row = mysqli_fetch_array($result);
                                    echo '<div class="alert alert-success">
                                            <p>Registro exitoso! Ahora puedes <a href="/signin.php">iniciar sesión</a></p>
                                        </div>';  

                                    $firstname=$email=$pw2=$username=$terms="";
                                }else
                                {
                                    echo '<div class="alert alert-danger">Error al enviar código de confirmación.</div>';
                                }
                            }
                        }    

                    }

                    $connPW = null;
                    $conn = null;//Cerrando conexion
                    mysqli_close($con);
                } 

                //Formulario solo si no esta logueado
                if(!isset($_SESSION['username']) && !isset($_SESSION['user_id']))
                {
                    echo '<form role="form" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" method="post" />
                        <div class="form-group">
                            <label for="registerName">Nombre completo</label>
                            <input name="firstname" type="text" class="form-control" id="registerName" placeholder="Nombre y apellido" value="'.$firstname.'" />
                        </div>
                        <div class="form-group">
                            <label for="registerEmail">Correo Electrónico</label>
                            <input name="email" type="email" class="form-control" id="registerEmail" placeholder="Introduce un email válido" value="'.$email.'" />
                        </div>
                        <div class="form-group">
                            <label for="registerPass">Contraseña</label>
                            <input name="pw2" value="'.$pw2.'" type="password" class="form-control" id="registerPass" placeholder="Contraseña" />
                        </div>
                        <div class="form-group">
                            <label for="registerUsers">Usuario</label>
                            <input name="username" type="text" class="form-control" id="registerUsers" placeholder="Caracteres permitidos: a-z, A-Z, 0-9 y _" value="'.$username.'" />
                        </div>
                        <div class="checkbox">
                            <label>
                                 <input type="checkbox" name="terms" value="acepted"';
                                 if(isset($terms)&&$terms=="acepted") echo "checked";
                        echo '/> Acepto Términos y Condiciones
                            </label>
                        </div>
                        <button type="submit" class="btn btn-warning black-text">Registrarme</button>
                    </form>';
                }else 
                {
                    echo '<div class="alert alert-info">Ya estas registrado en F++ y has iniciado sesión.</div>';
                }

                ?>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-3 col-lg-3"></div>
        </section>
    </div>
    <!-- Fin de contenido -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
</body>
</html>