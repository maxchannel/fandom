<?php
if(isset($_SESSION['username']))
{
    require "/home/django83/public_html/drivers/columbia.php";
    $nav = new Usuario($_SESSION['user_id']);

    //Verificar si tenemos notificaciones pendiente de revisar
    require '/home/django83/public_html/link.php';
    $con = mysqli_connect($host,$user,$pw,$db);
    $result = mysqli_query($con,"SELECT ID FROM notifications_profile WHERE USER_ID='".$_SESSION['user_id']."' AND VIEW='0' "); 

    //Viendo si hay mensajes no leidos
    require '/home/django83/public_html/link3.php';
    $con2 = mysqli_connect($linked['host'],$linked['user'],$linked['pw'],$linked['db']);
    $resultM = mysqli_query($con2,"SELECT ID FROM conversation WHERE VISTO='0' AND PIV='".$_SESSION['user_id']."' "); 

	echo '<!-- Inicio del Nav-->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://www.fanplusplus.com">F++</a>
        </div>    
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="http://www.fanplusplus.com/fandoms">Fandoms</a></li>
            </ul>
            <ul class="nav navbar-nav">
                <li><a href="http://www.fanplusplus.com/notifications"><span class="glyphicon glyphicon-bell"></span> Notificaciones ';
                //Notificaciones
                if(mysqli_num_rows($result) > 0)
                {
                    echo '<span class="badge">'.mysqli_num_rows($result).'</span>';
                }
    echo '      </a></li>
            </ul>
            <form action="http://www.fanplusplus.com/search" method="get" class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input name="q" type="text" class="form-control" placeholder="Encuentra cosas">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="http://www.fanplusplus.com/m"><span class="glyphicon glyphicon-envelope"></span> Mensajes ';
                    //Notificaciones
                    if(mysqli_num_rows($resultM) > 0)
                    {
                        echo '<span class="badge">'.mysqli_num_rows($resultM).'</span>';
                    }
    echo '      </a></li>
                <li class="">
                    <a href="http://www.fanplusplus.com/sandbox"><span class="glyphicon glyphicon-inbox"></span> Sandbox</a>
                </li>
                <li class="dropdown">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" ></span> ';
                    $nav->getLucky("FIRSTNAME");
                    echo '<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="http://www.fanplusplus.com/';
                        $nav->getLucky("USERNAME");
                        echo '">Perfil</a></li>
                        <li><a href="http://www.fanplusplus.com/invite">Invitar Amigos</a></li>
                        <li class="divider"></li>
                        <li><a href="http://www.fanplusplus.com/settings"><span class="glyphicon glyphicon-cog"></span> Configuración</a></li>
                    </ul>
                </li>
                <li><a href="http://www.fanplusplus.com/logout">Cerrar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <!-- Fin del Nav-->';
}else
{
	echo '<!-- Inicio del Nav-->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://www.fanplusplus.com">F++</a>
        </div>    
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="http://www.fanplusplus.com/fandoms">Fandoms</a></li>
            </ul>
            <form action="http://www.fanplusplus.com/search" method="get" class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input name="q" type="text" class="form-control" placeholder="Encuentra cosas">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://www.fanplusplus.com/signin">Iniciar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <!-- Fin del Nav-->';
}
?>