//Seguir a usuario
function followUser(a,b,btn)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/followuser.php?a="+a+"&b="+b,true);
    xmlhttp.send();
}
//Dejar de seguir usuarios
function unfollowUser(a,b,btn)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/unfollowuser.php?a="+a+"&b="+b,true);
    xmlhttp.send();
}
//Unirse a un Fandom
function makeFan(a,b,btn)//Para ser Fan
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/befan.php?a="+a+"&b="+b,true);
    xmlhttp.send();
}
//Dejar el Fandom
function noFan(a,b,btn)//Para dejar de ser Fan
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/nofan.php?a="+a+"&b="+b,true);
    xmlhttp.send();
}
//Anadir publicacion a sandbox
function addPublication(publication_id, who_add, who_create, publicatedin_id, mode, btn)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/sandboxpublication.php?publication_id="+publication_id+"&who_add="+who_add+"&who_create="+who_create+"&publicatedin_id="+publicatedin_id+"&mode="+mode, true);
    xmlhttp.send();
}
//Eliminar publicacion a sandbox
function deletePublication(publication_id,user_id,mode,btn)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
           btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/sandboxpublicationdelete.php?publication_id="+publication_id+"&user_id="+user_id+"&mode="+mode,true);
    xmlhttp.send();
}
//Anadir nota a sandbox
function noteSandbox(note_id, who_add, who_create, publicatedin_id, mode, btn)
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/notesandbox.php?note_id="+note_id+"&who_add="+who_add+"&who_create="+who_create+"&publicatedin_id="+publicatedin_id+"&mode="+mode,true);
    xmlhttp.send();
}
//Eliminar publicacion a sandbox
function noteDeleteSandbox(note,user_id,mode,btn)
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/deletenotesandbox.php?note="+note+"&user_id="+user_id+"&mode="+mode,true);
    xmlhttp.send();
}
//Reportar usuario
function reportUser(a,b)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("rpArea").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/reportuser.php?a="+a+"&b="+b,true);
    xmlhttp.send();
}

function helloWorld(btn)
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/helloworld.php",true);
    xmlhttp.send();
}

//Añadir imagen a sandbox proveniente de fandom
function addSandboxF(image_id,who_add,who_upload_image,uploadin_id,mode)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
        {
            document.getElementById("btnAddSandbox").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/addsandboxp.php?image_id="+image_id+"&who_add="+who_add+"&who_upload_image="+who_upload_image+"&uploadin_id="+uploadin_id+"&mode="+mode,true);
    xmlhttp.send();
}
//Añadir imagen a sandbox proveniente de un perfil
function addSandboxP(image_id,who_add,who_upload_image,uploadin_id,mode,btn)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/addsandboxp.php?image_id="+image_id+"&who_add="+who_add+"&who_upload_image="+who_upload_image+"&uploadin_id="+uploadin_id+"&mode="+mode,true);
    xmlhttp.send();
}
//Borrar imagen de sandbox
function deleteSandbox(pic,who_add,mode,btn)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/deletesandbox.php?pic="+pic+"&who_add="+who_add+"&mode="+mode,true);
    xmlhttp.send();
}
//Borrar mensaje en el sandbox
function deleteMessage(id,btn)
{
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
        {
            btn.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../drivers/ajax/deletemessage.php?id="+id,true);
    xmlhttp.send();
}
